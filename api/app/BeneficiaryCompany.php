<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeneficiaryCompany extends myModel
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['beneficiary_id', 'company_id', 'policy_id', 'is_active'];

    /**
     * a beneficiary belongs to a beneficiary
     * 
     * @return 
     */
    public function beneficiary()
    {
    	return $this->belongsTo(User::class, 'beneficiary_id');
    }

    /**
     * a beneficiary belongs to a company
     * 
     * @return 
     */
    public function company()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'company_id');
    }

    /**
     * a beneficiarycompany belongs to a policy
     * 
     * @return 
     */
    public function policy()
    {
        return $this->belongsTo(Policy::class, 'policy_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeneficiaryDependent extends myModel
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var array
	 */
    protected $fillable = ['dependent_id', 'beneficiary_id', 'company_id', 'is_active'];

    /**
     * a beneficiarydependent belongs to a dependent
     * 
     * @return 
     */
    public function dependent()
    {
    	return $this->belongsTo(User::class, 'dependent_id');
    }

    /**
     * a benficiarydependent belongs to a beneficiary
     * 
     * @return 
     */
    public function beneficiary()
    {
    	return $this->belongsTo(User::class, 'beneficiary_id');
    }

    /**
     * a beneficiarydependent belongs to an organization_profile
     * 
     * @return 
     */
    public function organization_profile()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'company_id');
    }

}

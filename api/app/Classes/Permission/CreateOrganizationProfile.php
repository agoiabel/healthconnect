<?php

namespace App\Classes\Permission;

use App\RoleUser;
use App\UserProfile;
use App\OrganizationProfile;
use App\Classes\User\GetEffective;
use App\Classes\Utility\FileUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CreateOrganizationProfile
{
	/**
	 * return effective user
	 * 
	 * @param  $request 
	 * @return 
	 */
	public function from($owner, $request)
	{
    	$data = json_decode($request->input('data'));

    	try {

    		$organization_profile = (new OrganizationProfile())->where('name', $data->name)->firstOrFail();

    	} catch (ModelNotFoundException $e) {
	
			$organization_profile = OrganizationProfile::create([
				'owner_id' => $owner->id,
				'organization_id' => $data->organization_type_id,
				'name' => $data->name,
				'bank_name' => $data->bank_name,
				'account_name' => $data->account_name,
				'account_number' => $data->account_number,
				'is_active' => $data->is_active,
				'logo' => url().'/'.(new FileUploader())->uploadFile($request->file('logo')),
				'code' => $data->code,
				'organization_address' => $data->organization_address,
				'organization_profile_state_id' => $data->organization_profile_state_id,
			]);

			UserProfile::create([
				'user_id' => $owner->id,
				'organization_profile_id' => $organization_profile->id,
				'picture' => (new FileUploader())->uploadFile($request->file('picture')),
				'address' => $data->address
			]);
    	}

    	return $organization_profile;

	}
}
<?php

namespace App\Classes\Permission;

use App\Role;
use App\RoleUser;
use App\Organization;
use App\OrganizationProfile;
use App\Classes\User\GetEffective;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CreateOrganizationProfileUserRole 
{
    public static $role_lookup = [
        Organization::HMO => Role::HMO_OWNER,
        Organization::PROVIDER => Role::PROVIDER_OWNER,
        Organization::COMPANY => Role::COMPANY_OWNER
    ];

    public static $moderator_role_lookup = [
        Organization::HMO => Role::HMO_MODERATOR,
        Organization::PROVIDER => Role::PROVIDER_MODERATOR,
        Organization::COMPANY => Role::COMPANY_MODERATOR
    ];

	/**
	 * Handle the process of creating role_user
	 * 
	 * @return 
	 */
	public function fromOrganizationProfile($user, $organization_profile, $active_status)
	{
        $role_id = static::$role_lookup[$organization_profile->organization_id];
        try {
            RoleUser::where('role_id', $role_id)->where('user_id', $user->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            RoleUser::create([
                'role_id' => $role_id,
                'role_slug' => Role::where('id', $role_id)->firstOrFail()->slug,
                'user_id' => $user->id,
                'user_slug' => $user->slug,
                'organization_profile_id' => $organization_profile->id,
                'organization_profile_slug' => $organization_profile->slug,
                'is_active' => $active_status
            ]);
        }
	}

    /**
     * Handle the creation of roleuser on beneficiary creation
     * 
     * @param  $request 
     * @param  $user    
     * @return          
     */
    public function fromBeneficiary($request, $user)
    {
        $organization_profile = OrganizationProfile::where('id', $request->header('OrganizationProfileId'))->firstOrFail();
        $role = Role::where('id', Role::BENEFICIARY)->firstOrFail();
        try {
            RoleUser::where('role_id', $role->id)->where('user_id', $user->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
        } catch (ModelNotFoundException $e) {        
            RoleUser::create([
                'role_id' => $role->id,
                'role_slug' => $role->slug,
                'user_id' => $user->id,
                'user_slug' => $user->slug,
                'organization_profile_id' => $organization_profile->id,
                'organization_profile_slug' => $organization_profile->slug,
                'is_active' => $organization_profile->is_active
            ]);
        }

        return $organization_profile;
    }

    /**
     * Handle the creation of roleuser on beneficiary creation
     * 
     * @param  $request 
     * @param  $user    
     * @return          
     */
    public function fromModerator($request, $user)
    {
        $organization_profile = OrganizationProfile::where('id', $request->header('OrganizationProfileId'))->firstOrFail();
        
        $role_id = static::$moderator_role_lookup[$organization_profile->organization_id];

        try {
            RoleUser::where('role_id', $role_id)->where('user_id', $user->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            RoleUser::create([
                'role_id' => $role_id,
                'role_slug' => Role::where('id', $role_id)->firstOrFail()->slug,
                'user_id' => $user->id,
                'user_slug' => $user->slug,
                'organization_profile_id' => $organization_profile->id,
                'organization_profile_slug' => $organization_profile->slug,
                'is_active' => $organization_profile->is_active
            ]);
        }

        return $organization_profile;
    }

    /**
     * Handle the creation of roleuser for dependent
     * 
     * @param   $request 
     * @param   $user    
     * @return           
     */
    public function fromDependent($request, $user)
    {
        $organization_profile = OrganizationProfile::where('id', $request->header('OrganizationProfileId'))->firstOrFail();
        $role = Role::where('id', Role::BENEFICIARY_DEPENDENT)->firstOrFail();
        try {
            RoleUser::where('role_id', $role->id)->where('user_id', $user->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
        } catch (ModelNotFoundException $e) {        
            RoleUser::create([
                'role_id' => $role->id,
                'role_slug' => $role->slug,
                'user_id' => $user->id,
                'user_slug' => $user->slug,
                'organization_profile_id' => $organization_profile->id,
                'organization_profile_slug' => $organization_profile->slug,
                'is_active' => $organization_profile->is_active
            ]);
        }

        return $organization_profile;  
    }
}
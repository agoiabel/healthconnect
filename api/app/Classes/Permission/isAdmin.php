<?php
namespace App\Classes\Permission;

use App\User;
use App\OrganizationProfile;
use App\Classes\User\GetEffective;
use App\Http\Requests\OrganizationProfileRequest;
use App\Classes\Permission\CreateOrganizationProfile;
use App\Classes\Permission\CreateOrganizationProfileUserRole;

class isAdmin
{
	/**
	 * Handle the prcoess 
	 * 
	 * @return 
	 */
	public function handle(OrganizationProfileRequest $request)
	{		
    	$user = (new GetEffective(new User))->from($request);

    	$organization_profile = (new CreateOrganizationProfile())->from($user, $request);

    	(new CreateOrganizationProfileUserRole())->fromOrganizationProfile($user, $organization_profile, json_decode($request->data)->is_active);
	}
}
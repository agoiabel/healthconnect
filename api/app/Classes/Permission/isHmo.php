<?php
namespace App\Classes\Permission;

use App\User;
use App\CompanyHmo;
use App\HmoProvider;
use App\Organization;
use App\OrganizationProfile;
use App\Classes\User\GetEffective;
use App\OrganizationProfileModerator;
use App\Http\Requests\OrganizationProfileRequest;
use App\Classes\Permission\CreateOrganizationProfile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Classes\Permission\CreateOrganizationProfileUserRole;

class isHmo
{	
	/**
	 * Handle the prcoess 
	 * 
	 * @return 
	 */
	public function handle(OrganizationProfileRequest $request)
	{		
    	$user = (new GetEffective(new User))->from($request);

    	$organization_profile = (new CreateOrganizationProfile())->from($user, $request);

    	$this->createOrganizationRelationshipFrom($request, $organization_profile);

    	(new CreateOrganizationProfileUserRole())->fromOrganizationProfile($user, $organization_profile, json_decode($request->data)->is_active);

    	$this->createOrganizationProfileModeratorFrom($user, $organization_profile);
	}	

	/**
	 * handle the process of creation of moderator
	 * 
	 * @param  $moderator            
	 * @param  $organization_profile 
	 * @return                       
	 */
	private function createOrganizationProfileModeratorFrom($moderator, $organization_profile)
	{
		try {
			OrganizationProfileModerator::where('organization_profile', $moderator->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
		} catch (ModelNotFoundException $e) {
			OrganizationProfileModerator::create([
				'moderator_id' => $moderator->id,
				'organization_profile' => $moderator->id,
			]);
		}
	}

	private function createOrganizationRelationshipFrom($request, $organization_profile)
	{
    	$data = json_decode($request->input('data'));

		if ($data->organization_type_id == Organization::PROVIDER) {
			try {
				(new HmoProvider())->where('hmo_id', $request->header('OrganizationProfileId'))->where('provider_id', $organization_profile->id)->firstOrFail();
				return;
			} catch (ModelNotFoundException $e) {			
				(new HmoProvider())->create([
					'hmo_id' => $request->header('OrganizationProfileId'),
					'provider_id' => $organization_profile->id,
				]);
			}
		} else {
			try {
				(new CompanyHmo())->where('hmo_id', $request->header('OrganizationProfileId'))->where('company_id', $organization_profile->id)->firstOrFail();
				return;
			} catch (ModelNotFoundException $e) {
				(new CompanyHmo())->create([
					'hmo_id' => $request->header('OrganizationProfileId'),
					'company_id' => $organization_profile->id,
				]);
			}
		} 	
	}

}
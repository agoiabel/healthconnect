<?php

namespace App\Classes\Plan;

use App\Plan;
use App\Service;

class GetServicesWithoutPrice
{
	/**
	 * Inject service
	 * 
	 * @param Service $service
	 */
	public function __construct(Service $service)
	{
		$this->service = $service;
	}

	/**
	 * Get services for plan
	 * 
	 * @param  Plan   $plan 
	 * @return 
	 */
	public function forThis(Plan $plan)
	{
		$service_array = [];
		if (! empty($plan->services) ) {
			foreach ($plan->services as $key => $plan_service) {
				$service_array[] = $plan_service->service_id;
			}
			return $this->service->with(['plans'])->whereNotIn('id', $service_array)->get();
		}
		return $this->service->with(['plans'])->get();
	}
}
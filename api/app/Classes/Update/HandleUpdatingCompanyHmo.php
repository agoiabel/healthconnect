<?php

namespace App\Classes\Update;

use App\CompanyHmo;
use App\Http\Requests\TableColumnRequest;

class HandleUpdatingCompanyHmo
{
	/**
	 * Handle the process of updating profile
	 * 
	 * @return 
	 */
	public function handle(TableColumnRequest $request)
	{
		$companyHmo = CompanyHmo::where('id', $request->column_id)->firstOrFail();

		$companyHmo->update([
            $request->column_name => $request->column_value 
        ]);
	}

}
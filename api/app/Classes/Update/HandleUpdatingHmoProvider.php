<?php

namespace App\Classes\Update;

use App\HmoProvider;
use App\Http\Requests\TableColumnRequest;

class HandleUpdatingHmoProvider
{
	/**
	 * Handle the process of updating profile
	 * 
	 * @return 
	 */
	public function handle(TableColumnRequest $request)
	{
		$hmoProvider = HmoProvider::where('id', $request->column_id)->firstOrFail();

		$hmoProvider->update([
            $request->column_name => $request->column_value 
        ]);
	}

}
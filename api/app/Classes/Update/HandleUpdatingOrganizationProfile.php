<?php

namespace App\Classes\Update;

use App\RoleUser;
use App\OrganizationProfile;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TableColumnRequest;

class HandleUpdatingOrganizationProfile
{
	/**
	 * Handle the process of updating profile
	 * 
	 * @return 
	 */
	public function handle(TableColumnRequest $request)
	{
		$organizationProfile = OrganizationProfile::where('id', $request->column_id)->firstOrFail();

		$organizationProfile->update([
            $request->column_name => $request->column_value 
        ]);

		//owner and moderator blocked
		foreach (RoleUser::where('organization_profile_id', $organizationProfile->id)->get() as $role_user) {
			$role_user->update([
				'is_active' => $request->column_value
			]);
		}
	}

}
<?php

namespace App\Classes\Update;

use App\OrganizationProfileModerator;
use App\Http\Requests\TableColumnRequest;

class HandleUpdatingOrganizationProfileModerator
{
	/**
	 * Handle the process of updating profile
	 * 
	 * @return 
	 */
	public function handle(TableColumnRequest $request)
	{
		$organizationProfileModerator = OrganizationProfileModerator::where('id', $request->column_id)->firstOrFail();

		$organizationProfileModerator->update([
            $request->column_name => $request->column_value 
        ]);
	}

}
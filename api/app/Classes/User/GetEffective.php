<?php

namespace App\Classes\User;

use App\User;
use App\RoleUser;
use Illuminate\Auth\Guard;
use App\OrganizationProfile;
use App\Classes\Utility\FileUploader;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GetEffective 
{

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * return effective user
	 * 
	 * @param  $request 
	 * @return 
	 */
	public function from($request)
	{
    	$data = json_decode($request->input('data'));

        try {
            $user = $this->user->where('email', $data->email)->firstOrFail();
        } catch (ModelNotFoundException $e) {
         
            $user = $this->user->create([
                'state_id' => $data->state_id,
                'first_name' => $data->first_name,
                'middle_name' => $data->middle_name,
                'last_name' => $data->last_name,
                'email' => $data->email,
                'date_of_birth' => $data->date_of_birth,
                'password' => $data->code ? $data->code : OrganizationProfile::where('id', $request->header('OrganizationProfileId'))->firstOrFail()->code,
                'gender' => $data->gender,
                'primary_phone_number' => $data->primary_phone_number,
                'primary_profile_picture' => url().'/'.(new FileUploader())->uploadFile($request->file('picture')),
            ]);
            
        }

        return $user;
	}


}
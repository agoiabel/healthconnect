<?php 

namespace App\Classes\User;

use App\Exceptions\CustomException;

class PasswordIsNotCorrect
{
	public function handle()
	{
		throw new CustomException(route('index'), "password is not correct");
	}
}
<?php 

namespace App\Classes\User;

class RedirectAdminTo 
{

	/**
	 * redirect admin to create client route on login
	 * 
	 * @return 
	 */
	public function handle()
	{
		return redirect()->route('hmo.index');	
	}

}
<?php 

namespace App\Classes\User;

use App\Role;

class RedirectBaseOnRole 
{
    /**
     * password lookup
     * 
     * @var 
     */
    public static $userRoleLookUp = [
        Role::ADMIN => \App\Classes\User\isAdmin::class,
        Role::HMO_OWNER => \App\Classes\User\isHmoOwner::class,
    ];

    /**
     * redirect user base on role on provider creation
     * 
     * @return 
     */
    public function onProviderCreated()
    {
        return (new static::$userRoleLookUp[ (int)session()->get('role_chosen_on_login')  ])->provider();                          
    }


}
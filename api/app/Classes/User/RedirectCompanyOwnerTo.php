<?php 

namespace App\Classes\User;

class RedirectCompanyOwnerTo 
{

	/**
	 * redirect admin to create client route on login
	 * 
	 * @return 
	 */
	public function handle()
	{
		return redirect()->route('company_owner.company_index');	
	}

}
<?php 

namespace App\Classes\User;

class RedirectHmoModeratorTo 
{

	/**
	 * redirect admin to create client route on login
	 * 
	 * @return 
	 */
	public function handle()
	{
		return redirect()->route('hmo_moderator.hmo_index');	
	}

}
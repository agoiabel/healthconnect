<?php 

namespace App\Classes\User;

class RedirectHmoOwnerTo 
{

	/**
	 * redirect admin to create client route on login
	 * 
	 * @return 
	 */
	public function handle()
	{
		return redirect()->route('hmo_owner.hmo_index');	
	}

}
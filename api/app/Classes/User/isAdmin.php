<?php

namespace App\Classes\User;

class isAdmin
{
	public function handle()
	{
		return redirect()->route('admin_dashboard');
	}

	/**
	 * redirect base on provider creation
	 * 
	 * @return 
	 */
	public function provider()
	{
		return route('provider.index');
	}
}
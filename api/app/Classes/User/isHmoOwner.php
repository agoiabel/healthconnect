<?php

namespace App\Classes\User;

class isHmoOwner
{
	public function handle()
	{
		return redirect()->route('admin_dashboard');
	}

	/**
	 * redirect base on provider creation
	 * 
	 * @return 
	 */
	public function provider()
	{
		return redirect()->route('hmo_owner.hmo_index');
	}
	
}
<?php

namespace App\Classes\User;

class isNotAdmin
{
	public function handle()
	{
		return redirect()->route('others_dashboard');
	}
}
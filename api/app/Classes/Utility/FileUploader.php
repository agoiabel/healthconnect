<?php 

namespace App\Classes\Utility;

use Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{

    /**
     * Image path
     * 
     * @var string
     */
    public $baseDir = 'upload/';

    /**
     * upload image and generate thumbnail
     * 
     * @param  UploadedFile $image 
     * 
     * @return array.              
     */
    public function upload(UploadedFile $image, $width, $height)
    {
        $files = [];

        $files['image_path'] = $this->makeImage($image);
        $files['image_thumbnail_path'] = $this->makeThumbnail($image, $width, $height);

        return $files;
    }

    /**
     * make image and move to destination
     * 
     * @param  string $image 
     * @return string        
     */
    public function makeImage($image)
    {
        $name = $this->makeName($image);
        $path = $this->baseDir . $name;

        Image::make($image)->save($path);
            
        return $path;
    }

    /**
     * move resize image to path
     * 
     * @param  string $image
     * @return string        	 
     */
    public function makeThumbnail($image, $width, $height)
    {
        $path = $this->baseDir . 'tn-'. $this->makeName($image);
            
        Image::make($image)->resize($width, $height)->save($path);

        return $path;
    }

    /**
     * upload files
     * 
     * @return file path
     */
    public function uploadFile($file_name)
    {
        $name = $this->makeName($file_name);
        
        $path = $this->baseDir . $name;

        $file_name->move($this->baseDir, $name);
            
        return $path;
    }

    /**
     * generate name
     * 
     * @param  string $name 
     * @return string       
     */
    protected function makeName($image)
    {
        return $this->image_name = sprintf("%s-%s-%s", rand(1, 200), time(), $image->getClientOriginalName());
    }
    
}
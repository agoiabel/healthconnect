<?php

namespace App\Classes\Utility;

class Generate 
{
	/**
	 * Generate token
	 * 
	 * @return 
	 */
	public function token()
	{
		return uniqid() . time();
	}
}
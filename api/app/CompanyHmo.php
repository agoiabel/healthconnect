<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyHmo extends myModel
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['company_id', 'hmo_id', 'is_active'];

    /**
     * a companyHmo belongs to an hmo
     * 
     * @return 
     */
    public function hmo()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'hmo_id');
    }

    /**
     * a companyHmo belongs to a company
     * 
     * @return 
     */
    public function company()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'company_id');	
    }
    
}

<?php 

namespace App\Exceptions;

class CustomException extends \Exception
{
	public function __construct($url, $error_message)
	{
		$this->url = $url;

		$this->error_message = $error_message;
	}
}
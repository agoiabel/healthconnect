<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HmoProvider extends myModel
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['hmo_id', 'provider_id', 'is_active'];

    /**
     * an hmoProvider belongs to an hmo
     * 
     * @return 
     */
    public function hmo()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'hmo_id');
    }

    /**
     * an hmoProvider belongs to a provider
     * 
     * @return 
     */
    public function provider()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'provider_id');
    }
}

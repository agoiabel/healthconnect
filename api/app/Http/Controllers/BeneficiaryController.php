<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\BeneficiaryCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BeneficiaryFormRequest;

class BeneficiaryController extends Controller
{

    public function index(BeneficiaryCompany $beneficiaryCompany, Request $request)
    {
        return response()->json([
            'beneficiaryCompanies' => $beneficiaryCompany->where('company_id', $request->header('OrganizationProfileId'))->with(['beneficiary', 'policy.policyset'])->get()
        ]);
    }

	/**
	 * Handle the process of storing beneficiary
	 * 
	 * @return 
	 */
    public function store(BeneficiaryFormRequest $request)
    {
    	$request->handle();

    	return response()->json([
    		'message' => 'beneficiary was created successfully'
    	]);
    }
}

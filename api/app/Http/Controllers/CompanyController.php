<?php

namespace App\Http\Controllers;

use App\PolicySet;
use App\CompanyHmo;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CompanyController extends Controller
{
	/**
	 * get the company policy sets
	 * 
	 * @return 
	 */
    public function policyset(Request $request, CompanyHmo $companyHmo, PolicySet $policyset)
    {
    	$company_id = $request->header('OrganizationProfileId');

    	$hmo_ids = [];
    	foreach ($companyHmo->where('company_id', $company_id)->get() as $key => $companyHmo) {
    		$hmo_ids[] = $companyHmo->hmo_id;
    	}

    	$policyset_ids = [];
    	foreach ($hmo_ids as $key => $hmo_id) {
    		try {
    			$policyset_ids[] = $policyset->where('company_id', $company_id)->where('hmo_id', $hmo_id)->firstOrFail()->id;			
			} catch (ModelNotFoundException $e) {
				return;
			}	
    	}

    	return response()->json([
    		'policysets' => $policyset->whereIn('id', $policyset_ids)->with(['policies'])->get()
    	]);
    }

}

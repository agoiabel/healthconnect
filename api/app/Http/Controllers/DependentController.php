<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use App\BeneficiaryDependent;
use App\Http\Controllers\Controller;
use App\Http\Requests\DependentFormRequest;

class DependentController extends Controller
{
    /**
     * Handle the process of displaying dependent
     * 
     * @return 
     */
    public function index(BeneficiaryDependent $beneficiaryDependent, Request $request, Guard $auth)
    {
        return response()->json([
            'beneficiaryCompanyDependents' => BeneficiaryDependent::where('company_id', $request->header('OrganizationProfileId'))
                                                                    ->where('beneficiary_id', $auth->user()->id)
                                                                    ->with(['dependent'])
                                                                    ->get()
        ]);
    }

	/**
	 * Handle the process of creating a new dependent
	 * 
	 * @param  DependentFormRequest $request 
	 * @return 
	 */
    public function store(DependentFormRequest $request)
    {
    	$request->handle();

    	return response()->json([
    		'message' => 'dependent was created successfully'
    	]);
    }
}

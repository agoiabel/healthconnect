<?php

namespace App\Http\Controllers;

use App\CompanyHmo;
use App\HmoProvider;
use App\Organization;
use App\Http\Requests;
use App\OrganizationProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HmoController extends Controller
{
	/**
	 * display hmo companies
	 * 
	 * @return 
	 */
    public function company(CompanyHmo $companyHmo, Request $request)
    {
    	return $companyHmo->where('hmo_id', $request->header('OrganizationProfileId'))->with(['company.owner', 'company.state'])->latest()->get();
    }

    /**
     * display all hmo providers
     * 
     * @param  HmoProvider $hmoProvider 
     * @param  Request     $request     
     * @return                    
     */
    public function provider(HmoProvider $hmoProvider, Request $request)
    {
    	return $hmoProvider->where('hmo_id', $request->header('OrganizationProfileId'))->with(['provider.owner'])->latest()->get();
    }

    /**
     * display all companies
     * 
     * @return 
     */
    public function all_company(OrganizationProfile $profile)
    {
        return $profile->where('organization_id', Organization::COMPANY)->with(['owner', 'state', 'organization'])->latest()->get();
    }

    /**
     * display all providers
     * 
     * @return 
     */
    public function all_provider(OrganizationProfile $profile)
    {
        return $profile->where('organization_id', Organization::PROVIDER)->with(['owner', 'state', 'organization'])->latest()->get();
    }

    /**
     * display all hmos
     * 
     * @return 
     */
    public function all_hmo(OrganizationProfile $profile)
    {
        return $profile->where('organization_id', Organization::HMO)->with(['owner', 'state', 'organization'])->latest()->get();
    }

    /**
     * display all hmos
     * 
     * @return 
     */
    public function all_organization(OrganizationProfile $profile)
    {
        return $profile->with(['owner', 'state', 'organization'])->latest()->get();
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
	/**
	 * Handle the process of uploading image
	 * 
	 * @param  Request $request 
	 * @return            
	 */
    public function store(Request $request)
    {
    	$data = json_decode($request->input('data'));

    	return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;

class LoginController extends Controller
{	

    /**
     * Process authentication
     * 
     * @param  LoginFormRequest $request 
     * @return 
     */
    public function store(LoginFormRequest $request)
    {
        try {
    		$user = $request->handle(new User);
            return response()->json([
                'user' => $user
            ], 200);
        } catch (CustomException $e) {
            return response()->json(['error' => 'oops, credentials not correct, check email and password'], 422);
        }

    }

}	

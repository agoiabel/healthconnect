<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrganizationProfileModerator;
use App\Http\Requests\ModeratorFormRequest;

class ModeratorController extends Controller
{
	/**
	 * handle the process of displaying organization moderators
	 * 
	 * @return 
	 */
	public function index(OrganizationProfileModerator $organization_profile_moderator, Request $request)
	{
		return response()->json([
			'organization_profile_moderators' => $organization_profile_moderator->where('organization_profile_id', $request->header('OrganizationProfileId'))->with(['moderator'])->get()
		]);
	}

	/**
	 * Handle the process of creation of moderator
	 * 
	 * @param  ModeratorFormRequest $request 
	 * @return                         
	 */
    public function store(ModeratorFormRequest $request)
    {
    	$request->handle();

    	return response()->json([
    		'message' => 'moderator was created successfully'
    	]);
    }
}

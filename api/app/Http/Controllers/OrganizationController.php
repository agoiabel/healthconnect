<?php

namespace App\Http\Controllers;

use App\Organization;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrganizationController extends Controller
{
	/**
	 * Return all organizations
	 * 
	 * @return 
	 */
    public function index(Organization $organization)
    {
    	return $organization->latest()->get();
    }

}

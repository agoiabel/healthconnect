<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\OrganizationProfile;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrganizationProfileRequest;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class OrganizationProfileController extends Controller
{
    /**
     * Handle the process of getting all profiles
     * 
     * @return 
     */
    public function index(OrganizationProfile $profile)
    {
        return $profile->with(['owner', 'state'])->latest()->get();
    }

	/**
	 * handle the process of storing new organization profile
	 * 
	 * @param  Request $request 
	 * @return            
	 */
    public function store(OrganizationProfileRequest $request)
    {   
        try {
            $request->handle();
        	return response()->json([
        		'message' => 'the organization profile was created successfully'
        	]);
        } catch (FileException $e) {
            return response()->json([
                'error' => 'there was an issue uploading the image to the server, please re upload the image'
            ], 422);
        }
    }
}

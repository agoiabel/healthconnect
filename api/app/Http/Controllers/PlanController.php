<?php

namespace App\Http\Controllers;

use App\Plan;
use App\PlanService;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PlanFormRequest;
use App\Http\Requests\UpdatePlanFormRequest;
use App\Classes\Plan\GetServicesWithoutPrice;
use App\Http\Requests\AddPlanServicePriceFormRequest;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class PlanController extends Controller
{
    /**
     * display view to show all plans
     * 
     * @return 
     */
    public function index(Plan $plan, Request $request)
    {
    	return $plan->where('hmo_id', $request->header('OrganizationProfileId'))->latest()->get();
    }

    /**
     * Handle the process of creation
     * 
     * @return 
     */
    public function store(PlanFormRequest $request)
    {
        $request->handle(new plan);

        return response()->json(['message' => 'plan was created successfully']);        
    }


    /**
     * Handle the deletion of hmo
     * 
     * @param  $id 
     * @return 
     */
    public function delete($id, Plan $plan)
    {
        $plan->where('id', $id)->firstOrFail()->delete();

        return response()->json(['message' => 'plan was deleted successfully']);        
    }

    /**
     * display view to show services and add plan to it
     * 
     * @return 
     */
    public function addService($plan_slug, Plan $plan, GetServicesWithoutPrice $service)
    {
    	try {
	        $plan = $plan->where('slug', $plan_slug)->where('is_active', Plan::active)->firstOrFail();
    	} catch (ModelNotFoundException $e) {
        	return response()->json(['error' => 'plan was deleted successfully'], 404);        
    	}

        $services = $service->forThis($plan);

        return response()->json([
        	'plan' => $plan,
        	'services' => $services
        ], 200);
    }


    /**
     * Handle the process of storing service plan with price
     * 
     * @param AddPlanServicePriceFormRequest 
     */
    public function addServicePrice(AddPlanServicePriceFormRequest $request)
    {
        $request->handle();

        return response()->json(['message' => 'the price was successfully added to the service']);
    }

    /**
     * Handle getting of service plans
     * 
     * @return 
     */
    public function viewService(Plan $plan, PlanService $planService, $plan_slug)
    {
    	try {
	        $plan = $plan->where('slug', $plan_slug)->where('is_active', Plan::active)->firstOrFail();
    	} catch (ModelNotFoundException $e) {
        	return response()->json(['error' => 'plan not found successfully'], 404);        
    	}

        $planServices = $planService->with(['plan', 'service'])->where('plan_id', $plan->id)->get();

        return response()->json([
        	'plan' => $plan,
        	'planServices' => $planServices
        ], 200);
    }


    /**
     * Delete Plan Size Price
     * 
     * @param $plan_service_id 
     * @return 
     */
    public function deleteServicePrice($plan_service_id, PlanService $planService)
    {
        $planService->where('id', $plan_service_id)->firstOrFail()->delete();
        
        return response()->json([
            'message' => 'the service was successfully remove from your plan',
        ]);   
    }


    /**
     * display view ti show single service
     * 
     * @param  $slug 
     * @return 
     */
    public function show($slug, Plan $plan)
    {
        return response()->json(['plan' => $plan->where('slug', $slug)->firstOrFail()]);
    }

    /**
     * Handle the process of updating service
     * 
     * @return 
     */
    public function update(UpdatePlanFormRequest $request)
    {
        $request->handle(new Plan);

        return response()->json(['message' => 'plan was updated successfully']);           
    }
}

<?php

namespace App\Http\Controllers;

use App\Policy;
use App\PolicySet;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PolicyFormRequest;
use App\Http\Requests\UpdatePolicyFormRequest;

class PolicyController extends Controller
{
	/**
	 * display view to show policy
	 * 
	 * @return 
	 */
    public function index(Policy $policy, PolicySet $policyset, $policysetSlug)
    {
		return $policy->where('policy_set_id', $policyset->where('slug', $policysetSlug)->firstOrFail()->id)->latest()->get();    	
    }

    /**
     * store new policy
     * 
     * @return 
     */
    public function store(PolicyFormRequest $request)
    {
    	$request->handle();	

    	return response()->json([
    		'message' => 'The policy was created successfully'
    	]);
    }

    /**
     * Handle the process of deleting policy
     * 
     * @return 
     */
    public function delete(Policy $policy, $policy_id)
    {        
    	$policy->where('id', $policy_id)->firstOrFail()->delete();

    	return response()->json([
    		'message' => 'The policy was deleted successfully'
    	]);    	
    }

    /**
     * display view ti show single service
     * 
     * @param  $slug 
     * @return 
     */
    public function show($slug, Policy $policy)
    {
        return response()->json(['policy' => $policy->where('slug', $slug)->firstOrFail()]);
    }

    /**
     * Handle the process of updating service
     * 
     * @return 
     */
    public function update(UpdatePolicyFormRequest $request)
    {        
        $request->handle(new Policy);

        return response()->json(['message' => 'policy was updated successfully']);           
    }
}

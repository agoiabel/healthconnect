<?php

namespace App\Http\Controllers;

use App\PolicySet;
use App\Http\Requests;
use App\OrganizationProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PolicySetFormRequest;
use App\Http\Requests\UpdatePolicySetFormRequest;

class PolicysetController extends Controller
{
	/**
	 * display view to get companies policysets
	 * 
	 * @return 
	 */
	public function index(PolicySet $policyset, Request $request, $companySlug, OrganizationProfile $profile)
	{
    	return $policyset->where('company_id', $profile->where('slug', $companySlug)->firstOrFail()->id)->where('hmo_id', $request->header('OrganizationProfileId'))->latest()->get();	
	}

	/**
	 * Process the storing of new policyset
	 * 
	 * @param  PolicySetFormRequest $request 
	 * @return 
	 */
    public function store(PolicySetFormRequest $request)
    {
    	$request->handle();

    	return response()->json([
    		'message' => 'policyset was created successfully'
    	], 200);
    }

    /**
     * Handle the deletion of policyset
     * 
     * @return 
     */
    public function delete($policy_set_id, PolicySet $policySet)
    {
    	$policySet->where('id', $policy_set_id)->firstOrFail()->delete();

        return response()->json(['message' => 'policyset was deleted successfully']);
    }

    /**
     * display view ti show single service
     * 
     * @param  $slug 
     * @return 
     */
    public function show($slug, PolicySet $policySet)
    {
        return response()->json(['policyset' => $policySet->where('slug', $slug)->firstOrFail()]);
    }

    /**
     * Handle the process of updating service
     * 
     * @return 
     */
    public function update(UpdatePolicySetFormRequest $request)
    {
        $request->handle(new PolicySet);

        return response()->json(['message' => 'policyset was updated successfully']);           
    }
}

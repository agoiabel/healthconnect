<?php

namespace App\Http\Controllers;

use App\Service;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceFormRequest;
use App\Http\Requests\UpdateServiceFormRequest;

class ServiceController extends Controller
{
    /**
     * display view to show all services
     * 
     * @return 
     */
    public function index(Service $service)
    {
        return $service->latest()->paginate(10);
    }

    /**
     * Handle the process of creation
     * 
     * @return 
     */
    public function store(ServiceFormRequest $request)
    {
        $request->handle(new Service);

        return response()->json(['message' => 'service was created successfully']);        
    }


    /**
     * Handle the deletion of hmo
     * 
     * @param  $id 
     * @return 
     */
    public function delete($id, Service $service)
    {
        $service->where('id', $id)->firstOrFail()->delete();

        return response()->json(['message' => 'service was deleted successfully']);        
    }

    /**
     * display view ti show single service
     * 
     * @param  $slug 
     * @return 
     */
    public function show($slug, Service $service)
    {
        return response()->json(['service' => $service->where('slug', $slug)->firstOrFail()]);
    }

    /**
     * Handle the process of updating service
     * 
     * @return 
     */
    public function update(UpdateServiceFormRequest $request)
    {
        $request->handle(new Service);

        return response()->json(['message' => 'service was updated successfully']);           
    }

}


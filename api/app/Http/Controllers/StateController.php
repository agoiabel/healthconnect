<?php

namespace App\Http\Controllers;

use App\State;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StateController extends Controller
{
	/**
	 * return all states
	 * 
	 * @return 
	 */
    public function index(State $state)
    {
    	return $state->latest()->get();
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TableColumnRequest;

class TableController extends Controller
{
    public function column(TableColumnRequest $request)
    {
    	$request->handle();

    	return response()->json([
    		'message' => 'your update was successful'
    	]);
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	/**
	 * get all users
	 * 
	 * @return 
	 */
    public function index(User $user)
    {
    	return $user->latest()->get();
    }
}

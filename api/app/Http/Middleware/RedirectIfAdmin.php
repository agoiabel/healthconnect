<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RedirectIfAdmin
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, User $user, Role $role)
    {
        $this->auth = $auth;

        $this->user = $user;

        $this->role = $role;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $client_role_id = $request->header('RoleId');

        if ( empty($client_role_id) ) {
            return response()->json([
                'message' => 'you do not have the permission'
            ], 401);
        }

        if ( $this->role->isAdmin($this->auth->user()->id) ) {

           try {
                $this->roleUser->where('user_id', $this->auth->user()->id)->where('role_id', $client_role_id)->where('is_active', Role::ACTIVE)->firstOrFail();
            } catch (ModelNotFoundException $e) {
                return response()->json([
                    'message' => 'you are not an active user'
                ], 401);            
            }

        }        
    }

}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use Illuminate\Contracts\Auth\Guard;
use App\OrganizationProfileModerator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RedirectIfModeratorIsActive
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, Role $role, OrganizationProfileModerator $organizationProfileModerator)
    {
        $this->auth = $auth;

        $this->role = $role;

        $this->organizationProfileModerator = $organizationProfileModerator;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $client_role_id = $request->header('RoleId');
        $client_organization_profile_id = $request->header('OrganizationProfileId');

        if ( $this->role->hmoModerator($client_role_id) || $this->role->companyModerator($client_role_id) || $this->role->providerModerator($client_role_id) ) {
            
           try {
                
                $this->organizationProfileModerator->where('moderator_id', $this->auth->user()->id)
                     ->where('organization_profile_id', $client_organization_profile_id)
                     ->where('is_active', OrganizationProfileModerator::ACTIVE)
                     ->firstOrFail();

                return $next($request);
            } catch (ModelNotFoundException $e) {
                return response()->json([
                    'message' => 'you are currently inactive, please contact the admin'
                ], 401);            
            }

        }   
        
        return $next($request);

    }

}

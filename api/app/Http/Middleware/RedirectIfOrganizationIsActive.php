<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use App\OrganizationProfile;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RedirectIfOrganizationIsActive
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, Role $role, OrganizationProfile $organizationProfile)
    {
        $this->auth = $auth;

        $this->role = $role;

        $this->organizationProfile = $organizationProfile;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $client_role_id = $request->header('RoleId');
        $client_organization_profile_id = $request->header('OrganizationProfileId');

        //handle case of super admin user
        if ( $this->role->isSuperAdmin($client_role_id) || $this->role->isAdmin($client_role_id) ) {
            //check and make sure authenticated user has role of super admin real
            return $next($request);
        }   

       try {
            $this->organizationProfile->where('id', $client_organization_profile_id)->where('is_active', OrganizationProfile::ACTIVE)->firstOrFail();
            return $next($request);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'the organization profile is currently inactive, please contact the admin'
            ], 401);            
        }

    }

}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use App\User;
use App\RoleUser;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RedirectIfPermitted
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, User $user, Role $role, RoleUser $roleUser)
    {
        $this->auth = $auth;

        $this->role = $role;

        $this->user = $user;

        $this->roleUser = $roleUser;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $client_role_id = $request->header('RoleId');
        $client_organization_profile_id = $request->header('OrganizationProfileId');

        // return response()->json([
        //     'auth_user_id' => $this->auth->user()->id,
        //     'client_role_id' => $client_role_id
        // ], 401);

        if ( empty($client_role_id) ) {
            return response()->json([
                'message' => 'you have not chosen a role'
            ], 401);
        }

        //handle case of super admin user
        if ( $this->role->isSuperAdmin($client_role_id) ) {
            //check and make sure authenticated user has role of super admin real
            return $next($request);
        }   

        //handle case of admin user
        if ( $this->role->isAdmin($client_role_id) ) {
           try {
                $this->roleUser->where('user_id', $this->auth->user()->id)->where('role_id', $client_role_id)->where('is_active', Role::ACTIVE)->firstOrFail();
                return $next($request);
            } catch (ModelNotFoundException $e) {
                return response()->json([
                    'message' => 'you are not an active user'
                ], 401);            
            }
        }      
   

        if ( empty($client_organization_profile_id) ) {
            return response()->json([
                'message' => 'you have to chose an organization profile'
            ], 401);
        }

        try {

            $this->roleUser->where('user_id', $this->auth->user()->id)->where('role_id', $client_role_id)->where('organization_profile_id', $client_organization_profile_id)->firstOrFail();
        
            return $next($request);

            //i need to check if the role actually has the permission

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => 'you do not have the required permission'
            ], 401);            
        }

    }

    // private function hasAdminRole($user_id)
    // {
    //     try {
    //         $this->roleUser->where('role_id', $user_id)->firstOrFail();
    //         return true;
    //     } catch (ModelNotFoundException $e) {
    //         return false;
    //     }
    // }

}

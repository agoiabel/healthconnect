<?php

namespace App\Http\Requests;

use App\PlanService;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AddPlanServicePriceFormRequest extends Request
{

    public function __construct(PlanService $planService)
    {
        $this->planService = $planService;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id' => 'required',
            'plan_id' => 'required',
            'amount' => 'required',
        ];
    }

    /**
     * Update the price if already exists
     * 
     * @return 
     */
    public function updatePrice()
    {
        $planService = $this->planService->where('plan_id', $this->plan_id)->where('service_id', $this->service_id)->firstOrFail();
        return $planService->updateModel(['price' => $this->amount]);        
    }

    /**
     * Handle the process of attaching a service to a plan
     * 
     * @return 
     */
    public function handle()
    {
        try {
            $this->updatePrice();
        } catch (ModelNotFoundException $e) {
            $this->planService->initModel([
                'plan_id' => $this->plan_id,
                'service_id' => $this->service_id,
                'price' => $this->amount,
            ])->saveModel();
        }
    }

    /**
     * Handle the updating of price
     * 
     * @return 
     */
    public function update()
    {
        return $this->updatePrice();
    }

}

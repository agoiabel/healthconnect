<?php

namespace App\Http\Requests;

use App\User;
use App\UserProfile;
use App\BeneficiaryCompany;
use App\OrganizationProfile;
use App\Http\Requests\Request;
use App\Classes\User\GetEffective;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Classes\Permission\CreateOrganizationProfileUserRole;

class BeneficiaryFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => 'required',
            'data' => 'required',
        ];
    }

    /**
     * Handle the process of creation of beneficiary
     * 
     * @return 
     */
    public function handle()
    {
        $user = (new GetEffective(new User))->from($this);

        $organization_profile = (new CreateOrganizationProfileUserRole())->fromBeneficiary($this, $user);

        UserProfile::create([
            'user_id' => $user->id,
            'organization_profile_id' => $organization_profile->id,
            'picture' => '',
            'address' => json_decode($this->data)->address,
            'job_name' => json_decode($this->data)->job_name,
        ]);

        try {
            BeneficiaryCompany::where('beneficiary_id', $user->id)->where('company_id', $organization_profile->id)->where('policy_id', json_decode($this->data)->policy_id)->firstOrFail();
            return;
        } catch (ModelNotFoundException $e) {
            BeneficiaryCompany::create([
                'beneficiary_id' => $user->id,
                'company_id' => $organization_profile->id,
                'policy_id' => json_decode($this->data)->policy_id,
                'is_active' => $organization_profile->is_active,
            ]);
        }
     
    }

}

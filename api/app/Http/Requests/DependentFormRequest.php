<?php

namespace App\Http\Requests;

use App\User;
use App\UserProfile;
use Illuminate\Auth\Guard;
use App\BeneficiaryDependent;
use App\Http\Requests\Request;
use App\Classes\User\GetEffective;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Classes\Permission\CreateOrganizationProfileUserRole;

class DependentFormRequest extends Request
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => 'required',
            'data' => 'required',
        ];
    }

    /**
     * handle the process of storing new dependent
     * 
     * @return 
     */
    public function handle()
    {
        $user = (new GetEffective(new User))->from($this);
        $organization_profile = (new CreateOrganizationProfileUserRole())->fromDependent($this, $user);
        
        try {
            UserProfile::where('user_id', $user->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            UserProfile::create([
                'user_id' => $user->id,
                'organization_profile_id' => $organization_profile->id,
                'picture' => '',
                'address' => json_decode($this->data)->address,
                'job_name' => json_decode($this->data)->job_name,
            ]);
        }

        try {
            BeneficiaryDependent::where('beneficiary_id', $this->auth->user()->id)->where('dependent_id', $user->id)->where('company_id', $organization_profile->id)->firstOrFail();
            return;
        } catch (ModelNotFoundException $e) {
            BeneficiaryDependent::create([
                'dependent_id' => $user->id,
                'beneficiary_id' => $this->auth->user()->id,
                'company_id' => $organization_profile->id,
                'is_active' => $organization_profile->is_active,
            ]);
        }
    }
}

<?php

namespace App\Http\Requests;

use App\User;
use App\Http\Requests\Request;
use App\Classes\Utility\Generate;
use App\Exceptions\CustomException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LoginFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
        ];
    }

    /**
     * Handle Processing of login
     * 
     * @return 
     */
    public function handle(User $user)
    {
        try {
            $user =  $user->with(['role_users.organization_profile.organization', 'role_users.role.permissionRoles.permission.permissionChildren'])->where('email', $this->email)->firstOrFail();

            $user->update([
                'authorization' => (new Generate())->token()
            ]);

            $this->comparePassword($user, $this->password);
            Auth::login($user, true);
            return $user;
        } catch (ModelNotFoundException $e) {
            throw new CustomException(route('index'), "bad credentials, seems something is wrong with email");
        }
    }


    /**
     * password lookup
     * 
     * @var 
     */
    public static $passwordLookUp = [
        true => \App\Classes\User\PasswordIsCorrect::class,
        false => \App\Classes\User\PasswordIsNotCorrect::class
    ];


    /**
     * check password is correct
     * @param  $user_password 
     * @param  $password      
     * @return                 
     */
    public function comparePassword($user, $password)
    {
        (new static::$passwordLookUp[Hash::check($password, $user->password)])->handle();                    
    }

}

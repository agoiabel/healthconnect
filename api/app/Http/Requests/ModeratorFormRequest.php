<?php

namespace App\Http\Requests;

use App\User;
use App\UserProfile;
use App\Http\Requests\Request;
use App\Classes\User\GetEffective;
use App\OrganizationProfileModerator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Classes\Permission\CreateOrganizationProfileUserRole;


class ModeratorFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => 'required',
            'data' => 'required',
        ];
    }

    /**
     * handle the process of creation of moderator
     * 
     * @return 
     */
    public function handle()
    {
        $user = (new GetEffective(new User))->from($this);
        $organization_profile = (new CreateOrganizationProfileUserRole())->fromModerator($this, $user);

        try {
            UserProfile::where('user_id', $user->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            UserProfile::create([
                'user_id' => $user->id,
                'organization_profile_id' => $organization_profile->id,
                'picture' => '',
                'address' => json_decode($this->data)->address,
                'job_name' => json_decode($this->data)->job_name,
            ]);
        }
        try {
            OrganizationProfileModerator::where('moderator_id', $user->id)->where('organization_profile_id', $organization_profile->id)->firstOrFail();
            return;
        } catch (ModelNotFoundException $e) {
            OrganizationProfileModerator::create([
                'moderator_id' => $user->id,
                'organization_profile_id' => $organization_profile->id,
                'is_active' => $organization_profile->is_active,
            ]);
        }
    }

}
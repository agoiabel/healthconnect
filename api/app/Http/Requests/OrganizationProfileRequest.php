<?php

namespace App\Http\Requests;

use App\Role;
use App\OrganizationProfile;
use App\Http\Requests\Request;

class OrganizationProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data' => 'required',
            'logo' => 'required',
            'picture' => 'required',
        ];
    }

    /**
     * password lookup
     * 
     * @var 
     */
    public static $userRoleLookUp = [
        Role::SUPER_ADMIN => \App\Classes\Permission\isAdmin::class,
        Role::ADMIN => \App\Classes\Permission\isAdmin::class,
        Role::HMO_OWNER => \App\Classes\Permission\isHmo::class,
        Role::HMO_MODERATOR => \App\Classes\Permission\isHmo::class,
    ];

    /**
     * Handle the process of storing organization profile
     * 
     * @return 
     */
    public function handle()
    {
        return (new static::$userRoleLookUp[$this->header('RoleId')])->handle($this);                                       
    }

}

<?php

namespace App\Http\Requests;

use App\Plan;
use App\Http\Requests\Request;

class PlanFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'required',
            'is_active' => 'required',
        ];
    }

    /**
     * Handle the process of creating new plan
     * 
     * @param  Plan   $plan 
     * @return        
     */
    public function handle(Plan $plan)
    {
        $plan->create([
            'name' => $this->name,
            'is_active' => $this->is_active,
            'code' => $this->code,
            'hmo_id' => $this->header('OrganizationProfileId'),
        ]);   
    }

}

<?php

namespace App\Http\Requests;

use App\PolicySet;
use App\OrganizationProfile;
use App\Http\Requests\Request;

class PolicySetFormRequest extends Request
{
    public function __construct(PolicySet $policy_set)
    {
        $this->policy_set = $policy_set;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'is_active' => 'required',
            'start_date' => 'required|date',
            'end_date' => [
                'required',
                'date',
                'after:start_date',
            ]
        ];
    }

    /**
     * Handle the process storing new policyset
     * 
     * @return 
     */
    public function handle()
    {
        $this->policy_set->create([
            'hmo_id' => $this->header('OrganizationProfileId'),
            'company_id' => (new OrganizationProfile())->where('slug', $this->company_slug)->firstOrFail()->id,
            'name' => $this->name,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'is_active' => $this->is_active,
        ]);
    }

}

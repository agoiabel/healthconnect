<?php

namespace App\Http\Requests;

use App\Service;
use App\Http\Requests\Request;

class ServiceFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:services',
            'code' => 'required|unique:services',
            'is_active' => 'required',
        ];
    }

    /**
     * Handle the processing of service
     * 
     * @return
     */
    public function handle(Service $service)
    {
        $service->create([
            'name' => $this->name,
            'code' => $this->code,
            'is_active' => $this->is_active,
        ]);       
    }

}

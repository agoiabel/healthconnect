<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TableColumnRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model_name' => 'required',
            'column_name' => 'required',
            'column_id' => 'required',
            'column_value' => 'required',
        ];
    }

    public static $tableLookup = [
        'OrganizationProfile' => \App\Classes\Update\HandleUpdatingOrganizationProfile::class,
        'CompanyHmo' => \App\Classes\Update\HandleUpdatingCompanyHmo::class,
        'HmoProvider' => \App\Classes\Update\HandleUpdatingHmoProvider::class,
        'OrganizationProfileModerator' => \App\Classes\Update\HandleUpdatingOrganizationProfileModerator::class,
    ];

    /**
     * Handle the process of updating table
     * 
     * @return 
     */
    public function handle()
    {
        return (new static::$tableLookup[$this->model_name])->handle($this);
    }
    
}

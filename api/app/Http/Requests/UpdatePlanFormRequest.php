<?php

namespace App\Http\Requests;

use App\Plan;
use App\Http\Requests\Request;

class UpdatePlanFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required',
            'name' => 'required',
            'code' => 'required',
            'is_active' => 'required',
        ];    
    }

    /**
     * Handle the process of updating plan
     * 
     * @return 
     */
    public function handle(Plan $plan)
    {
        $plan->where('slug', $this->slug)->firstOrFail()->update([
            'name' => $this->name,
            'code' => $this->code,
            'is_active' => $this->is_active,
        ]);
    }
}

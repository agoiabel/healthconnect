<?php

namespace App\Http\Requests;

use App\Policy;
use App\PolicySet;
use App\Http\Requests\Request;

class UpdatePolicyFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_active' => 'required',
            'maximum_no_of_beneficiary_dependant' => 'required',
            'name' => 'required',
            'plan_id' => 'required',
            'policyset_slug' => 'required',
        ];
    }

    /**
     * Handle the process of updating policy
     * 
     * @param  Policy $policy 
     * @return 
     */
    public function handle(Policy $policy)
    {
        return $policy->where('slug', $this->policy_slug)->firstOrFail()->update([
            'policy_set_id' => (new PolicySet())->where('slug', $this->policyset_slug)->firstOrFail()->id,
            'plan_id' => $this->plan_id,
            'is_active' => $this->is_active,
            'name' => $this->name,
            'maximum_no_of_beneficiary_dependant' => $this->maximum_no_of_beneficiary_dependant,
        ]);
    }
}

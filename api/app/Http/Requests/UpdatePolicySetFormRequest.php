<?php

namespace App\Http\Requests;

use App\Policyset;
use App\OrganizationProfile;
use App\Http\Requests\Request;

class UpdatePolicySetFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'policyset_slug' => 'required',
            'name' => 'required',
            'is_active' => 'required',
            'start_date' => 'required|date',
            'end_date' => [
                'required',
                'date',
                'after:start_date',
            ]
        ];
    }

    /**
     * Handle the updating of policyset
     * 
     * @param  Policyset $policyset 
     * @return 
     */
    public function handle(Policyset $policyset)
    {
        $policyset->where('slug', $this->policyset_slug)->firstOrFail()->update([
            'hmo_id' => $this->header('OrganizationProfileId'),
            'company_id' => (new OrganizationProfile())->where('slug', $this->company_slug)->firstOrFail()->id,
            'name' => $this->name,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'is_active' => $this->is_active,
        ]);
    }

}

<?php

namespace App\Http\Requests;

use App\Service;
use App\Http\Requests\Request;

class UpdateServiceFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required',
            'name' => 'required',
            'is_active' => 'required',
            'code' => 'required',
        ];
    }

    /**
     * Handle the process of updating service
     * 
     * @return 
     */
    public function handle(Service $service)
    {
        $service->where('slug', $this->slug)->firstOrFail()->update([
            'name' => $this->name,
            'code' => $this->code,
            'is_active' => $this->is_active,
        ]);
    }
}

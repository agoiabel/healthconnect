<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', ['as' => 'index', function () {
    return view('welcome');
}]);


Route::group(['middleware' => 'cors', 'prefix' => 'api'], function () {

	post('login', ['as' => 'postLogin', 'uses' => 'LoginController@store']);

	/**
	 * Needs token authorization
	 */
	Route::group(['middleware' => 'apiAuth'], function () {

		//handle only routes that needs only authentication without permissions
		


		Route::group(['middleware' => ['permission', 'active_organization', 'active_moderator']], function () {

			// post('image/store', ['as' => 'image.post', 'uses' => 'ImageController@store']);

			post('database-column-update', ['as' => 'database-column-update', 'uses' => 'TableController@column']);
			
			get('organization/index', ['as' => 'organization.index', 'uses' => 'OrganizationController@index']);
			get('state/index', ['as' => 'state.index', 'uses' => 'StateController@index']);
			get('user/index', ['as' => 'user.index', 'uses' => 'UserController@index']);

			get('hmo/company', ['as' => 'hmo.company', 'uses' => 'HmoController@company']);
			get('hmo/provider', ['as' => 'hmo.provider', 'uses' => 'HmoController@provider']);

			get('all_company', ['as' => 'all_company', 'uses' => 'HmoController@all_company']);
			get('all_provider', ['as' => 'all_provider', 'uses' => 'HmoController@all_provider']);
			get('all_hmo', ['as' => 'all_hmo', 'uses' => 'HmoController@all_hmo']);
			get('all_organization', ['as' => 'all_organization', 'uses' => 'HmoController@all_organization']);

			get('company/policyset', ['as' => 'company.policyset', 'uses' => 'CompanyController@policyset']);

			//handle only routes that needs only permissions
			Route::group(['prefix' => 'service'], function () {

				get('index', ['as' => 'service.index', 'uses' => 'ServiceController@index']);
				get('delete/{id}', ['as' => 'service.delete', 'uses' => 'ServiceController@delete']);
				post('store', ['as' => 'service.store', 'uses' => 'ServiceController@store']);
				post('update', ['as' => 'service.update', 'uses' => 'ServiceController@update']);
				get('show/{slug}', ['as' => 'service.show', 'uses' => 'ServiceController@show']);

			});

			
			Route::group(['prefix' => 'organization-profile'], function () {

				get('index', ['as' => 'organization-profile.index', 'uses' => 'OrganizationProfileController@index']);
				post('store', ['as' => 'organization-profile.store', 'uses' => 'OrganizationProfileController@store']);

			});
			
			Route::group(['prefix' => 'beneficiary'], function () {

				post('store', ['as' => 'beneficiary.store', 'uses' => 'BeneficiaryController@store']);
				get('index', ['as' => 'beneficiary.index', 'uses' => 'BeneficiaryController@index']);

			});

			Route::group(['prefix' => 'dependent'], function () {

				get('index', ['as' => 'dependent.index', 'uses' => 'DependentController@index']);
				post('store', ['as' => 'dependent.store', 'uses' => 'DependentController@store']);

			});


			Route::group(['prefix' => 'moderator'], function () {

				get('index', ['as' => 'moderator.index', 'uses' => 'ModeratorController@index']);
				post('store', ['as' => 'moderator.store', 'uses' => 'ModeratorController@store']);

			});

			Route::group(['prefix' => 'plan'], function () {

				get('index', ['as' => 'plan.index', 'uses' => 'PlanController@index']);
				post('store', ['as' => 'plan.store', 'uses' => 'PlanController@store']);
				get('delete/{id}', ['as' => 'plan.delete', 'uses' => 'PlanController@delete']);
				get('show/{slug}', ['as' => 'plan.show', 'uses' => 'PlanController@show']);
				post('update', ['as' => 'plan.update', 'uses' => 'PlanController@update']);

				get('add-service/{plan_slug}', ['as' => 'plan.add-service', 'uses' => 'PlanController@addService']);
				post('add-service-price', ['as' => 'plan.add-service-price', 'uses' => 'PlanController@addServicePrice']);
				
				get('view-service/{plan_slug}', ['as' => 'plan.view-service', 'uses' => 'PlanController@viewService']);
				get('delete-service-price/{plan_service_id}', ['as' => 'plan.delete-service-price', 'uses' => 'PlanController@deleteServicePrice']);

			});
			
			Route::group(['prefix' => 'policyset'], function () {

				post('store', ['as' => 'policy_set.store', 'uses' => 'PolicysetController@store']);
				get('index/{companySlug}', ['as' => 'policy_set.index', 'uses' => 'PolicysetController@index']);
				get('delete/{policy_set_id}', ['as' => 'policy_set.delete', 'uses' => 'PolicysetController@delete']);
				get('show/{slug}', ['as' => 'policy_set.show', 'uses' => 'PolicysetController@show']);
				post('update', ['as' => 'policy_set.update', 'uses' => 'PolicysetController@update']);

			});


			Route::group(['prefix' => 'policy'], function () {

				post('store', ['as' => 'policy.store', 'uses' => 'PolicyController@store']);
				get('index/{policysetSlug}', ['as' => 'policy.index', 'uses' => 'PolicyController@index']);
				get('delete/{policy_id}', ['as' => 'policy_.delete', 'uses' => 'PolicyController@delete']);
				get('show/{slug}', ['as' => 'policy.show', 'uses' => 'PolicyController@show']);
				post('update', ['as' => 'policy.update', 'uses' => 'PolicyController@update']);
				
			});
	
		});


	});

	
});
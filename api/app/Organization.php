<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends myModel
{

    const HMO = 1;
    const PROVIDER = 2;
    const COMPANY = 3;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable  = ['name', 'description'];

    /**
     * an organization has one profile
     * 
     * @return 
     */
	public function profiles()
    {
    	return $this->hasMany(OrganizationProfile::class, 'organization_profile_id');
    }    

    /**
     * An organization has many hmos
     * 
     * @return 
     */
    public function hmoProviders()
    {
        return $this->hasMany(HmoProvider::class, 'hmo_id');    
    }

    /**
     * An organization has many providers
     * 
     * @return 
     */
    public function providerHmos()
    {
        return $this->hasMany(HmoProvider::class, 'provider_id');    
    }

    /**
     * an 
     * 
     * @return 
     */
    public function hmoCompanies()
    {
       return $this->hasMany(CompanyHmo::class, 'hmo_id'); 
    }

    /**
     * an hmo has many companies
     * 
     * @return 
     */
    public function companyHmos()
    {
        return $this->hasMany(CompanyHmo::class, 'company_id');    
    }

}

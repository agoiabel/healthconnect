<?php

namespace App;

use App\Classes\Model\Sluggable;
use Illuminate\Database\Eloquent\Model;

class OrganizationProfile extends myModel
{
    use Sluggable;

    const ACTIVE = true;
    const INACTIVE = false;

    /**
     * attr that can be mass assigned
     * 
     * @var [type]
     */
    protected $fillable = ['owner_id', 'organization_id', 'organization_profile_state_id', 'name', 'bank_name', 'account_name', 'account_number', 'is_active', 'code', 'slug', 'logo', 'organization_address'];

    /**
     * an organization profile belongs to an organization
     * 
     * @return 
     */
    public function organization()
    {
    	return $this->belongsTo(Organization::class, 'organization_id');	
    }

    /**
     * an organization profile belongs to a state
     * 
     * @return 
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'organization_profile_state_id');
    }

    /**
     * an organization profile belongs to a user
     * 
     * @return 
     */
    public function owner()
    {
    	return $this->belongsTo(User::class, 'owner_id');	
    }

    /**
     * an organization profile has many role_user
     * 
     * @return 
     */
    public function role_users()
    {
        return $this->hasMany(RoleUser::class, 'organization_profile_id');    
    }

    /**
     * an organization profile has one user_profile
     * 
     * @return 
     */
    public function user_profile()
    {
        return $this->hasOne(UserProfile::class, 'organization_profile_id');
    }

    /**
     * an organization profile (hmo) has many plans
     * 
     * @return 
     */
    public function plans()
    {
        return $this->hasMany(Plan::class, 'hmo_id');
    }
        
    /**
     * a company organization profile has many policysets
     * 
     * @return 
     */
    public function hmoPolicySets()
    {
        return $this->hasMany(PolicySet::class, 'hmo_id');
    }

    /**
     * a company organization profile has many policysets
     * 
     * @return 
     */
    public function companyPolicySets()
    {
        return $this->hasMany(PolicySet::class, 'company_id');
    }

    /**
     * an organizationProfile has many organizationProfileModerators
     * 
     * @return 
     */
    public function organization_profile_moderators()
    {
        return $this->hasMany(OrganizationProfileModerator::class, 'organization_profile_id');    
    }

}

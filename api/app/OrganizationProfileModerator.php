<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationProfileModerator extends myModel
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['moderator_id', 'organization_profile_id', 'is_active'];

    /**
     * an organizationProfileModerator belongs to a moderator
     * 
     * @return 
     */
    public function moderator()
    {
    	return $this->belongsTo(User::class, 'moderator_id');
    }

    /**
     * an organizationProfileModerator belongs to an organizationProfile
     * 
     * @return 
     */
    public function organization_profile()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'organization_profile_id');
    }
    
}

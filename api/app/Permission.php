<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends myModel
{
    const SHOW_ON_NAVBAR = TRUE;
    const DO_NOT_SHOW_ON_NAVBAR = FALSE;
    
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['name', 'url', 'icon', 'show_on_navbar'];

    /**
     * a permission has many permission roles
     * 
     * @return 
     */
	public function permissionRoles()
    {
    	return $this->hasMany(PermissionRole::class, 'permission_id');	
    }

    /**
     * a permission has many childs
     * 
     * @return 
     */
    public function permissionChildren()
    {
        return $this->hasMany(PermissionChild::class, 'permission_id');
    }

}

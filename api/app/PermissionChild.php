<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionChild extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['permission_id', 'name', 'url'];

    /**
     * permission child belongs to a permission
     * 
     * @return 
     */
    public function permission()
    {
    	return $this->belongsTo(Permission::class, 'permission_id');
    }
}

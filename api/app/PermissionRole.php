<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends myModel
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['role_id', 'permission_id'];

    /**
     * a permissionRole belongs to a role
     * 
     * @return 
     */
    public function role()
    {
    	return $this->belongsTo(Role::class, 'role_id');	
    }

    /**
     * a permissionRole belongs to a permission
     * 
     * @return 
     */
    public function permission()
    {
    	return $this->belongsTo(Permission::class, 'permission_id');	
    }

}

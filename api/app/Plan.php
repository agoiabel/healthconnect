<?php

namespace App;

use App\Classes\Model\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Plan extends myModel
{
    use Sluggable;

    const active = true;
    const inactive = true;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['slug', 'hmo_id', 'name', 'slug', 'code', 'is_active'];

    /**
     * a plan belongs to an hmo
     * 
     * @return 
     */
    public function hmo()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'hmo_id');	
    }

    /**
     * a plan belongs to many services
     * 
     * @return 
     */
    public function services()
    {
        return $this->hasMany(PlanService::class, 'plan_id');
    }
}

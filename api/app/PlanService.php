<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanService extends myModel
{
	
    /**
     * attr that can be mass assigned
     * 
     * @var []
     */
    protected $fillable = ['service_id', 'plan_id', 'price'];

    /**
     * a plan service belongs to a service
     * 
     * @return 
     */
    public function service()
    {
    	return $this->belongsTo(Service::class, 'service_id');	
    }

    /**
     * a plan service belongs to a plan
     * 
     * @return 
     */
    public function plan()
    {
    	return $this->belongsTo(Plan::class, 'plan_id');
    }

}

<?php

namespace App;

use App\Classes\Model\Sluggable;
use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class Policy extends myModel
{
    use Sluggable;

    /**
     * attr that can be mass assigned
     * 
     * @var 
     */
    protected $fillable = ['slug', 'plan_id', 'policy_set_id', 'name', 'maximum_no_of_beneficiary_dependant', 'is_active'];

    /**
     * a policy belongs to a policy set
     * 
     * @return 
     */
    public function policySet()
    {
        return $this->belongsTo(PolicySet::class, 'policy_set_id');
    }

    /**
     * A policy belongs to a plan
     * 
     * @return 
     */
    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }
        
}

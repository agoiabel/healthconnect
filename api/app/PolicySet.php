<?php

namespace App;

use Carbon\Carbon;
use App\Classes\Model\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PolicySet extends myModel
{
	use Sluggable;

	protected $date = ['start_date', 'end_date'];

    //a policyset should belong to a company_id and hmo_id

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['slug', 'company_id', 'hmo_id', 'name', 'start_date', 'end_date', 'is_active'];

    /**
     * a policyset belongs to a company
     * 
     * @return 
     */
    public function company()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'company_id');
    }

    /**
     * a policy set belongs to an hmo
     * 
     * @return 
     */
    public function hmo()
    {
        return $this->belongsTo(OrganizationProfile::class, 'hmo_id');
    }

    /**
     * a policysets has many policies
     * 
     * @return 
     */
    public function policies()
    {
        return $this->hasMany(Policy::class, 'policy_set_id');
    }

    /**
     * Change published at to carbon instance
     */
    public function setStartDateAttribute($date)
    {
        $this->attributes['start_date'] = Carbon::parse($date);
    }

    /**
     * Change published at to carbon instance
     */
    public function setEndDateAttribute($date)
    {
        $this->attributes['end_date'] = Carbon::parse($date);
    }

}

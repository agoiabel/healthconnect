<?php

namespace App;
use App\Classes\Model\Sluggable;

class Role extends myModel
{
	use Sluggable;

    const SUPER_ADMIN = 1;
    const ADMIN = 2;
    const HMO_OWNER = 3;
    const HMO_MODERATOR = 4;
    const PROVIDER_OWNER = 5;
    const PROVIDER_MODERATOR = 6;
    const COMPANY_OWNER = 7;
    const COMPANY_MODERATOR = 8;
    const BENEFICIARY = 9;
    const BENEFICIARY_DEPENDENT = 10;
    const BENEFICIARY_NEXT_OF_KIN = 11;

	
	/**
	 * attr that can be mass assigned
	 * 
	 * @var array
	 */
    protected $fillable = ['slug', 'name'];

    /**
     * a role has many role_users
     * 
     * @return 
     */
    public function role_users()
    {
        return $this->hasMany(RoleUser::class, 'role_id');    
    }    

    /**
     * a role has many permissionRoles
     * 
     * @return 
     */
    public function permissionRoles()
    {
        return $this->hasMany(PermissionRole::class, 'role_id');    
    }


    /**
     * check if user is super admin
     * 
     * @return boolean
     */
    public function isAdmin($client_role_id)
    {
        if ($client_role_id == self::ADMIN) {
            return true;
        }
        return false;   
    }

    /**
     * check if user is super admin
     * 
     * @return boolean
     */
    public function isSuperAdmin($user_id)
    {
        if ($user_id == self::SUPER_ADMIN) {
            return true;
        }
        return false;   
    }

    /**
     * check if user is hmo moderator
     * 
     * @param  $user_id 
     * @return          
     */
    public function hmoModerator($user_id)
    {
        if ($user_id == self::HMO_MODERATOR) {
            return true;
        }
        return false;      
    }

    /**
     * check if user is company moderator
     * 
     * @param  $user_id 
     * @return          
     */
    public function companyModerator($user_id)
    {
        if ($user_id == self::COMPANY_MODERATOR) {
            return true;
        }
        return false;      
    }

    /**
     * check if user is provider moderator
     * 
     * @param  $user_id 
     * @return          
     */
    public function providerModerator($user_id)
    {
        if ($user_id == self::PROVIDER_MODERATOR) {
            return true;
        }
        return false;
    }

}

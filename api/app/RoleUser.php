<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends myModel
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['role_id', 'role_slug', 'user_id', 'user_slug', 'organization_profile_id', 'organization_profile_slug', 'is_active'];

    /**
     * a role_user belongs to a user
     * 
     * @return 
     */
	public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }    

    /**
     * a role_user belongs to a role
     * 
     * @return 
     */
    public function role()
    {
    	return $this->belongsTo(Role::class, 'role_id');
    }

    /**
     * a role_user belongs to an organization_profile
     * 
     * @return 
     */
    public function organization_profile()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'organization_profile_id');	
    }

    
}

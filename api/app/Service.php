<?php

namespace App;

use App\Classes\Model\Sluggable;

class Service extends myModel
{
	use Sluggable;

	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['slug', 'name', 'code', 'is_active'];

    /**
     * a plan belongs to many services
     * 
     * @return 
     */
    public function plans()
    {
        return $this->hasMany(PlanService::class, 'plan_id');
    }
    
}

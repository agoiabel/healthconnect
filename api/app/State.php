<?php

namespace App;

use App\Classes\Model\Sluggable;

class State extends myModel
{
	use Sluggable;
	
	/**
	 * attr that can be mass assigned
	 * 
	 * @var [type]
	 */
    protected $fillable = ['slug', 'name'];

    /**
     * a state has many users
     * 
     * @return 
     */
    public function users()
    {
    	return $this->hasMany(User::class, 'state_id');
    }

    /**
     * a state has many organization profile 
     * 
     * @return 
     */
    public function organization_profiles()
    {
        return $this->hasMany(OrganizationProfile::class, 'organization_profile_state_id');
    }
    
}

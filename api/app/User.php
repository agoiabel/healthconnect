<?php

namespace App;

use Carbon\Carbon;
use App\Classes\Model\Sluggable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends myModel implements AuthenticatableContract, CanResetPasswordContract
{
    use Sluggable, Authenticatable, CanResetPassword;


    /**
     * Generate slug from
     * 
     * @return 
     */
    public function getSluggableString()
    {
        return $this->first_name .'-'.$this->last_name;
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * carbon instance
     * 
     * @var array
     */
    protected $dates = ['date_of_birth'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'middle_name', 'last_name', 'date_of_birth', 'primary_phone_number', 'primary_profile_picture', 'gender', 'email', 'password', 'authorization', 'state_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * mutate password column
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Change published at to carbon instance
     */
    public function setDateOfBirthAttribute($date)
    {
        $this->attributes['date_of_birth'] = Carbon::parse($date);
    }

    /**
     * a user has many role_users
     * 
     * @return 
     */
    public function role_users()
    {
        return $this->hasMany(RoleUser::class, 'user_id');
    }

    /**
     * a user has many organization profiles
     * 
     * @return 
     */
    public function organization_profiles()
    {
        return $this->hasMany(OrganizationProfile::class, 'owner_id');
    }

    /**
     * a user has many profiles
     * 
     * @return 
     */
    public function user_profiles()
    {
        return $this->hasMany(UserProfile::class, 'user_id');
    }
            
    /**
     * a user belongs to a state
     * 
     * @return 
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    /**
     * a user has many organization profile
     * 
     * @return 
     */
    public function organization_profile_moderators()
    {
        return $this->hasMany(OrganizationProfileModerator::class, 'moderator_id');
    }
}

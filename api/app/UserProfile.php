<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
	/**
	 * attr that can be mass assigned
	 * 
	 * @var []
	 */
    protected $fillable = ['user_id', 'organization_profile_id', 'picture', 'address', 'job_name'];

    /**
     * a user_profile belongs to a user
     * 
     * @return 
     */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');	
    }

    /**
     * a user_profile belongs to an organization_profile
     * 
     * @return 
     */
    public function organization_profile()
    {
    	return $this->belongsTo(OrganizationProfile::class, 'organization_profile_id');
    }

}

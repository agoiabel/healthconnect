<?php

namespace App;

use App\Classes\Model\ModelHelper;
use Illuminate\Database\Eloquent\Model;

class myModel extends Model
{
	use ModelHelper;

	const ACTIVE = true;
	const IN_ACTIVE = false;

	/**
	 * Generate slug from
	 * 
	 * @return 
	 */
	public function getSluggableString()
	{
		return $this->name;
	}

}

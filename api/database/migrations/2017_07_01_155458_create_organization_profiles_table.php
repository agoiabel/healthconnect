<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_profiles', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug');

            $table->integer('organization_id')->unsigned();
            $table->integer('organization_profile_state_id')->unsigned();
            $table->integer('owner_id')->unsigned();

            $table->string('name')->unique();

            $table->string('bank_name');
            $table->string('account_number');
            $table->string('account_name');
            
            $table->string('organization_address');

            $table->string('code');
            $table->string('logo');
            $table->boolean('is_active')->default(true);

            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
            $table->foreign('organization_profile_state_id')->references('id')->on('states')->onDelete('cascade');

            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organization_profiles');
    }
}

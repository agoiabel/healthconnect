<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicySetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hmo_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->string('slug');
            $table->string('name');
            $table->boolean('is_active')->default(false);
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->timestamps();

            $table->foreign('hmo_id')->references('id')->on('organization_profiles')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('organization_profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('policy_sets');
    }
}

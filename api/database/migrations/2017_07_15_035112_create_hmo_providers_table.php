<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHmoProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hmo_providers', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('hmo_id')->unsigned();
            $table->foreign('hmo_id')->references('id')->on('organization_profiles')->onDelete('cascade');

            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('organization_profiles')->onDelete('cascade');

            $table->boolean('is_active')->default(true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hmo_providers');
    }
}

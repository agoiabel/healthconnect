<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationProfileModeratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_profile_moderators', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('moderator_id')->unsigned();
            $table->foreign('moderator_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('organization_profile_id')->unsigned();
            $table->foreign('organization_profile_id')->references('id')->on('organization_profiles')->onDelete('cascade');

            $table->boolean('is_active')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organization_profile_moderators');
    }
}

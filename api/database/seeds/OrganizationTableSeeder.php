<?php

use App\Organization;
use Illuminate\Database\Seeder;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organizations = [
        	[
                'name' => 'HMO',
                'description' => 'health management organization'
            ],
            [
                'name' => 'PROVIDER',
                'description' => 'hospitals'
            ],
            [
                'name' => 'COMPANY',
                'description' => 'company that are clients of an HMO'
            ],
        ];

        foreach ($organizations as $key => $organization) {
        	Organization::create($organization);
        }
        
    }
}

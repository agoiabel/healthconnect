<?php

use App\PermissionChild;
use Illuminate\Database\Seeder;

class PermissionChildrenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
        	[
                'permission_id' => '1',
        		'url' => 'secured.permission.organization-create',
        		'name' => 'Create Organization'
        	],

            [
                'permission_id' => '2',
                'url' => 'secured.permission.service-index',
                'name' => 'All Service'
            ],
            [
                'permission_id' => '2',
                'url' => 'secured.permission.service-create',
                'name' => 'Create Service'
            ],

        	[
                'permission_id' => '3',
        		'url' => 'secured.permission.moderator-index',
        		'name' => 'All Moderator'
        	],
            [
                'permission_id' => '3',
                'url' => 'secured.permission.moderator-create',
                'name' => 'Create Moderator'
            ],

        	[
                'permission_id' => '4',
        		'url' => 'secured.permission.policyset-index',
        		'name' => 'All PolicySet'
        	],
            [
                'permission_id' => '4',
                'url' => 'secured.permission.policyset-create',
                'name' => 'Create PolicySet'
            ],


        	[
                'permission_id' => '5',
        		'url' => 'secured.permission.plan-index',
        		'name' => 'All Plan'
        	],
            [
                'permission_id' => '5',
                'url' => 'secured.permission.plan-create',
                'name' => 'Create Plan'
            ],

        	[
                'permission_id' => '6',
        		'url' => 'secured.permission.beneficiary-index',
        		'name' => 'All Beneficiary'
        	],
            [
                'permission_id' => '6',
                'url' => 'secured.permission.beneficiary-create',
                'name' => 'Create Beneficiary'
            ],

        	[
                'permission_id' => '7',
        		'url' => 'secured.permission.dependent-index',
        		'name' => 'All Dependent'
        	],
            [
                'permission_id' => '7',
                'url' => 'secured.permission.dependent-create',
                'name' => 'Create Dependent'
            ],

            [
                'permission_id' => '8',
                'url' => 'secured.permission.hmo-company',
                'name' => 'View Companies'
            ],

            [
                'permission_id' => '9',
                'url' => 'secured.permission.hmo-provider',
                'name' => 'View Providers'
            ],

            [
                'permission_id' => '10',
                'url' => 'secured.permission.all-organization',
                'name' => 'All Organizations'
            ],
            [
                'permission_id' => '10',
                'url' => 'secured.permission.all-hmo',
                'name' => 'All Hmos'
            ],
            [
                'permission_id' => '10',
                'url' => 'secured.permission.all-company',
                'name' => 'All Companies'
            ],
            [
                'permission_id' => '10',
                'url' => 'secured.permission.all-provider',
                'name' => 'All Providers'
            ],

            [
                'permission_id' => '11',
                'url' => 'secured.permission.policy-index',
                'name' => 'All Policies'
            ],
            [
                'permission_id' => '11',
                'url' => 'secured.permission.policy-create',
                'name' => 'Create Policy'
            ],

        ];

        foreach ($permissions as $key => $permission) {
        	PermissionChild::create($permission);
        }
    }
}

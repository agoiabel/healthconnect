<?php

use App\Role;
use App\Permission;
use App\PermissionRole;
use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach (Permission::all() as $key => $permission) {
    			
    		#hmo.index
    		if ($permission->id == '1') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::SUPER_ADMIN,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::ADMIN,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_MODERATOR,
		        ]);
    		}

    		#service.index
    		if ($permission->id == '2') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::SUPER_ADMIN,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::ADMIN,
		        ]);
    		}

    		#moderator.index
    		if ($permission->id == '3') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::PROVIDER_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::COMPANY_OWNER,
		        ]);
    		}


    		#moderator.index
    		if ($permission->id == '4') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_MODERATOR,
		        ]);
    		}


    		#plan.index
    		if ($permission->id == '5') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_MODERATOR,
		        ]);
    		}


    		#beneficiary.index
    		if ($permission->id == '6') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::COMPANY_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::COMPANY_MODERATOR,
		        ]);
    		}

    		#dependent.index
    		if ($permission->id == '7') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::BENEFICIARY,
		        ]);
    		}

    		#dependent.index
    		if ($permission->id == '8') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_MODERATOR,
		        ]);
    		}

    		#dependent.index
    		if ($permission->id == '9') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_MODERATOR,
		        ]);
    		}

    		#dependent.index
    		if ($permission->id == '10') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::SUPER_ADMIN,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::ADMIN,
		        ]);
    		}

    		#policy
    		if ($permission->id == '11') {
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_OWNER,
		        ]);
		        PermissionRole::create([
		        	'permission_id' => $permission->id,
		        	'role_id' => Role::HMO_MODERATOR,
		        ]);
    		}
    	}

    }
}

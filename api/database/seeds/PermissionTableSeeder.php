<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
        	[#1
                'icon' => 'icon s7-user',
                'name' => 'Organization',
        		'url' => 'secured.permission.organization-create'
        	],

        	[#2
                'icon' => 'icon s7-user',
        		'name' => 'Service',
                'url' => 'secured.permission.service-index'
        	],

        	[#3
                'icon' => 'icon s7-user',
        		'name' => 'Moderator',
                'url' => 'secured.permission.moderator-index'
        	],

        	[#4
                'icon' => 'icon s7-user',
        		'name' => 'PolicySet',
                'url' => 'secured.permission.policyset-index',
                'show_on_navbar' => Permission::DO_NOT_SHOW_ON_NAVBAR
        	],

        	[#5
                'icon' => 'icon s7-user',
        		'name' => 'Plan',
                'url' => 'secured.permission.plan-index',
        	],

        	[#6
                'icon' => 'icon s7-user',
        		'name' => 'Beneficiary',
                'url' => 'secured.permission.beneficiary-index'
        	],
            
        	[#7
                'icon' => 'icon s7-user',
        		'name' => 'Dependent',
                'url' => 'secured.permission.dependent-index'
        	],

            [#8
                'icon' => 'icon s7-user',
                'name' => 'View Companies',
                'url' => 'secured.permission.hmo-company'
            ],

            [#9
                'icon' => 'icon s7-user',
                'name' => 'View Providers',
                'url' => 'secured.permission.hmo-provider'
            ],

            [#10
                'icon' => 'icon s7-user',
                'name' => 'View Organizations',
                'url' => 'secured.permission.all-organization'
            ],

            [#11
                'icon' => 'icon s7-user',
                'name' => 'Policy',
                'url' => 'secured.permission.policy-index',
                'show_on_navbar' => Permission::DO_NOT_SHOW_ON_NAVBAR
            ],
        ];

        foreach ($permissions as $key => $permission) {
        	Permission::create($permission);
        }
    }
}

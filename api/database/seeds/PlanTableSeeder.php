<?php

use App\Plan;
use App\OrganizationProfile;
use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();

		foreach (range(1, 20) as $key => $value) {
			Plan::create([
				'hmo_id' => OrganizationProfile::find(1)->id,
				'name' => $faker->name,
				'code' => $faker->postcode
			]);
		}
    }
}

<?php

use App\Policy;
use App\PolicySet;
use Illuminate\Database\Seeder;

class PolicysetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();

		foreach (range(1, 20) as $key => $value) {
			PolicySet::create([
				'hmo_id' => OrganizationProfile::find(1)->id,
				'name' => $faker->name,
				'code' => $faker->postcode
			]);
		}
 
    }
}

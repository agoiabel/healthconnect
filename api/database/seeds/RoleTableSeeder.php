<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	['name' => 'SUPER_ADMIN'],
        	['name' => 'ADMIN'],
        	['name' => 'HMO_OWNER'],
        	['name' => 'HMO_MODERATOR'],
        	['name' => 'PROVIDER_OWNER'],
        	['name' => 'PROVIDER_MODERATOR'],
            ['name' => 'COMPANY_OWNER'],
            ['name' => 'COMPANY_MODERATOR'],
        	['name' => 'BENEFICIARY'],
        	['name' => 'BENEFICIARY_DEPENDENT'],
        ];

        foreach ($roles as $key => $role) {
        	Role::create($role);
        }
        
    }
}

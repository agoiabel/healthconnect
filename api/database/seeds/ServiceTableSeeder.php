<?php

use App\Service;
use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();

		foreach (range(1, 100) as $key => $value) {
			Service::create([
				'name' => $faker->name,
				'code' => $faker->postcode
			]);
		}
    }
}

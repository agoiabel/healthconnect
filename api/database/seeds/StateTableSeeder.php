<?php

use App\State;
use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();

		foreach (range(1, 25) as $key => $value) {
			State::create([
				'name' => $faker->state
			]);
		}
    }
}

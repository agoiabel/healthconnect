<?php

use App\User;
use App\Role;
use App\State;
use App\RoleUser;
use App\UserProfile;
use App\Organization;
use App\BeneficiaryCompany;
use App\OrganizationProfile;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();

        $gender = ['MALE', 'FEMALE'];

        /**
         * Admin user
         * 
         * @var 
         */
        $user = User::create([
        	'first_name' => $faker->firstName,
        	'middle_name' => $faker->firstName,
        	'last_name' => $faker->lastName,
        	'email' => 'johndoe@admin.com',
        	'password' => 'abc',
            'state_id' => State::first()->id,
            'gender' => $gender[shuffle($gender)],
            'date_of_birth' => $faker->date,
            'primary_phone_number' => $faker->e164PhoneNumber,
            'primary_profile_picture' => 'assets/img/avatars/img3.jpg'
        ]);
        UserProfile::create([
            'user_id' => $user->id,
            //'picture' => 'img/doctor-pic-01.jpg'
            'picture' => 'assets/img/avatars/img3.jpg'
        ]);
        RoleUser::create([
        	'role_id' => Role::ADMIN,
        	'role_slug' => Role::where('id', Role::ADMIN)->first()->slug,
        	'user_id' => $user->id,
        	'user_slug' => $user->slug,
        ]);


    }
}

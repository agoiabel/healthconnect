var hmoApplication = angular.module('hmoApplication', [
		'authentication', 
		'secured',
		'permission',
		'dashboard',
		'mainDashboard',
		'service',
		'plan',
		'policyset',
		'organization',
		'directives',
		'policy',
		'beneficiary',
		'moderator',
		'dependent',
]);

hmoApplication.constant('config', {  
  apiUrl: 'http://hmoapi.dev/api/',
  baseUrl: '/',
  enableDebug: true,
  SUPER_ADMIN: 1,
  ADMIN: 2,
  PROVIDER: 2,
  COMPANY: 3,
});

hmoApplication.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/login')
}]);


hmoApplication.run(['$rootScope', 'authenticationService', '$state', 'localstorageService', 'checkService', '$timeout', 
	function ($rootScope, authenticationService, $state, localstorageService, checkService, $timeout) {

	$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

		console.log('about to reload page');
		// console.log($state);

		//needs authentication
		if (toState.authentication) {
			//the user is not authenticated
			if (! authenticationService.isAuthenticated() ) {
				event.preventDefault();
				authenticationService.storeStateGuestWantToVisit(toState);
				$state.go('login');
			}

			//the user is authentication
			if ( authenticationService.isAuthenticated() ) {
				//i need to do my role/permissions here
				if (! checkService.forEmptyString(localstorageService.get('role_chosen')) ) {

					$timeout(function () {
						$rootScope.$broadcast('roleWasChosen', localstorageService.get('role_chosen'));					
					}, 500);

				}
				// console.log('needs authentication and already authenticated');
			}
		}


		//does not needs authentication 
		if (!toState.authentication) {
			//user is authenticated
			if ( authenticationService.isAuthenticated() ) {
				//redirect if the user wants to visit login page when authenticated
				if (toState.name == 'login') {
					event.preventDefault();
					$state.go('secured.dashboard');
				}
			}

			//user is not authenticated
			if (! authenticationService.isAuthenticated() ) {
				console.log('route does not need authentication, and the user is not authenticated');
			}
		}


	});

}]);
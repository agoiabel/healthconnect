var hmoApplication = angular.module('hmoApplication', ['authentication']);

var authentication = angular.module('authentication', ['ui.router']);

authentication.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('login', {
			url: '/login',
			templateUrl: 'modules/authentication/views/login.html',
			controller: 'authenticationController'
		});
}]);
var dashboard = angular.module('dashboard', ['ui.router']);

dashboard.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('dashboard', {
			url: '/dashboard',
			templateUrl: 'modules/dashboard/views/dashboard.html',
			controller: 'dashboardController'
		});
}]);
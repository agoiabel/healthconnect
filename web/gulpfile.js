var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

gulp.task('default', ['modules', 'controllers', 'services', 'directives', 'filters']);

gulp.task('modules', function () {
	gulp.src([
			'assets/js/app.module.js', 
			'modules/shared/services/services.module.js',
			'modules/shared/secured/secured.module.js',
			'modules/shared/permission/permission.module.js',
			'modules/authentication/authentication.module.js',
			'modules/dashboard/dashboard.module.js',
			'modules/main-dashboard/main-dashboard.module.js',
			'modules/service/service.module.js',
			'modules/plan/plan.module.js',
			'modules/policyset/policyset.module.js',
			'modules/policy/policy.module.js',
			'modules/organization/organization.module.js',
			'modules/shared/directives/directives.module.js',
			'modules/beneficiary/beneficiary.module.js',
			'modules/moderator/moderator.module.js',
			'modules/dependent/dependent.module.js',
			'modules/shared/filters/filters.module.js',
		])
		.pipe(concat('modules.js'))
		.pipe(gulp.dest('scripts'));
});

gulp.task('controllers', function () {
	gulp.src([
			'modules/shared/secured/secured.controller.js',
			'modules/shared/permission/permission.controller.js',
			'modules/authentication/authentication.controller.js',
			'modules/dashboard/dashboard.controller.js',
			'modules/main-dashboard/main-dashboard.controller.js',
			'modules/organization/organization.controller.js',
			'modules/organization/hmo-provider.controller.js',
			'modules/organization/hmo-company.controller.js',
			
			'modules/organization/all-company.controller.js',
			'modules/organization/all-provider.controller.js',
			'modules/organization/all-hmo.controller.js',
			'modules/organization/all-organization.controller.js',

			'modules/service/service-index.controller.js',
			'modules/service/service-create.controller.js',
			'modules/service/service-edit.controller.js',
			
			'modules/plan/plan.controller.js',
			'modules/plan/plan-edit.controller.js',
			'modules/plan/plan-service.controller.js',
			'modules/plan/plan-view-service.controller.js',

			'modules/policyset/policyset-index.controller.js',
			'modules/policyset/policyset-create.controller.js',
			'modules/policyset/policyset-edit.controller.js',

			'modules/beneficiary/beneficiary-index.controller.js',
			'modules/beneficiary/beneficiary-create.controller.js',

			'modules/moderator/moderator-index.controller.js',
			'modules/moderator/moderator-create.controller.js',

			'modules/dependent/dependent-index.controller.js',
			'modules/dependent/dependent-create.controller.js',
			
			'modules/policy/policy-index.controller.js',
			'modules/policy/policy-create.controller.js',
			'modules/policy/policy-edit.controller.js',
		])
		.pipe(concat('controllers.js'))
		.pipe(gulp.dest('scripts'));
});

gulp.task('directives', function () {
	gulp.src([
			'modules/shared/directives/my-loading-spinner.directive.js',
			'modules/shared/directives/on-file-change.directive.js',
			'modules/shared/directives/unique-user-phonenumber.directive.js',
			'modules/shared/directives/organization-profile-uniqueness-for-code.directive.js',
		])
		.pipe(concat('directives.js'))
		.pipe(gulp.dest('scripts'));
});

gulp.task('filters', function () {
	gulp.src([
			// 'modules/shared/directives/date-formatter.directive.js',
		])
		.pipe(concat('directives.js'))
		.pipe(gulp.dest('scripts'));
});


gulp.task('services', function () {
	gulp.src([
			'modules/authentication/authentication.service.js',
			'modules/shared/services/sweetalert.service.js',
			'modules/shared/services/localstorage.service.js',
			'modules/shared/services/loading.service.js',
			'modules/shared/services/http.client.js',
			'modules/shared/services/check.service.js',
			'modules/service/service.service.js',
			'modules/organization/organization.service.js',
			'modules/plan/plan.service.js',
			'modules/policyset/policyset.service.js',
			'modules/policy/policy.service.js',
			'modules/beneficiary/beneficiary.service.js',
			'modules/moderator/moderator.service.js',
			'modules/dependent/dependent.service.js',
		])
		.pipe(concat('services.js'))
		.pipe(gulp.dest('scripts'));
});
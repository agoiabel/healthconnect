authentication.controller('authenticationController', ['$scope', 'httpClient', 'authenticationService', '$state', 'sweetalertService', 'localstorageService', 'checkService', 
	function ($scope, httpClient, authenticationService, $state, sweetalertService, localstorageService, checkService) {
		
	$scope.loginFormData = {
		email: '',
		password: ''
	}

	$scope.loading = false;

	$scope.submitLoginForm = function () {
		$scope.loading = true;

		httpClient.post($scope.loginFormData, 'login')
				  .then(
				  		function (response) {
				  			$scope.loading = false;
				  			localstorageService.set('authenticated', true);

						 	localstorageService.setObject('user', response.data.user);

							console.log('ready for redirection');

							sweetalertService.success('hi', 'welcome back');
							
							$state.go('secured.dashboard');
				  		},
				  		function (response) {
				  			$scope.loading = false;
						 	var error = { response };
						 	sweetalertService.error('bad login', error.response.data.error);
				  		}
				  );

	}




}]);
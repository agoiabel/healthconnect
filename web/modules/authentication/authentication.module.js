var authentication = angular.module('authentication', ['ui.router', 'services', 'secured']);

authentication.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('login', {
			url: '/login',
			templateUrl: 'modules/authentication/views/login.html',
			controller: 'authenticationController',
			authentication: false
		});
}]);
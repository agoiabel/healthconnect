authentication.factory('authenticationService', ['$state', 'httpClient', 'sweetalertService', 'localstorageService', 'checkService',
	function ($state, httpClient, sweetalertService, localstorageService, checkService) {
	
	var authenticationService = {};

	/**
	 * check if authenticated
	 * 
	 * @return {Boolean} 
	 */
	authenticationService.isAuthenticated = function () {
		return localstorageService.get('authenticated', false);
	}

	/**
	 * store the state the user wants to visit
	 * 
	 * @param  state 
	 * @return 
	 */
	authenticationService.storeStateGuestWantToVisit = function (state) {
		localstorageService.set('previous_route', state.name);
	}

	/**
	 * check the previous state
	 * 
	 * @return 
	 */
	authenticationService.checkPreviousRoute = function () {
		if ( checkService.forEmptyString(localstorageService.get('previous_route')) ) {
			return false;
		}
		return true;
	}

	/**
	 * Get previous route
	 * 
	 * @return 
	 */
	authenticationService.getPreviousRoute = function () {
		return localstorageService.get('previous_route');
	}

	/**
	 * redirect user on successful login
	 * 
	 * @return 
	 */
	authenticationService.redirectUser = function () {
		if ( authenticationService.isAuthenticated() ) {
			console.log('ready for redirection');

			sweetalertService.success('hi', 'welcome back');

			//check if there,s a storage previous_route
			if ( authenticationService.checkPreviousRoute() ) {
				var route = authenticationService.getPreviousRoute('previous_route');

				localstorageService.removeItem(route);

				return $state.go(route);
			}
			
			return $state.go('secured.dashboard');
		}
	}

	authenticationService.logout = function () {

		localstorageService.removeAll();

		sweetalertService.success('bye', 'see yha again');

		return $state.go('login');
	}


	return authenticationService;
}]);
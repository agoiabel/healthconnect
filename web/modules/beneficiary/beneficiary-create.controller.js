beneficiary.controller('beneficiaryCreateController', ['$scope', 'localstorageService', 'beneficiaryService', 'sweetalertService', '$state', 'config', '$http', '$q',
    function ($scope, localstorageService, beneficiaryService, sweetalertService, $state, config, $http, $q) {

    $scope.showLoading = true;

    $scope.beneficiaryFormData = {
      policy_id: '',

      first_name: '',
      middle_name: '',
      last_name: '',

      date_of_birth: '',
      gender: '',
      picture: '',
      primary_phone_number: '',
      state_id: '',

      address: '',
      job_name: '',
      code: '',
    }

    //Handle the display of the modal
    $scope.currentStep = 1;
    $scope.steps = [
      {
        step: 1,
        name: "First step",
        template: "modules/beneficiary/views/step1.html"
      },
      {
        step: 2,
        name: "Second step",
        template: "modules/beneficiary/views/step2.html"
      },   
      {
        step: 3,
        name: "Third step",
        template: "modules/beneficiary/views/step3.html"
      }
    ];

    $scope.gotoStep = function(newStep) {
      $scope.currentStep = newStep;
    }  
    $scope.getStepTemplate = function(){
      for (var i = 0; i < $scope.steps.length; i++) {
        if ($scope.currentStep == $scope.steps[i].step) {
          return $scope.steps[i].template;
        }
      }
    }

    var policysets = beneficiaryService.getPolicies()
                      .then(function (response) {
                          console.dir(response);
                          $scope.policysets = response.data.policysets;
                      });

    var users = beneficiaryService.getUsers()
                      .then(function (response) {
                          console.dir(response);
                          $scope.users = response.data;
                      });

    var states = beneficiaryService.getStates()
          .then(function (response) {
              $scope.states = response.data;
          }, function (error) {
    });

    $q.all([policysets, users, states]).then(function () {
        $scope.showLoading = false;
    });

    $scope.onUserSelected = function (user, $model, $label) {
        // console.dir(user);
        $scope.beneficiaryFormData.email = user.email;
        $scope.beneficiaryFormData.first_name  = user.first_name;
        $scope.beneficiaryFormData.middle_name = user.middle_name;
        $scope.beneficiaryFormData.last_name = user.last_name;

        $scope.beneficiaryFormData.date_of_birth = user.date_of_birth;
        $scope.beneficiaryFormData.gender = user.gender;
        $scope.beneficiaryFormData.picture = user.primary_profile_picture;
        $scope.beneficiaryFormData.primary_phone_number = parseInt(user.primary_phone_number);
        $scope.beneficiaryFormData.state_id = user.state_id;

        // $scope.beneficiaryFormData.address = user.address;
        // $scope.beneficiaryFormData.job_name = user.job_name;
    }

    $scope.onPictureSelected = function (files) {
      console.dir(files);
      $scope.picture = files[0];
    }

    $scope.save = function () {
      console.dir('submiited');
     
      var data = new FormData();
      data.append("picture", $scope.picture);
      data.append("data", JSON.stringify($scope.beneficiaryFormData));

      $http({
        url: config.apiUrl+'beneficiary/store',
        method: "POST",
        data: data,
        headers: {
          "Content-Type": undefined,
          "Authorization": localstorageService.getObject('user').authorization,
          "RoleId": localstorageService.get('role_chosen', ''),
          "OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
        },        
        transformRequest: angular.identity      
      }).then(function (response) {
          sweetalertService.success('success', 'the beneficiary was created successfully');
          $state.go('secured.permission.beneficiary-index');          
      });
    }
}]);
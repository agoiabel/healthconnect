beneficiary.controller('beneficiaryIndexController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', 'beneficiaryService', 
    function ($scope, localstorageService, planService, sweetalertService, $state, beneficiaryService) {

    $scope.showLoading = true;

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	beneficiaryService.getBeneficiaryCompanies()
				  .then(function (response) {
                        console.dir(response);
                        $scope.showLoading = false;
                        $scope.beneficiaryCompanies = response.data.beneficiaryCompanies;
				  }, function (error) {

				  });

 //    $scope.deletePlan = function (plan, key) {
 //        planService.delete(plan)
 //        			  .then(function (response) {
	// 			  			// $scope.showLoading = false;

	// 		  	            $scope.plans.splice(key, 1);
	// 						toastr.success(response.data.message);
 //        			  });
 //    }

    /**
     * Handle plan creation
     * 
     * @return 
     */
    // $scope.submitPlanForm = function () {
    // 	planService.store($scope.planFormData)
    // 				  .then(function (response) {
    // 				  		sweetalertService.success(response.data.message);
    // 				  		return $state.go('secured.permission.plan-index');
    // 				  });
    // }

    // /**
    //  * Handle the process of adding service
    //  */
    // $scope.redirectToAddServicePageFor = function (plan) {
    //     $state.go('secured.permission.plan-add-service-for', {slug: plan.slug});
    // }

    // /**
    //  * Handle the process of viewing service
    //  * 
    //  * @param  plan 
    //  * @return
    //  */
    // $scope.redirectToViewServicePageFor = function (plan) {
    //     $state.go('secured.permission.plan-view-service-for', {slug: plan.slug});
    // }

}]);
var beneficiary = angular.module('beneficiary', ['ui.router', 'permission', 'ui.bootstrap', 'ngAnimate']);

beneficiary.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.beneficiary-index', {
			url: '/beneficiary-index',
			templateUrl: 'modules/beneficiary/views/beneficiary-index.html',
			controller: 'beneficiaryIndexController',
			authentication: true
		})
		.state('secured.permission.beneficiary-create', {
			url: '/beneficiary-create',
			templateUrl: 'modules/beneficiary/views/beneficiary-create.html',
			controller: 'beneficiaryCreateController',
			authentication: true
		})		
}]);
beneficiary.factory('beneficiaryService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var beneficiaryService = {};

	/**
	 * check if authenticated
	 * 
	 * @return
	 */
	beneficiaryService.getPolicies = function () {
		return httpClient.getWithPermission('company/policyset');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	beneficiaryService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	beneficiaryService.getStates = function () {
		return httpClient.getWithPermission('state/index');		
	}

	/**
	 * Get the company beneficiaries
	 * 
	 * @return 
	 */
	beneficiaryService.getBeneficiaryCompanies = function () {
		return httpClient.getWithPermission('beneficiary/index');		
	}


	return beneficiaryService;
}]);
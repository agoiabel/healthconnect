dashboard.controller('dashboardController', ['$scope', '$state', 'localstorageService', function ($scope, $state, localstorageService) {

	var user = localstorageService.getObject('user');
	
	$scope.user = user;
	$scope.role_users = user.role_users;


	/**
	 * disable access to main dashboard if role_user is inactive
	 * 
	 * @param  role_user 
	 * @return           
	 */
	$scope.inactive = function (role_user) {
		if (role_user.is_active == 1) {
			return false;
		}
		return true;
	}


	/**
	 * Access role
	 * @param  role_id                         
	 * @param  current_organization_profile_id 
	 * @return 
	 */
	$scope.accessRoleWith = function (role_id, current_organization_profile_id) {
		localstorageService.set('role_chosen', role_id);
		localstorageService.set('current_organization_profile_id', current_organization_profile_id);

		$scope.$emit('roleWasChosen', role_id);

		$state.go('secured.permission.mainDashboard');
	}

}]);
var dashboard = angular.module('dashboard', ['ui.router', 'secured']);

dashboard.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.dashboard', {
			url: '/dashboard',
			templateUrl: 'modules/dashboard/views/dashboard.html',
			controller: 'dashboardController',
			authentication: true,
		});
}]);
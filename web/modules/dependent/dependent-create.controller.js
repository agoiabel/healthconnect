dependent.controller('dependentCreateController', ['$scope', 'localstorageService', 'dependentService', 'sweetalertService', '$state', 'config', '$http', '$q',
    function ($scope, localstorageService, dependentService, sweetalertService, $state, config, $http, $q) {

    $scope.showLoading = true;

    $scope.dependentFormData = {
      email: '',

      first_name: '',
      middle_name: '',
      last_name: '',

      date_of_birth: '',
      gender: '',
      picture: '',
      
      primary_phone_number: '',
      state_id: '',

      address: '',
      job_name: '',
      code: '',
    }

    //Handle the display of the modal
    $scope.currentStep = 1;
    $scope.steps = [
      {
        step: 1,
        name: "First step",
        template: "modules/dependent/views/step1.html"
      },
      {
        step: 2,
        name: "Second step",
        template: "modules/dependent/views/step2.html"
      },   
      {
        step: 3,
        name: "Third step",
        template: "modules/dependent/views/step3.html"
      }
    ];

    $scope.gotoStep = function(newStep) {
      $scope.currentStep = newStep;
    }  
    $scope.getStepTemplate = function(){
      for (var i = 0; i < $scope.steps.length; i++) {
        if ($scope.currentStep == $scope.steps[i].step) {
          return $scope.steps[i].template;
        }
      }
    }


    var users = dependentService.getUsers()
                      .then(function (response) {
                          console.dir(response);
                          $scope.users = response.data;
                      });

    var states = dependentService.getStates()
          .then(function (response) {
              $scope.states = response.data;
          }, function (error) {
    });

    $q.all([users, states]).then(function () {
        $scope.showLoading = false;
    });

    $scope.onUserSelected = function (user, $model, $label) {
        console.dir(user);
        $scope.dependentFormData.email = user.email;
        $scope.dependentFormData.first_name  = user.first_name;
        $scope.dependentFormData.middle_name = user.middle_name;
        $scope.dependentFormData.last_name = user.last_name;

        $scope.dependentFormData.date_of_birth = user.date_of_birth;
        $scope.dependentFormData.gender = user.gender;
        $scope.dependentFormData.picture = user.primary_profile_picture;
        $scope.dependentFormData.primary_phone_number = parseInt(user.primary_phone_number);
        $scope.dependentFormData.state_id = user.state_id;
    }

    $scope.onPictureSelected = function (files) {
      console.dir(files);
      $scope.picture = files[0];
    }

    $scope.save = function () {
           
      var data = new FormData();
      data.append("picture", $scope.picture);
      data.append("data", JSON.stringify($scope.dependentFormData));

      $http({
        url: config.apiUrl+'dependent/store',
        method: "POST",
        data: data,
        headers: {
          "Content-Type": undefined,
          "Authorization": localstorageService.getObject('user').authorization,
          "RoleId": localstorageService.get('role_chosen', ''),
          "OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
        },        
        transformRequest: angular.identity      
      }).then(function (response) {
          sweetalertService.success('success', 'the dependent was created successfully');
          $state.go('secured.permission.dependent-index');          
      });

    }
}]);
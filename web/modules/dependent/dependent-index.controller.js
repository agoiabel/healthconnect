dependent.controller('dependentIndexController', ['$scope', 'localstorageService', 'sweetalertService', '$state', 'dependentService', 
    function ($scope, localstorageService, sweetalertService, $state, dependentService) {

    $scope.showLoading = true;

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	dependentService.getdependentCompanies()
				  .then(function (response) {
                        console.dir(response);
                        $scope.showLoading = false;
                        $scope.beneficiaryCompanyDependents = response.data.beneficiaryCompanyDependents;
				  }, function (error) {

				  });


}]);
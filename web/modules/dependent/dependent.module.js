var dependent = angular.module('dependent', ['ui.router', 'permission', 'ui.bootstrap', 'ngAnimate']);

dependent.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.dependent-index', {
			url: '/dependent-index',
			templateUrl: 'modules/dependent/views/dependent-index.html',
			controller: 'dependentIndexController',
			authentication: true
		})
		.state('secured.permission.dependent-create', {
			url: '/dependent-create',
			templateUrl: 'modules/dependent/views/dependent-create.html',
			controller: 'dependentCreateController',
			authentication: true
		})		
}]);
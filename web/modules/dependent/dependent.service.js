dependent.factory('dependentService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var dependentService = {};

	/**
	 * get all users
	 * 
	 * @return 
	 */
	dependentService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	dependentService.getStates = function () {
		return httpClient.getWithPermission('state/index');		
	}

	dependentService.getdependentCompanies = function () {
		return httpClient.getWithPermission('dependent/index');
	}

	return dependentService;
}]);
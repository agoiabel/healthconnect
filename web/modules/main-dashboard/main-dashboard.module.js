var mainDashboard = angular.module('mainDashboard', ['ui.router', 'permission']);

mainDashboard.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.mainDashboard', {
			url: '/main-dashboard',
			templateUrl: 'modules/main-dashboard/views/main-dashboard.html',
			controller: 'mainDashboardController',
			authentication: true,
		});
}]);
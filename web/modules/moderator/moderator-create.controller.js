moderator.controller('moderatorCreateController', ['$scope', 'localstorageService', 'moderatorService', 'sweetalertService', '$state', 'config', '$http', '$q',
    function ($scope, localstorageService, moderatorService, sweetalertService, $state, config, $http, $q) {

    $scope.showLoading = true;

    $scope.moderatorFormData = {
      email: '',
      first_name: '',
      middle_name: '',
      last_name: '',

      date_of_birth: '',
      gender: '',
      picture: '',
      primary_phone_number: '',
      state_id: '',

      address: '',
      job_name: '',
      code: '',
    }

    //Handle the display of the modal
    $scope.currentStep = 1;
    $scope.steps = [
      {
        step: 1,
        name: "First step",
        template: "modules/moderator/views/step1.html"
      },
      {
        step: 2,
        name: "Second step",
        template: "modules/moderator/views/step2.html"
      },   
      {
        step: 3,
        name: "Third step",
        template: "modules/moderator/views/step3.html"
      }
    ];

    $scope.gotoStep = function(newStep) {
      $scope.currentStep = newStep;
    }  
    $scope.getStepTemplate = function(){
      for (var i = 0; i < $scope.steps.length; i++) {
        if ($scope.currentStep == $scope.steps[i].step) {
          return $scope.steps[i].template;
        }
      }
    }


    var users = moderatorService.getUsers()
                      .then(function (response) {
                          console.dir(response);
                          $scope.users = response.data;
                      });

    var states = moderatorService.getStates()
          .then(function (response) {
              $scope.states = response.data;
          }, function (error) {
    });

    $q.all([users, states]).then(function () {
        $scope.showLoading = false;
    });

    $scope.onUserSelected = function (user, $model, $label) {
        console.dir(user);
        $scope.moderatorFormData.email = user.email;
        $scope.moderatorFormData.first_name  = user.first_name;
        $scope.moderatorFormData.middle_name = user.middle_name;
        $scope.moderatorFormData.last_name = user.last_name;

        $scope.moderatorFormData.date_of_birth = user.date_of_birth;
        $scope.moderatorFormData.gender = user.gender;
        $scope.moderatorFormData.picture = user.primary_profile_picture;
        $scope.moderatorFormData.primary_phone_number = parseInt(user.primary_phone_number);
        $scope.moderatorFormData.state_id = user.state_id;
    }

    $scope.onPictureSelected = function (files) {
      console.dir(files);
      $scope.picture = files[0];
    }

    $scope.save = function () {
      console.dir('submiited');
     
      var data = new FormData();
      data.append("picture", $scope.picture);
      data.append("data", JSON.stringify($scope.moderatorFormData));

      $http({
        url: config.apiUrl+'moderator/store',
        method: "POST",
        data: data,
        headers: {
          "Content-Type": undefined,
          "Authorization": localstorageService.getObject('user').authorization,
          "RoleId": localstorageService.get('role_chosen', ''),
          "OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
        },        
        transformRequest: angular.identity      
      }).then(function (response) {
          sweetalertService.success('success', 'the moderator was created successfully');
          $state.go('secured.permission.moderator-index');          
      });
    }
}]);
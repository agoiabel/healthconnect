moderator.controller('moderatorIndexController', ['$scope', 'localstorageService', 'sweetalertService', '$state', 'moderatorService', 
    function ($scope, localstorageService, sweetalertService, $state, moderatorService) {

    $scope.showLoading = true;

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	moderatorService.getmoderatorCompanies()
				  .then(function (response) {
                        console.dir(response);
                        $scope.showLoading = false;
                        $scope.organizationProfileModerators = response.data.organization_profile_moderators;
				  }, function (error) {

				  });

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    
    $scope.status = {
        model_name: 'OrganizationProfileModerator',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.statusClick = function (organization) {
        $scope.status.column_id = organization.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        moderatorService.updateStatus($scope.status)
                           .then(function (response) {
                                //push an event that is going to manually update role_user
                                toastr.success(response.data.message);
                           });
    }
}]);
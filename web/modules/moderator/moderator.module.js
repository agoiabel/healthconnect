var moderator = angular.module('moderator', ['ui.router', 'permission', 'ui.bootstrap', 'ngAnimate']);

moderator.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.moderator-index', {
			url: '/moderator-index',
			templateUrl: 'modules/moderator/views/moderator-index.html',
			controller: 'moderatorIndexController',
			authentication: true
		})
		.state('secured.permission.moderator-create', {
			url: '/moderator-create',
			templateUrl: 'modules/moderator/views/moderator-create.html',
			controller: 'moderatorCreateController',
			authentication: true
		})		
}]);
moderator.factory('moderatorService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var moderatorService = {};

	/**
	 * check if authenticated
	 * 
	 * @return
	 */
	moderatorService.getPolicies = function () {
		return httpClient.getWithPermission('company/policyset');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	moderatorService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	moderatorService.getStates = function () {
		return httpClient.getWithPermission('state/index');		
	}

	/**
	 * Get the company beneficiaries
	 * 
	 * @return 
	 */
	moderatorService.getmoderatorCompanies = function () {
		return httpClient.getWithPermission('moderator/index');		
	}

	moderatorService.updateStatus = function (statusFormData) {
		return httpClient.postWithPermission(statusFormData, 'database-column-update');
	}

	return moderatorService;
}]);
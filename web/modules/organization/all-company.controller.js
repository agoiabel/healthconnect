organization.controller('allCompanyController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.allCompanies()
    				   .then(function (response) {
    						$scope.showLoading = false;
    				   		$scope.companies = response.data;
    				   });

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.status = {
        model_name: 'OrganizationProfile',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    $scope.statusClick = function (company) {
        $scope.status.column_id = company.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                toastr.success(response.data.message);
                           });
    }

}]);
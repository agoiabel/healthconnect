organization.controller('allOrganizationController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.getOrganizations()
    				   .then(function (response) {
    				   	    console.log(response);
    						$scope.showLoading = false;
    				   		$scope.organizations = response.data;
    				   });

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    
    $scope.status = {
        model_name: 'OrganizationProfile',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.statusClick = function (organization) {
        $scope.status.column_id = organization.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                //push an event that is going to manually update role_user
                                toastr.success(response.data.message);
                           });
    }

}]);
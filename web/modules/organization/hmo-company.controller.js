organization.controller('hmoCompanyController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.getCompanies()
    				   .then(function (response) {
    				   	    console.log(response);
    						$scope.showLoading = false;
    				   		$scope.companyHmos = response.data;
    				   });

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.viewPolicySet = function (companyHmo) {
    	// console.dir(companyHmo.company.slug);
    	$state.go('secured.permission.policyset-index', {'companySlug': companyHmo.company.slug});
    }

    $scope.status = {
        model_name: 'CompanyHmo',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    $scope.statusClick = function (hmo) {
        $scope.status.column_id = hmo.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                toastr.success(response.data.message);
                           });
    }

}]);
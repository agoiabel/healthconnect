organization.controller('organizationController', ['$scope', 'organizationService', 'localstorageService', 'config', '$http', '$q', 'sweetalertService', '$state',
	function ($scope, organizationService, localstorageService, config, $http, $q, sweetalertService, $state) {
	
    $scope.showLoading = true;

	$scope.organizationFormData = {
		name: '',
		code: '',

		bank_name: '',
		account_name: '',
		account_number: '',
		
		organization_type_id: '',

		organization_profile_state_id: '',
		organization_address: '',

		is_active: '',

		logo: '',

		email: '',
		first_name: '',
		middle_name: '',
		last_name: '',

		date_of_birth: '',
		
		gender: '',
		primary_phone_number: '',
		state_id: '',
		address: '',
		picture: '', 
	};

	/**
	 * handle the process of display organization based on user role
	 */
	var organizations = organizationService.get()
				  .then(function (response) {
				  		var role_id = localstorageService.get('role_chosen');
				  		$scope.organizations = response.data;
						if (role_id == config.SUPER_ADMIN || role_id == config.ADMIN) {
						} else {
				  			$scope.organizations.splice(0, 1);
						}
				  }, function (error) {
  	});

	/**
	 * Get all states
	 */
	var states = organizationService.getStates()
				  .then(function (response) {
				  		$scope.states = response.data;
				  }, function (error) {
  	});

	/**
	 * Get all states
	 */
	var users = organizationService.getUsers()
				  .then(function (response) {
				  		console.dir(response.data);
				  		$scope.users = response.data;
				  }, function (error) {
  	});

	/**
	 * Get all organization profiles
	 */
	var organizationProfiles = organizationService.getOrganizationProfiles()
			   .then(function (response) {
			   		$scope.organizationProfiles = response.data;
			   });

	/**
	 * Make sure all promises are resolved
	 * 
	 * @param organization, states, organizationProfiles, users promises
	 * @return
	 */
	$q.all([organizations, states, organizationProfiles, users]).then(function () {
        $scope.showLoading = false;
	});

	//Handle the display of the modal
	$scope.currentStep = 1;
	$scope.steps = [
		{
			step: 1,
			name: "First step",
			template: "modules/organization/views/step1.html"
		},
		{
			step: 2,
			name: "Second step",
			template: "modules/organization/views/step2.html"
		},   
		{
			step: 3,
			name: "Third step",
			template: "modules/organization/views/step3.html"
		},             
		{
			step: 4,
			name: "Four step",
			template: "modules/organization/views/step4.html"
		},             
	];
	$scope.gotoStep = function(newStep) {
		$scope.currentStep = newStep;
	}
	$scope.getStepTemplate = function(){
		for (var i = 0; i < $scope.steps.length; i++) {
			if ($scope.currentStep == $scope.steps[i].step) {
				return $scope.steps[i].template;
			}
		}
	}

	$scope.hasPermission = function () {
  		var role_id = localstorageService.get('role_chosen');
					
		if (role_id == config.SUPER_ADMIN || role_id == config.ADMIN) {
			return true;
		} else { 
			return false;
		}
	}

	$scope.onOrganizationProfileSelected = function (organizationProfile, $model, $label) {
		// $scope.loading = true;		
		// sweetalertService.confirm('Hi', 'seems we have the organization you want to create in our system', 'Yes, View Details', function () {
		// 	// console.dir('show form to display organization property');
		// 	// var modalInstance = $modal.open({
		// 	// 	templateUrl: 'modules/organization/views/detail-step-one.html',
		// 	// 	controller: ModalInstanceCtrl
		// 	// });
		// 	// angular.element("#myModal").modal('show');
  //     		// angular.element('#exampleModalLong').modal('show');
		// });
		var formatted_organizationProfiles = [];

		for (var j = 0; j < $scope.organizationProfiles.length; j++) {
			formatted_organizationProfiles[$scope.organizationProfiles[j].id] = $scope.organizationProfiles[j];
		}
		
		$scope.organizationFormData.name = formatted_organizationProfiles[organizationProfile.id].name;
		$scope.organizationFormData.code = formatted_organizationProfiles[organizationProfile.id].code;
		$scope.organizationFormData.bank_name = formatted_organizationProfiles[organizationProfile.id].bank_name;
		$scope.organizationFormData.account_name = formatted_organizationProfiles[organizationProfile.id].account_name;
		$scope.organizationFormData.account_number = parseInt(formatted_organizationProfiles[organizationProfile.id].account_number);
		$scope.organizationFormData.organization_type_id = formatted_organizationProfiles[organizationProfile.id].organization_id;
		$scope.organizationFormData.logo = formatted_organizationProfiles[organizationProfile.id].logo;
		$scope.organizationFormData.organization_address = formatted_organizationProfiles[organizationProfile.id].organization_address;
		$scope.organizationFormData.organization_profile_state_id = formatted_organizationProfiles[organizationProfile.id].organization_profile_state_id;

		$scope.organizationFormData.email = formatted_organizationProfiles[organizationProfile.id].owner.email;
		$scope.organizationFormData.first_name = formatted_organizationProfiles[organizationProfile.id].owner.first_name;
		$scope.organizationFormData.middle_name = formatted_organizationProfiles[organizationProfile.id].owner.middle_name;
		$scope.organizationFormData.last_name = formatted_organizationProfiles[organizationProfile.id].owner.last_name;

		// date_of_birth: formatted_organizationProfiles[organizationProfile.id].owner.date_of_birth,
		$scope.organizationFormData.gender = formatted_organizationProfiles[organizationProfile.id].owner.gender;

		$scope.organizationFormData.primary_phone_number = parseInt(formatted_organizationProfiles[organizationProfile.id].owner.primary_phone_number);
		$scope.organizationFormData.state_id = formatted_organizationProfiles[organizationProfile.id].owner.state_id;
		$scope.organizationFormData.picture = formatted_organizationProfiles[organizationProfile.id].owner.primary_profile_picture;
		$scope.organizationFormData.address = formatted_organizationProfiles[organizationProfile.id].owner.address;

		// $scope.organizationForm.code.$setValidity('organizationProfileUniquenessForCode', false);
		// organizationForm.$setValidity('primary_phone_number', true);
	}


	/**
	 * Set the picture
	 * 
	 * @param files 
	 * @return       
	 */
	$scope.onLogoSelected = function (files) {
		$scope.logo = files[0];
	}
	$scope.onOwnerPictureSelected = function (files) {
		$scope.picture = files[0];
	}


	//Handle the storing of organization profile
	$scope.save = function() {
		var data = new FormData();
		data.append("logo", $scope.logo);
		data.append("picture", $scope.picture);
		data.append("data", JSON.stringify($scope.organizationFormData));

		$http({
			url: config.apiUrl+'organization-profile/store',
			method: "POST",
			data: data,
			headers: {
				"Content-Type": undefined,
				"Authorization": localstorageService.getObject('user').authorization,
				"RoleId": localstorageService.get('role_chosen', ''),
				"OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
			},				
			transformRequest: angular.identity			
		}).then(function (response) {
			sweetalertService.success('success', 'the organization was successfully created');
	  		var role_id = localstorageService.get('role_chosen');
			if (role_id == config.SUPER_ADMIN || role_id == config.ADMIN) {
				$state.go('secured.permission.all-organization');
			} else { 
				if ($scope.organizationFormData.organization_type_id == config.COMPANY) {
					$state.go('secured.permission.hmo-company');
				}
				if ($scope.organizationFormData.organization_type_id == config.PROVIDER) {
					$state.go('secured.permission.hmo-provider');
				}
			}
		}, function (response) {
		 	var error = { response };
		 	console.dir(error);
		 	sweetalertService.error('oops', error.response.data.error);
		});
	}

}]);
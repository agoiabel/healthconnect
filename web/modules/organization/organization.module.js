var organization = angular.module('organization', ['ui.router', 'permission', 'directives', 'ui.bootstrap', 'ngAnimate', 'filters', 'xeditable']);

organization.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.organization-create', {
			url: '/organization-create',
			templateUrl: 'modules/organization/views/organization-create.html',
			controller: 'organizationController',
			authentication: true
		})
		.state('secured.permission.hmo-company', {
			url: '/hmo-company',
			templateUrl: 'modules/organization/views/hmo-company.html',
			controller: 'hmoCompanyController',
			authentication: true
		})
		.state('secured.permission.hmo-provider', {
			url: '/hmo-provider',
			templateUrl: 'modules/organization/views/hmo-provider.html',
			controller: 'hmoProviderController',
			authentication: true
		})
		.state('secured.permission.all-organization', {
			url: '/all-organization',
			templateUrl: 'modules/organization/views/all-organization.html',
			controller: 'allOrganizationController',
			authentication: true
		})
		.state('secured.permission.all-hmo', {
			url: '/all-hmo',
			templateUrl: 'modules/organization/views/all-hmo.html',
			controller: 'allHmoController',
			authentication: true
		})
		.state('secured.permission.all-company', {
			url: '/all-company',
			templateUrl: 'modules/organization/views/all-company.html',
			controller: 'allCompanyController',
			authentication: true
		})
		.state('secured.permission.all-provider', {
			url: '/all-provider',
			templateUrl: 'modules/organization/views/all-provider.html',
			controller: 'allProviderController',
			authentication: true
		})
}]);

organization.run(function(editableOptions) {
  editableOptions.theme = 'bs4'; // bootstrap3 theme. Can be also 'bs2', 'default'
});
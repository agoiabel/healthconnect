organization.factory('organizationService', ['$state', 'httpClient', 'config', '$http', 'localstorageService',
	function ($state, httpClient, config, $http, localstorageService) {
	
	var organizationService = {};

	/**
	 * return all organization 
	 * 
	 * @return 
	 */
	organizationService.get = function () {
		return httpClient.getWithPermission('organization/index');
	}

	/**
	 * return all states
	 * 
	 * @return 
	 */
	organizationService.getStates = function () {
		return httpClient.getWithPermission('state/index');
	}

	/**
	 * Handle the process of getting all organizations
	 * 
	 * @return 
	 */
	organizationService.getOrganizationProfiles = function () {
		return httpClient.getWithPermission('organization-profile/index');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getCompanies = function () {
		return httpClient.getWithPermission('hmo/company');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getProviders = function () {
		return httpClient.getWithPermission('hmo/provider');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getOrganizations = function () {
		return httpClient.getWithPermission('all_organization');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getHmos = function () {
		return httpClient.getWithPermission('all_hmo');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.allCompanies = function () {
		return httpClient.getWithPermission('all_company');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.allProviders = function () {
		return httpClient.getWithPermission('all_provider');
	}

	/**
	 * Get all users
	 * 
	 * @return 
	 */
	organizationService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	organizationService.updateStatus = function (statusFormData) {
		return httpClient.postWithPermission(statusFormData, 'database-column-update');
	}


	return organizationService;
}]);
plan.controller('planEditController', ['$scope', 'planService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, planService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    planService.getSingleFrom($stateParams.planSlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.plan = response.data.plan;
                        $scope.planFormData = {
                            name: $scope.plan.name,
                            code: $scope.plan.code,
                            is_active: $scope.plan.is_active,
                            slug: $scope.plan.slug,
                        }
                  }, function (error) {

                  });

    /**
     * Handle plan creation
     * 
     * @return 
     */
    $scope.submitPlanForm = function () {
    	planService.update($scope.planFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.plan-index');
    				  });
    }

}]);
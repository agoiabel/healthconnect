plan.controller('planServiceController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', '$stateParams', 
    function ($scope, localstorageService, planService, sweetalertService, $state, $stateParams) {

        $scope.showLoading = true;

        var plan_slug = $stateParams.slug;

        planService.getPlan('plan/add-service/'+plan_slug)
        			  .then(function (response) {
                            $scope.showLoading = false;
                            $scope.plan = response.data.plan;
        			  		        $scope.services = response.data.services;

          					        $scope.service = {
          					          service_id: '',
          					          plan_id: $scope.plan.id,
          					          amount: 'add amount'
          					        };  

        			  }, function (error) {
                        
        			  });


        $scope.setService = function (service) {
          $scope.service.service_id = service.id;
        }

       $scope.addAmount = function (key, data) {
          $scope.service.amount = data;
          planService.addPrice($scope.service)
          			 .then(function (response) {
			            $scope.services.splice(key, 1);
			            toastr.success(response.data.message);
          			 }, function (response) {

          			 });
       }
}]);
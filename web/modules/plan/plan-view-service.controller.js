plan.controller('planViewServiceController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', '$stateParams', 'httpClient',
    function ($scope, localstorageService, planService, sweetalertService, $state, $stateParams, httpClient) {

        var plan_slug = $stateParams.slug;

        planService.getPlan('plan/view-service/'+plan_slug)
                .then(function (response) {

                    console.log(response);

                    $scope.planServices = response.data.planServices;
                    $scope.plan = response.data.plan;

                    $scope.service = {
                      service_id: '',
                      plan_id: $scope.plan.id,
                      amount: ''
                    };  

                }, function (error) {
                        
                });


      /**
       * Handle the process of setting plan service
       * 
       * @param planService 
       */
      $scope.setService = function (planService) {
        $scope.service.amount = parseInt(planService.price);
        $scope.service.service_id = planService.service.id;
      }

      /**
       * Handle the process of adding adding amount
       * 
       * @param key
       * @param data
       */
      $scope.addAmount = function (key, data) {
        $scope.service.amount = data;

        planService.updatePrice($scope.service)
                   .then(function (response) {
                      $scope.planServices[key].price = data;
                      toastr.success(response.data.message);            
                   }, function () {

                   });
      }

      /**
       * Handle the process of redirecting to service page 
       * 
       * @param plan 
       * @return 
       */
      $scope.redirectToAddServicePageFor = function (plan) {
        $state.go('secured.permission.plan-add-service-for', {slug: plan.slug});
      }

      /**
       * handle the process of deleting trash 
       * 
       * @param  key         
       * @param  planService 
       * @return             
       */
      $scope.trashService = function (key, planService) {

        httpClient.getWithPermission('plan/delete-service-price/'+planService.id)
                 .then(function (response) {
                    $scope.planServices.splice(key, 1);
                    toastr.success(response.data.message);          
                 }, function () {

                 });
      }

}]);
plan.controller('planController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', function ($scope, localstorageService, planService, sweetalertService, $state) {

    $scope.showLoading = true;
	
    $scope.planFormData = {
        name: '',
        code: '',
        is_active: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	planService.get()
				  .then(function (response) {
                        $scope.showLoading = false;
				  		$scope.plans = response.data;
				  }, function (error) {

				  });

    $scope.deletePlan = function (plan, key) {
        planService.delete(plan)
        			  .then(function (response) {
				  			// $scope.showLoading = false;

			  	            $scope.plans.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    /**
     * Handle plan creation
     * 
     * @return 
     */
    $scope.submitPlanForm = function () {
    	planService.store($scope.planFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.plan-index');
    				  });
    }

    /**
     * Handle the process of adding service
     */
    $scope.redirectToAddServicePageFor = function (plan) {
        $state.go('secured.permission.plan-add-service-for', {slug: plan.slug});
    }

    /**
     * Handle the process of viewing service
     * 
     * @param  plan 
     * @return
     */
    $scope.redirectToViewServicePageFor = function (plan) {
        $state.go('secured.permission.plan-view-service-for', {slug: plan.slug});
    }

    /**
     * Redirect to edit plan
     * 
     * @param plan 
     * @return      
     */
    $scope.redirectToEdit = function (plan) {
        $state.go('secured.permission.plan-edit', {planSlug: plan.slug});
    }

}]);

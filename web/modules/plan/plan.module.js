var plan = angular.module('plan', ['ui.router', 'permission', 'xeditable']);

plan.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.plan-index', {
			url: '/plan-index',
			templateUrl: 'modules/plan/views/plan-index.html',
			controller: 'planController',
			authentication: true
		})
		.state('secured.permission.plan-create', {
			url: '/plan-create',
			templateUrl: 'modules/plan/views/plan-create.html',
			controller: 'planController',
			authentication: true
		})
		.state('secured.permission.plan-edit', {
			url: '/plan-edit/:planSlug',
			templateUrl: 'modules/plan/views/plan-edit.html',
			controller: 'planEditController',
			authentication: true
		})
		.state('secured.permission.plan-add-service-for', {
			url: '/plan-add-service-for/:slug',
			templateUrl: 'modules/plan/views/plan-add-service.html',
			controller: 'planServiceController',
			authentication: true
		})
		.state('secured.permission.plan-view-service-for', {
			url: '/plan-view-service-for/:slug',
			templateUrl: 'modules/plan/views/plan-view-service.html',
			controller: 'planViewServiceController',
			authentication: true
		})		
}]);

// plan.run(function(editableOptions) {
//   editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
// });
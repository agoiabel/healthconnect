plan.factory('planService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var planService = {};

	/**
	 * check if authenticated
	 * 
	 * @return
	 */
	planService.get = function () {
		return httpClient.getWithPermission('plan/index');
	}

	/**
	 * delete a plan
	 * 
	 * @param plan 
	 * @return 
	 */
	planService.delete = function (plan) {
		return httpClient.getWithPermission('plan/delete/'+plan.id)
	}
		
	/**
	 * store creation of plan
	 * 
	 * @param planFormData 
	 * @return 
	 */
	planService.store = function (planFormData) {
		return httpClient.postWithPermission(planFormData, 'plan/store');
	}

	/**
	 * get the plan for a particular service
	 * 
	 * @param url 
	 * @return     
	 */
	planService.getPlan = function (url) {
		return httpClient.getWithPermission(url);
	}

	/**
	 * Add price to a particular service
	 * 
	 * @param 
	 */
	planService.addPrice = function (planServiceData) {
		return httpClient.postWithPermission(planServiceData, 'plan/add-service-price');
	}

	/**
	 * update the price
	 * 
	 * @param service 
	 * @return         
	 */
	planService.updatePrice = function (service) {
		return httpClient.postWithPermission(service, 'plan/add-service-price');
	}

	/**
	 * delete a service with price
	 * 
	 * @param planService 
	 * @return 
	 */
	planService.deleteServiceWithPrice = function (planService) {
		return httpClient.getWithPermission('plan/delete-service-price/'+planService.id);
	}

	planService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('plan/show/'+slug);
	}

	planService.update = function (planFormData) {
		return httpClient.postWithPermission(planFormData, 'plan/update');
	}

	return planService;
}]);
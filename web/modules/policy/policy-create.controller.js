policy.controller('policyCreateController', ['$scope', 'localstorageService', 'policyService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policyService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    $scope.policyFormData = {
        policyset_slug: $stateParams.policysetSlug,
        plan_id: '',
        name: '',
        maximum_no_of_beneficiary_dependant: '',
        is_active: '',
    }

    /**
     * redirect to policy index
     * 
     * @return 
     */
    $scope.allPolicy = function () {
        $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    }

    /**
     * Get this organization plans
     */
    policyService.getPlan()
                 .then(function (response) {
                    $scope.showLoading = false;
                    $scope.plans = response.data;
                 });
    

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitPolicyForm = function () {
        console.dir('submited');
        // console.dir($scope.policyFormData);

    	policyService.store($scope.policyFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                            $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            // sweetalertService.error('oops', error.response.data);
                      });
    }

}]);
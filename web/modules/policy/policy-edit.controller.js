policy.controller('policyEditController', ['$scope', 'localstorageService', 'policyService', 'sweetalertService', '$state', '$stateParams', '$q',
    function ($scope, localstorageService, policyService, sweetalertService, $state, $stateParams, $q) {

    $scope.showLoading = true;

    /**
     * Get single policy
     * 
     * @type 
     */
    var policy = policyService.getSingleFrom($stateParams.policySlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.policy = response.data.policy;

                        $scope.policyFormData = {
                            policy_slug: $stateParams.policySlug,
                            policyset_slug: $stateParams.policysetSlug,
                            plan_id: $scope.policy.plan_id,
                            name: $scope.policy.name,
                            maximum_no_of_beneficiary_dependant: $scope.policy.maximum_no_of_beneficiary_dependant,
                            is_active: $scope.policy.is_active,
                        }

                  }, function (error) {

                  });

    /**
     * Get this organization plans
     */
    var plans = policyService.getPlan()
                 .then(function (response) {
                    $scope.showLoading = false;
                    $scope.plans = response.data;
                 });

    /**
     * Note when all promises are resolved
     * 
     * @param  
     * @return 
     */
    $q.all([policy, plans]).then(function () {
        $scope.showLoading = false;
    });

    /**
     * redirect to policy index
     * 
     * @return 
     */
    $scope.allPolicy = function () {
        $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    }
    
    $scope.allPolicySet = function () {
        $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    }

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitPolicyForm = function () {
    	policyService.update($scope.policyFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                            $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            // sweetalertService.error('oops', error.response.data);
                      });
    }

}]);
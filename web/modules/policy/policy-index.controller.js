policy.controller('policyIndexController', ['$scope', 'localstorageService', 'policyService', 'sweetalertService', '$state', '$stateParams', 'planService',
    function ($scope, localstorageService, policyService, sweetalertService, $state, $stateParams, planService) {

    $scope.showLoading = true;

    /**
     * Redirect to adding new policy
     */
    $scope.addNewPolicy = function () {
        $state.go('secured.permission.policy-create', {policysetSlug: $stateParams.policysetSlug});
    }
	
    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }


	policyService.get($stateParams.policysetSlug)
				  .then(function (response) {
                        $scope.showLoading = false;                    
				  		$scope.policies = response.data;
                  }, function (error) {

				  });


    $scope.deletePolicy = function (policy, key) {
        console.dir(policy);
        policyService.delete(policy)
        			  .then(function (response) {
			  	            $scope.policies.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    $scope.redirectToEdit = function (policy) {
        $state.go('secured.permission.policy-edit', {policysetSlug: $stateParams.policysetSlug, policySlug: policy.slug});
    }

}]);
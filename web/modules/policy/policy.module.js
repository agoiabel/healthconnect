var policy = angular.module('policy', ['ui.router', 'permission']);

policy.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.policy-index', {
			url: '/policy-index/:policysetSlug',
			templateUrl: 'modules/policy/views/policy-index.html',
			controller: 'policyIndexController',
			authentication: true
		})
		.state('secured.permission.policy-create', {
			url: '/policy-create/:policysetSlug',
			templateUrl: 'modules/policy/views/policy-create.html',
			controller: 'policyCreateController',
			authentication: true
		})
		.state('secured.permission.policy-edit', {
			url: '/policy-edit/:policysetSlug/:policySlug',
			templateUrl: 'modules/policy/views/policy-edit.html',
			controller: 'policyEditController',
			authentication: true
		})
}]);

policy.factory('policyService', ['$state', 'httpClient', 'localstorageService',
	function ($state, httpClient, localstorageService) {
	
	var policyService = {};

	policyService.getPlan = function () {
		return httpClient.getWithPermission('plan/index');
	}

	/**
	 * check if authenticated
	 * 
	 * @return 
	 */
	policyService.get = function (policysetSlug) {
		return httpClient.getWithPermission('policy/index/'+policysetSlug);
	}

	policyService.delete = function (policy) {
		return httpClient.getWithPermission('policy/delete/'+policy.id)
	}
	
	policyService.store = function (policyFormData) {
		return httpClient.postWithPermission(policyFormData, 'policy/store');
	}

	policyService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('policy/show/'+slug);
	}

	policyService.update = function (policyFormData) {
		return httpClient.postWithPermission(policyFormData, 'policy/update');
	}	

	return policyService;
}]);

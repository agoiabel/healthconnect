policyset.controller('policysetCreateController', ['$scope', 'localstorageService', 'policysetService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policysetService, sweetalertService, $state, $stateParams) {

    $scope.allPolicySet = function () {
        $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    }

    $scope.policysetFormData = {
        company_slug: $stateParams.companySlug,
        name: '',
        start_date: '',
        end_date: '',
        is_active: '',
    }
    

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitPolicysetForm = function () {
        
    	policysetService.store($scope.policysetFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                            $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            sweetalertService.error('oops', error.response.data.end_date);
                      });
    }

}]);
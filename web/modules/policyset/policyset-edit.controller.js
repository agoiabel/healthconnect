policyset.controller('policysetEditController', ['$scope', 'localstorageService', 'policysetService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policysetService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    $scope.allPolicySet = function () {
        $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    }

    policysetService.getSingleFrom($stateParams.policysetSlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.policyset = response.data.policyset;

                        $scope.policysetFormData = {
                            company_slug: $stateParams.companySlug,
                            policyset_slug: $stateParams.policysetSlug,
                            name: $scope.policyset.name,
                            start_date: $scope.policyset.start_date,
                            end_date: $scope.policyset.end_date,
                            is_active: $scope.policyset.is_active,
                        }

                  }, function (error) {

                  });

    

    /**
     * Handle policyset creation
     * 
     * @return 
     */
    $scope.submitPolicysetForm = function () {
        
    	policysetService.update($scope.policysetFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                  $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            sweetalertService.error('oops', error.response.data.end_date);
                      });
    }


}]);
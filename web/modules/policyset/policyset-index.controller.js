policyset.controller('policysetIndexController', ['$scope', 'localstorageService', 'policysetService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policysetService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    $scope.displayPolicyFor = function (policyset) {
        $state.go('secured.permission.policy-index', {policysetSlug: policyset.slug});
    }
	
    $scope.addNewPolicySet = function () {
        $state.go('secured.permission.policyset-create', {companySlug: $stateParams.companySlug});
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }


	policysetService.get($stateParams.companySlug)
				  .then(function (response) {
                        $scope.showLoading = false;                    
				  		$scope.policysets = response.data;
                  }, function (error) {

				  });


    $scope.deletePolicyset = function (service, key) {
        policysetService.delete(service)
        			  .then(function (response) {
			  	            $scope.policysets.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    $scope.redirectToEdit = function (policyset) {
        $state.go('secured.permission.policyset-edit', {companySlug: $stateParams.companySlug, policysetSlug: policyset.slug});
    }

}]);
var policyset = angular.module('policyset', ['ui.router', 'permission']);

policyset.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.policyset-index', {
			url: '/policyset-index/:companySlug',
			templateUrl: 'modules/policyset/views/policyset-index.html',
			controller: 'policysetIndexController',
			authentication: true
		})
		.state('secured.permission.policyset-create', {
			url: '/policyset-create/:companySlug',
			templateUrl: 'modules/policyset/views/policyset-create.html',
			controller: 'policysetCreateController',
			authentication: true
		})
		.state('secured.permission.policyset-edit', {
			url: '/policyset-edit/:companySlug/:policysetSlug',
			templateUrl: 'modules/policyset/views/policyset-create.html',
			controller: 'policysetEditController',
			authentication: true
		})
}]);


policyset.filter('moment', function () {
    return function (input, momentFn /*, param1, param2, ...param n */) {
        var args = Array.prototype.slice.call(arguments, 2),
        momentObj = moment(input);
        return momentObj[momentFn].apply(momentObj, args);
    };
});

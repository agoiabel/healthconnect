policyset.factory('policysetService', ['$state', 'httpClient', 'localstorageService',
	function ($state, httpClient, localstorageService) {
	
	var policysetService = {};

	/**
	 * check if authenticated
	 * 
	 * @return {Boolean} 
	 */
	policysetService.get = function (companySlug) {
		return httpClient.getWithPermission('policyset/index/'+companySlug);
	}

	
	policysetService.delete = function (policyset) {
		return httpClient.getWithPermission('policyset/delete/'+policyset.id)
	}
	
	policysetService.store = function (policysetFormData) {
		return httpClient.postWithPermission(policysetFormData, 'policyset/store');
	}

	policysetService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('policyset/show/'+slug);
	}

	policysetService.update = function (policysetFormData) {
		return httpClient.postWithPermission(policysetFormData, 'policyset/update');
	}	

	return policysetService;
}]);

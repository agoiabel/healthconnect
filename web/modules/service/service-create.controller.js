service.controller('serviceCreateController', ['$scope', 'localstorageService', 'serviceService', 'sweetalertService', '$state', function ($scope, localstorageService, serviceService, sweetalertService, $state) {

    $scope.serviceFormData = {
        name: '',
        code: '',
        is_active: '',
    }
	
    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitServiceForm = function () {
    	serviceService.store($scope.serviceFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.service-index');
    				  });
    }

}]);
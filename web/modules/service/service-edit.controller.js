service.controller('serviceEditController', ['$scope', 'localstorageService', 'serviceService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, serviceService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    serviceService.getSingleFrom($stateParams.serviceSlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.service = response.data.service;
                        $scope.serviceFormData = {
                            name: $scope.service.name,
                            code: $scope.service.code,
                            is_active: $scope.service.is_active,
                            slug: $scope.service.slug,
                        }
                  }, function (error) {

                  });

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitServiceForm = function () {
    	serviceService.update($scope.serviceFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.service-index');
    				  });
    }

}]);
service.controller('serviceIndexController', ['$scope', 'localstorageService', 'serviceService', 'sweetalertService', '$state',
    function ($scope, localstorageService, serviceService, sweetalertService, $state) {


    $scope.showLoading = true;

    $scope.mainPage = 1;

	$scope.loadPage = serviceService.get($scope.mainPage)
                          .then(function (response) {
                                console.dir(response);
                                $scope.showLoading = false;
                                
                                $scope.currentPage = response.data.current_page;
                                $scope.from = response.data.from;
                                $scope.lastPage = response.data.last_page;
                                $scope.to = response.data.to;
                                $scope.nextPageUrl = response.data.next_page_url;
                                $scope.prevPageUrl = response.data.prev_page_url;
                                $scope.perPage = response.data.per_page;
                                $scope.total = response.data.total;

                                $scope.services = response.data.data;
                          }, function (error) {
                    });

    $scope.loadPage;


    $scope.nextPage = function () {
        $scope.showLoading = true;
        $scope.services = {};

        $scope.mainPage = $scope.mainPage + 1;
        serviceService.get($scope.mainPage)
                      .then(function (response) {
                            console.dir(response);
                            $scope.showLoading = false;
                            
                            $scope.currentPage = response.data.current_page;
                            $scope.from = response.data.from;
                            $scope.lastPage = response.data.last_page;
                            $scope.to = response.data.to;
                            $scope.nextPageUrl = response.data.next_page_url;
                            $scope.prevPageUrl = response.data.prev_page_url;
                            $scope.perPage = response.data.per_page;
                            $scope.total = response.data.total;

                            $scope.services = response.data.data;
                      }, function (error) {
        });
    };

    $scope.prevPage = function () {

        $scope.showLoading = true;
        $scope.services = {};

        $scope.mainPage = $scope.mainPage - 1;
        serviceService.get($scope.mainPage)
                      .then(function (response) {
                            console.dir(response);
                            $scope.showLoading = false;
                            
                            $scope.currentPage = response.data.current_page;
                            $scope.from = response.data.from;
                            $scope.lastPage = response.data.last_page;
                            $scope.to = response.data.to;
                            $scope.nextPageUrl = response.data.next_page_url;
                            $scope.prevPageUrl = response.data.prev_page_url;
                            $scope.perPage = response.data.per_page;
                            $scope.total = response.data.total;
                                
                            $scope.services = response.data.data;
                      }, function (error) {
        });

    };

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.deleteService = function (service, key) {
		// $scope.showLoading = true;
        serviceService.delete(service)
        			  .then(function (response) {
				  			// $scope.showLoading = false;

			  	            $scope.services.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    $scope.redirectToEdit = function (service) {
        $state.go('secured.permission.service-edit', {serviceSlug: service.slug});
    }


    $scope.range = [];
    var pages = [];

    for (var i = 1; i <= $scope.lastPage; i++) {
        pages.push(i);
    }

    $scope.range = pages;
    

}]);
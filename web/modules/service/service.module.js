var service = angular.module('service', ['ui.router', 'permission', 'directives']);

service.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.service-index', {
			url: '/service-index',
			templateUrl: 'modules/service/views/service-index.html',
			controller: 'serviceIndexController',
			authentication: true
		})
		.state('secured.permission.service-create', {
			url: '/service-create',
			templateUrl: 'modules/service/views/service-create.html',
			controller: 'serviceCreateController',
			authentication: true
		})
		.state('secured.permission.service-edit', {
			url: '/service-edit/:serviceSlug',
			templateUrl: 'modules/service/views/service-edit.html',
			controller: 'serviceEditController',
			authentication: true
		})
}]);
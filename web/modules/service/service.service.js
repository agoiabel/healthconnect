service.factory('serviceService', ['$state', 'httpClient', 'localstorageService',
	function ($state, httpClient, localstorageService) {
	
	var serviceService = {};

	/**
	 * check if authenticated
	 * 
	 * @return {Boolean} 
	 */
	serviceService.get = function (page) {
		return httpClient.getWithPermission('service/index?page='+page);
	}

	serviceService.delete = function (service) {
		return httpClient.getWithPermission('service/delete/'+service.id)
	}
	
	serviceService.store = function (serviceFormData) {
		return httpClient.postWithPermission(serviceFormData, 'service/store');
	}

	serviceService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('service/show/'+slug);
	}

	serviceService.update = function (serviceFormData) {
		return httpClient.postWithPermission(serviceFormData, 'service/update');
	}

	return serviceService;
}]);
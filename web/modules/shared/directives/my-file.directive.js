directives.directive('myFile', function () {
	return {
		scope: {
			file: '='
		},
		link: function (scope, element, attrs) {
			element.bind('change', function (event) {
				var file = event.target.files[0];
				scope.file = file ? file : '';
				scope.$apply();
			});
		}
	}
});
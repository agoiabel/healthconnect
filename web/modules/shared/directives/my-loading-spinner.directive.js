directives.directive('myLoadingSpinner', ['$compile', function ($compile) {

	return {
		templateUrl: 'modules/shared/directives/circle-spinner.html' 
	}
	
}]);
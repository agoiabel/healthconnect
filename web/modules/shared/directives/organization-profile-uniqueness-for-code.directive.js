directives.directive('organizationProfileUniquenessForCode', ['checkService', function (checkService) {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			
			var organizationProfiles = scope.organizationProfiles;

			var array = [];	
			for (j = 0; j < organizationProfiles.length; j++) {
				array[organizationProfiles[j].code] = organizationProfiles[j].code; 
			}

			ngModelCtrl.$validators.organization_code_invalid = function (modelValue, viewValue) {
				return ! checkService.isInArray(viewValue, array);
			}

		}
	}
}]);
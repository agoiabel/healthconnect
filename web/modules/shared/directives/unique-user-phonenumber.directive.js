directives.directive('uniqueUserPhonenumber', ['checkService', function (checkService) {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			
			var users = scope.users;

			console.dir(users);

			var array = [];	
			for (j = 0; j < users.length; j++) {
				array[parseInt(users[j].primary_phone_number)] = parseInt(users[j].primary_phone_number); 
			}

			ngModelCtrl.$validators.user_phone_invalid = function (modelValue, viewValue) {
				return ! checkService.isInArray(parseInt(viewValue), array);
			}

		}
	}
}]);
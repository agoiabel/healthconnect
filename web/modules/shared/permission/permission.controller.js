permission.controller('permissionController', ['$scope', 'authenticationService', 'localstorageService', 'checkService', function ($scope, authenticationService, localstorageService, checkService) {

	var user = localstorageService.getObject('user');
	var role_id = localstorageService.get('role_chosen');

	var formatted_role_users = [];
	var role_users = user.role_users;

	for (var j = 0; j < role_users.length; j++){
	  formatted_role_users[role_users[j].role_id] = role_users[j];
	}

	$scope.current_organization_profile = formatted_role_users[role_id].organization_profile;
	$scope.role = formatted_role_users[role_id].role;

	
}]);
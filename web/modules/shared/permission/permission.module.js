var permission = angular.module('permission', ['ui.router', 'secured']);

permission.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission', {
			abstract: true,
			controller: 'permissionController',
			templateUrl: 'modules/shared/permission/views/permission.html',
		})
}]);
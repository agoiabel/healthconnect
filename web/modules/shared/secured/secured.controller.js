secured.controller('securedController', ['$scope', 'authenticationService', 'localstorageService', 'checkService', function ($scope, authenticationService, localstorageService, checkService) {

	$scope.permission_roles = [];
	$scope.user = localstorageService.getObject('user');

	/**
	 * Update permissions == navbar
	 * @param  event    
	 * @param  role_id 
	 * @return 
	 */
	$scope.$on('roleWasChosen', function (event, role_id) {
		var user = localstorageService.getObject('user');

		var formatted_role_users = [];
		var role_users = user.role_users;

		for (var j = 0; j < role_users.length; j++){
		  formatted_role_users[role_users[j].role_id] = role_users[j];
		}

		$scope.permission_roles = formatted_role_users[role_id].role.permission_roles;
	});

	/**
	 * Log the user out
	 * 
	 * @return 
	 */
	$scope.doLogout = function () {
		return authenticationService.logout();
	}

	/**
	 * handle the logic of display navigation or not
	 * 
	 * @return 
	 */
	$scope.canDisplayRole = function () {
		if (! checkService.forEmptyString( localstorageService.get('role_chosen') )) {
			return true
		}

		return false;
	}


	
}]);
var secured = angular.module('secured', ['ui.router']);

secured.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured', {
			abstract: true,
			controller: 'securedController',
			templateUrl: 'modules/shared/secured/views/secured.html',
		})
}]);

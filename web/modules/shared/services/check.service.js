services.factory('checkService', [function () {
	
	var checkService = {};

	/**
	 * check if a string is empty
	 * 
	 * @param string 
	 * @return
	 */
	checkService.forEmptyString = function (string) {
	    if (typeof string == 'undefined' || !string || string.length === 0 || string === "" || !/[^\s]/.test(string) || /^\s*$/.test(string) || string.replace(/\s/g,"") === "")
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}

	/**
	 * check if an object is empty
	 * 
	 * @param obj
	 * @return
	 */
	checkService.forEmptyObject = function (obj) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				return false;
			}
		}
		return true;
	} 

	/**
	 * check if a string is in an array
	 * 
	 * @return 
	 */
	checkService.isInArray = function (target, array) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] === target) {
				return true;
			}
		}

		return false;
	}

	/**
	 * check if an array is in another array
	 * 
	 * @return 
	 */
	checkService.arrayContainAnotherArray = function (array1, array2) {
		for (var i = 0; i < array1.length; i++) {
			if (array2.indexOf( array1[i] === -1 )) {
				return false;
			}
		}

		return true;
	}


	return checkService;
}]);
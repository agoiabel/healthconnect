services.factory('httpClient', ['$http', 'config', 'localstorageService', function ($http, config, localstorageService) {
	var httpClient = {};

	/**
	 * set http settings
	 * 
	 * @return 
	 */
	function getSettingsFrom(requestData) {
		return {
			url: requestData.url,
			method: requestData.method,
			dataType: requestData.dataType || "json",
			data: requestData.data || {},
			headers: requestData.headers || {
				"accept": "application/json"
			}
		}
	}

	httpClient.postWithPermission = function (formData, url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'POST',
			data: formData,
			headers: {
				"Authorization": localstorageService.getObject('user').authorization,
				"RoleId": localstorageService.get('role_chosen', ''),
				"OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
			}
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.getWithPermission = function (url) {
		// console.log(localstorageService.get('current_organization_profile_id', ''));
		var requestData = {
			url: config.apiUrl + url,
			method: 'GET',
			headers: {
				"Authorization": localstorageService.getObject('user').authorization,
				"RoleId": localstorageService.get('role_chosen', ''),
				"OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
			}			
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.postIsAuthorize = function (formData, url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'POST',
			data: formData,
			headers: {
				"Authorization": localstorageService.get('user').authorization
			}			
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.getIsAuthorize = function (url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'GET',
			data: formData,
			headers: {
				"Authorization": localstorageService.get('user').authorization
			}			
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.post = function (formData, url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'POST',
			data: formData			
		};
		return $http( getSettingsFrom(requestData) );
	}

	httpClient.get = function (url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'GET',
			data: formData			
		};
		return $http( getSettingsFrom(requestData) );
	}

	return httpClient;
}]);
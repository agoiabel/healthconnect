services.factory('localstorageService', ['$window', function ($window) {
	
	var localstorageService = {};

	/**
	 * set string in the local storage
	 * 
	 * @param key   
	 * @param value
	 */
	localstorageService.set = function (key, value) {
		$window.localStorage[key] = value;
	}

	/**
	 * set object in the local storage
	 * 
	 * @param key   
	 * @param value 
	 */
	localstorageService.setObject = function (key, value) {
		$window.localStorage[key] = JSON.stringify(value);
	}

	/**
	 * get a string stored in the local storage
	 * 
	 * @param  key          
	 * @param defaultValue
	 * @return
	 */
	localstorageService.get = function (key, defaultValue) {
		return $window.localStorage[key] || defaultValue;
	}

	/**
	 * get an object stored in the local storage
	 * 
	 * @param  key          
	 * @param defaultValue
	 * @return
	 */
	localstorageService.getObject = function (key) {
		return JSON.parse($window.localStorage[key] || {});
	}

	/**
	 * Remove an item from localstorage
	 * 
	 * @param key 
	 * @return
	 */
	localstorageService.removeItem = function (key) {
		$window.localStorage.removeItem(key);
	}

	/**
	 * clear all localstorage
	 * 
	 * @param key 
	 * @return
	 */
	localstorageService.removeAll = function () {
		$window.localStorage.clear();
	}


	return localstorageService;
}]);
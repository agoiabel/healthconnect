services.factory('sweetalertService', [function () {
	var sweetalertService = {};

	sweetalertService.create = function (type, title, message) {
		return swal({   
			title: title,   
			text: message,   
			type: type,
			timer: 3000,
			showConfirmButton: false   
		});
	}

	sweetalertService.info = function (title, message) {
		return sweetalertService.create("info", title, message);
	}

	sweetalertService.success = function (title, message) {
		return sweetalertService.create("success", title, message);
	}

	sweetalertService.error = function (title, message) {
		return sweetalertService.create("error", title, message);
	}

	sweetalertService.confirm = function (title, message, confirmButtonText, callbackFunction) {
		return swal({   
			title: title,   
			text: message,   
			type: "warning",
			showConfirmButton: true,
			showConfirmButtonColor: "#2cc185",
			confirmButtonText: confirmButtonText,   
		}, callbackFunction);
	}

	return sweetalertService;
}]);
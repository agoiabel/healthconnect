secured.controller('securedController', ['$scope', 'authenticationService', 'localstorageService', 'checkService', function ($scope, authenticationService, localstorageService, checkService) {

	$scope.permission_roles = [];
	$scope.user = localstorageService.getObject('user');

	/**
	 * Update permissions == navbar
	 * @param  event    
	 * @param  role_id 
	 * @return 
	 */
	$scope.$on('roleWasChosen', function (event, role_id) {
		var user = localstorageService.getObject('user');

		var formatted_role_users = [];
		var role_users = user.role_users;

		for (var j = 0; j < role_users.length; j++){
		  formatted_role_users[role_users[j].role_id] = role_users[j];
		}

		$scope.permission_roles = formatted_role_users[role_id].role.permission_roles;
	});

	/**
	 * Log the user out
	 * 
	 * @return 
	 */
	$scope.doLogout = function () {
		return authenticationService.logout();
	}

	/**
	 * handle the logic of display navigation or not
	 * 
	 * @return 
	 */
	$scope.canDisplayRole = function () {
		if (! checkService.forEmptyString( localstorageService.get('role_chosen') )) {
			return true
		}

		return false;
	}


	
}]);
permission.controller('permissionController', ['$scope', 'authenticationService', 'localstorageService', 'checkService', function ($scope, authenticationService, localstorageService, checkService) {

	var user = localstorageService.getObject('user');
	var role_id = localstorageService.get('role_chosen');

	var formatted_role_users = [];
	var role_users = user.role_users;

	for (var j = 0; j < role_users.length; j++){
	  formatted_role_users[role_users[j].role_id] = role_users[j];
	}

	$scope.current_organization_profile = formatted_role_users[role_id].organization_profile;
	$scope.role = formatted_role_users[role_id].role;

	
}]);
authentication.controller('authenticationController', ['$scope', 'httpClient', 'authenticationService', '$state', 'sweetalertService', 'localstorageService', 'checkService', 
	function ($scope, httpClient, authenticationService, $state, sweetalertService, localstorageService, checkService) {
		
	$scope.loginFormData = {
		email: '',
		password: ''
	}

	$scope.loading = false;

	$scope.submitLoginForm = function () {
		$scope.loading = true;

		httpClient.post($scope.loginFormData, 'login')
				  .then(
				  		function (response) {
				  			$scope.loading = false;
				  			localstorageService.set('authenticated', true);

						 	localstorageService.setObject('user', response.data.user);

							console.log('ready for redirection');

							sweetalertService.success('hi', 'welcome back');
							
							$state.go('secured.dashboard');
				  		},
				  		function (response) {
				  			$scope.loading = false;
						 	var error = { response };
						 	sweetalertService.error('bad login', error.response.data.error);
				  		}
				  );

	}




}]);
dashboard.controller('dashboardController', ['$scope', '$state', 'localstorageService', function ($scope, $state, localstorageService) {

	var user = localstorageService.getObject('user');
	
	$scope.user = user;
	$scope.role_users = user.role_users;


	/**
	 * disable access to main dashboard if role_user is inactive
	 * 
	 * @param  role_user 
	 * @return           
	 */
	$scope.inactive = function (role_user) {
		if (role_user.is_active == 1) {
			return false;
		}
		return true;
	}


	/**
	 * Access role
	 * @param  role_id                         
	 * @param  current_organization_profile_id 
	 * @return 
	 */
	$scope.accessRoleWith = function (role_id, current_organization_profile_id) {
		localstorageService.set('role_chosen', role_id);
		localstorageService.set('current_organization_profile_id', current_organization_profile_id);

		$scope.$emit('roleWasChosen', role_id);

		$state.go('secured.permission.mainDashboard');
	}

}]);
mainDashboard.controller('mainDashboardController', ['$scope', 'localstorageService', function ($scope, localstorageService) {
	
}]);
organization.controller('organizationController', ['$scope', 'organizationService', 'localstorageService', 'config', '$http', '$q', 'sweetalertService', '$state',
	function ($scope, organizationService, localstorageService, config, $http, $q, sweetalertService, $state) {
	
    $scope.showLoading = true;

	$scope.organizationFormData = {
		name: '',
		code: '',

		bank_name: '',
		account_name: '',
		account_number: '',
		
		organization_type_id: '',

		organization_profile_state_id: '',
		organization_address: '',

		is_active: '',

		logo: '',

		email: '',
		first_name: '',
		middle_name: '',
		last_name: '',

		date_of_birth: '',
		
		gender: '',
		primary_phone_number: '',
		state_id: '',
		address: '',
		picture: '', 
	};

	/**
	 * handle the process of display organization based on user role
	 */
	var organizations = organizationService.get()
				  .then(function (response) {
				  		var role_id = localstorageService.get('role_chosen');
				  		$scope.organizations = response.data;
						if (role_id == config.SUPER_ADMIN || role_id == config.ADMIN) {
						} else {
				  			$scope.organizations.splice(0, 1);
						}
				  }, function (error) {
  	});

	/**
	 * Get all states
	 */
	var states = organizationService.getStates()
				  .then(function (response) {
				  		$scope.states = response.data;
				  }, function (error) {
  	});

	/**
	 * Get all states
	 */
	var users = organizationService.getUsers()
				  .then(function (response) {
				  		console.dir(response.data);
				  		$scope.users = response.data;
				  }, function (error) {
  	});

	/**
	 * Get all organization profiles
	 */
	var organizationProfiles = organizationService.getOrganizationProfiles()
			   .then(function (response) {
			   		$scope.organizationProfiles = response.data;
			   });

	/**
	 * Make sure all promises are resolved
	 * 
	 * @param organization, states, organizationProfiles, users promises
	 * @return
	 */
	$q.all([organizations, states, organizationProfiles, users]).then(function () {
        $scope.showLoading = false;
	});

	//Handle the display of the modal
	$scope.currentStep = 1;
	$scope.steps = [
		{
			step: 1,
			name: "First step",
			template: "modules/organization/views/step1.html"
		},
		{
			step: 2,
			name: "Second step",
			template: "modules/organization/views/step2.html"
		},   
		{
			step: 3,
			name: "Third step",
			template: "modules/organization/views/step3.html"
		},             
		{
			step: 4,
			name: "Four step",
			template: "modules/organization/views/step4.html"
		},             
	];
	$scope.gotoStep = function(newStep) {
		$scope.currentStep = newStep;
	}
	$scope.getStepTemplate = function(){
		for (var i = 0; i < $scope.steps.length; i++) {
			if ($scope.currentStep == $scope.steps[i].step) {
				return $scope.steps[i].template;
			}
		}
	}

	$scope.hasPermission = function () {
  		var role_id = localstorageService.get('role_chosen');
					
		if (role_id == config.SUPER_ADMIN || role_id == config.ADMIN) {
			return true;
		} else { 
			return false;
		}
	}

	$scope.onOrganizationProfileSelected = function (organizationProfile, $model, $label) {
		// $scope.loading = true;		
		// sweetalertService.confirm('Hi', 'seems we have the organization you want to create in our system', 'Yes, View Details', function () {
		// 	// console.dir('show form to display organization property');
		// 	// var modalInstance = $modal.open({
		// 	// 	templateUrl: 'modules/organization/views/detail-step-one.html',
		// 	// 	controller: ModalInstanceCtrl
		// 	// });
		// 	// angular.element("#myModal").modal('show');
  //     		// angular.element('#exampleModalLong').modal('show');
		// });
		var formatted_organizationProfiles = [];

		for (var j = 0; j < $scope.organizationProfiles.length; j++) {
			formatted_organizationProfiles[$scope.organizationProfiles[j].id] = $scope.organizationProfiles[j];
		}
		
		$scope.organizationFormData.name = formatted_organizationProfiles[organizationProfile.id].name;
		$scope.organizationFormData.code = formatted_organizationProfiles[organizationProfile.id].code;
		$scope.organizationFormData.bank_name = formatted_organizationProfiles[organizationProfile.id].bank_name;
		$scope.organizationFormData.account_name = formatted_organizationProfiles[organizationProfile.id].account_name;
		$scope.organizationFormData.account_number = parseInt(formatted_organizationProfiles[organizationProfile.id].account_number);
		$scope.organizationFormData.organization_type_id = formatted_organizationProfiles[organizationProfile.id].organization_id;
		$scope.organizationFormData.logo = formatted_organizationProfiles[organizationProfile.id].logo;
		$scope.organizationFormData.organization_address = formatted_organizationProfiles[organizationProfile.id].organization_address;
		$scope.organizationFormData.organization_profile_state_id = formatted_organizationProfiles[organizationProfile.id].organization_profile_state_id;

		$scope.organizationFormData.email = formatted_organizationProfiles[organizationProfile.id].owner.email;
		$scope.organizationFormData.first_name = formatted_organizationProfiles[organizationProfile.id].owner.first_name;
		$scope.organizationFormData.middle_name = formatted_organizationProfiles[organizationProfile.id].owner.middle_name;
		$scope.organizationFormData.last_name = formatted_organizationProfiles[organizationProfile.id].owner.last_name;

		// date_of_birth: formatted_organizationProfiles[organizationProfile.id].owner.date_of_birth,
		$scope.organizationFormData.gender = formatted_organizationProfiles[organizationProfile.id].owner.gender;

		$scope.organizationFormData.primary_phone_number = parseInt(formatted_organizationProfiles[organizationProfile.id].owner.primary_phone_number);
		$scope.organizationFormData.state_id = formatted_organizationProfiles[organizationProfile.id].owner.state_id;
		$scope.organizationFormData.picture = formatted_organizationProfiles[organizationProfile.id].owner.primary_profile_picture;
		$scope.organizationFormData.address = formatted_organizationProfiles[organizationProfile.id].owner.address;

		// $scope.organizationForm.code.$setValidity('organizationProfileUniquenessForCode', false);
		// organizationForm.$setValidity('primary_phone_number', true);
	}


	/**
	 * Set the picture
	 * 
	 * @param files 
	 * @return       
	 */
	$scope.onLogoSelected = function (files) {
		$scope.logo = files[0];
	}
	$scope.onOwnerPictureSelected = function (files) {
		$scope.picture = files[0];
	}


	//Handle the storing of organization profile
	$scope.save = function() {
		var data = new FormData();
		data.append("logo", $scope.logo);
		data.append("picture", $scope.picture);
		data.append("data", JSON.stringify($scope.organizationFormData));

		$http({
			url: config.apiUrl+'organization-profile/store',
			method: "POST",
			data: data,
			headers: {
				"Content-Type": undefined,
				"Authorization": localstorageService.getObject('user').authorization,
				"RoleId": localstorageService.get('role_chosen', ''),
				"OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
			},				
			transformRequest: angular.identity			
		}).then(function (response) {
			sweetalertService.success('success', 'the organization was successfully created');
	  		var role_id = localstorageService.get('role_chosen');
			if (role_id == config.SUPER_ADMIN || role_id == config.ADMIN) {
				$state.go('secured.permission.all-organization');
			} else { 
				if ($scope.organizationFormData.organization_type_id == config.COMPANY) {
					$state.go('secured.permission.hmo-company');
				}
				if ($scope.organizationFormData.organization_type_id == config.PROVIDER) {
					$state.go('secured.permission.hmo-provider');
				}
			}
		}, function (response) {
		 	var error = { response };
		 	console.dir(error);
		 	sweetalertService.error('oops', error.response.data.error);
		});
	}

}]);
organization.controller('hmoProviderController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.getProviders()
    				   .then(function (response) {
    				   	    console.log(response);
    						$scope.showLoading = false;
    				   		$scope.hmoProviders = response.data;
    				   });

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.status = {
        model_name: 'HmoProvider',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    $scope.statusClick = function (hmo) {
        $scope.status.column_id = hmo.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                toastr.success(response.data.message);
                           });
    }
    
}]);
organization.controller('hmoCompanyController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.getCompanies()
    				   .then(function (response) {
    				   	    console.log(response);
    						$scope.showLoading = false;
    				   		$scope.companyHmos = response.data;
    				   });

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.viewPolicySet = function (companyHmo) {
    	// console.dir(companyHmo.company.slug);
    	$state.go('secured.permission.policyset-index', {'companySlug': companyHmo.company.slug});
    }

    $scope.status = {
        model_name: 'CompanyHmo',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    $scope.statusClick = function (hmo) {
        $scope.status.column_id = hmo.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                toastr.success(response.data.message);
                           });
    }

}]);
organization.controller('allCompanyController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.allCompanies()
    				   .then(function (response) {
    						$scope.showLoading = false;
    				   		$scope.companies = response.data;
    				   });

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.status = {
        model_name: 'OrganizationProfile',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    $scope.statusClick = function (company) {
        $scope.status.column_id = company.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                toastr.success(response.data.message);
                           });
    }

}]);
organization.controller('allProviderController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.allProviders()
    				   .then(function (response) {
    				   	    console.log(response);
    						$scope.showLoading = false;
    				   		$scope.providers = response.data;
    				   });

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.status = {
        model_name: 'OrganizationProfile',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    $scope.statusClick = function (provider) {
        $scope.status.column_id = provider.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                toastr.success(response.data.message);
                           });
    }

}]);
organization.controller('allHmoController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.getHmos()
    				   .then(function (response) {
    				   	    console.log(response);
    						$scope.showLoading = false;
    				   		$scope.hmos = response.data;
    				   });

    $scope.status = {
        model_name: 'OrganizationProfile',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    $scope.statusClick = function (hmo) {
        $scope.status.column_id = hmo.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                toastr.success(response.data.message);
                           });
    }

}]);
organization.controller('allOrganizationController', ['$scope', 'organizationService', 'localstorageService', '$state',
	function ($scope, organizationService, localstorageService, $state) {
	
    $scope.showLoading = true;

    organizationService.getOrganizations()
    				   .then(function (response) {
    				   	    console.log(response);
    						$scope.showLoading = false;
    				   		$scope.organizations = response.data;
    				   });

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    
    $scope.status = {
        model_name: 'OrganizationProfile',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.statusClick = function (organization) {
        $scope.status.column_id = organization.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        organizationService.updateStatus($scope.status)
                           .then(function (response) {
                                //push an event that is going to manually update role_user
                                toastr.success(response.data.message);
                           });
    }

}]);
service.controller('serviceIndexController', ['$scope', 'localstorageService', 'serviceService', 'sweetalertService', '$state',
    function ($scope, localstorageService, serviceService, sweetalertService, $state) {


    $scope.showLoading = true;

    $scope.mainPage = 1;

	$scope.loadPage = serviceService.get($scope.mainPage)
                          .then(function (response) {
                                console.dir(response);
                                $scope.showLoading = false;
                                
                                $scope.currentPage = response.data.current_page;
                                $scope.from = response.data.from;
                                $scope.lastPage = response.data.last_page;
                                $scope.to = response.data.to;
                                $scope.nextPageUrl = response.data.next_page_url;
                                $scope.prevPageUrl = response.data.prev_page_url;
                                $scope.perPage = response.data.per_page;
                                $scope.total = response.data.total;

                                $scope.services = response.data.data;
                          }, function (error) {
                    });

    $scope.loadPage;


    $scope.nextPage = function () {
        $scope.showLoading = true;
        $scope.services = {};

        $scope.mainPage = $scope.mainPage + 1;
        serviceService.get($scope.mainPage)
                      .then(function (response) {
                            console.dir(response);
                            $scope.showLoading = false;
                            
                            $scope.currentPage = response.data.current_page;
                            $scope.from = response.data.from;
                            $scope.lastPage = response.data.last_page;
                            $scope.to = response.data.to;
                            $scope.nextPageUrl = response.data.next_page_url;
                            $scope.prevPageUrl = response.data.prev_page_url;
                            $scope.perPage = response.data.per_page;
                            $scope.total = response.data.total;

                            $scope.services = response.data.data;
                      }, function (error) {
        });
    };

    $scope.prevPage = function () {

        $scope.showLoading = true;
        $scope.services = {};

        $scope.mainPage = $scope.mainPage - 1;
        serviceService.get($scope.mainPage)
                      .then(function (response) {
                            console.dir(response);
                            $scope.showLoading = false;
                            
                            $scope.currentPage = response.data.current_page;
                            $scope.from = response.data.from;
                            $scope.lastPage = response.data.last_page;
                            $scope.to = response.data.to;
                            $scope.nextPageUrl = response.data.next_page_url;
                            $scope.prevPageUrl = response.data.prev_page_url;
                            $scope.perPage = response.data.per_page;
                            $scope.total = response.data.total;
                                
                            $scope.services = response.data.data;
                      }, function (error) {
        });

    };

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

    $scope.deleteService = function (service, key) {
		// $scope.showLoading = true;
        serviceService.delete(service)
        			  .then(function (response) {
				  			// $scope.showLoading = false;

			  	            $scope.services.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    $scope.redirectToEdit = function (service) {
        $state.go('secured.permission.service-edit', {serviceSlug: service.slug});
    }


    $scope.range = [];
    var pages = [];

    for (var i = 1; i <= $scope.lastPage; i++) {
        pages.push(i);
    }

    $scope.range = pages;
    

}]);
service.controller('serviceCreateController', ['$scope', 'localstorageService', 'serviceService', 'sweetalertService', '$state', function ($scope, localstorageService, serviceService, sweetalertService, $state) {

    $scope.serviceFormData = {
        name: '',
        code: '',
        is_active: '',
    }
	
    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitServiceForm = function () {
    	serviceService.store($scope.serviceFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.service-index');
    				  });
    }

}]);
service.controller('serviceEditController', ['$scope', 'localstorageService', 'serviceService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, serviceService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    serviceService.getSingleFrom($stateParams.serviceSlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.service = response.data.service;
                        $scope.serviceFormData = {
                            name: $scope.service.name,
                            code: $scope.service.code,
                            is_active: $scope.service.is_active,
                            slug: $scope.service.slug,
                        }
                  }, function (error) {

                  });

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitServiceForm = function () {
    	serviceService.update($scope.serviceFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.service-index');
    				  });
    }

}]);
plan.controller('planController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', function ($scope, localstorageService, planService, sweetalertService, $state) {

    $scope.showLoading = true;
	
    $scope.planFormData = {
        name: '',
        code: '',
        is_active: '',
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	planService.get()
				  .then(function (response) {
                        $scope.showLoading = false;
				  		$scope.plans = response.data;
				  }, function (error) {

				  });

    $scope.deletePlan = function (plan, key) {
        planService.delete(plan)
        			  .then(function (response) {
				  			// $scope.showLoading = false;

			  	            $scope.plans.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    /**
     * Handle plan creation
     * 
     * @return 
     */
    $scope.submitPlanForm = function () {
    	planService.store($scope.planFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.plan-index');
    				  });
    }

    /**
     * Handle the process of adding service
     */
    $scope.redirectToAddServicePageFor = function (plan) {
        $state.go('secured.permission.plan-add-service-for', {slug: plan.slug});
    }

    /**
     * Handle the process of viewing service
     * 
     * @param  plan 
     * @return
     */
    $scope.redirectToViewServicePageFor = function (plan) {
        $state.go('secured.permission.plan-view-service-for', {slug: plan.slug});
    }

    /**
     * Redirect to edit plan
     * 
     * @param plan 
     * @return      
     */
    $scope.redirectToEdit = function (plan) {
        $state.go('secured.permission.plan-edit', {planSlug: plan.slug});
    }

}]);

plan.controller('planEditController', ['$scope', 'planService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, planService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    planService.getSingleFrom($stateParams.planSlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.plan = response.data.plan;
                        $scope.planFormData = {
                            name: $scope.plan.name,
                            code: $scope.plan.code,
                            is_active: $scope.plan.is_active,
                            slug: $scope.plan.slug,
                        }
                  }, function (error) {

                  });

    /**
     * Handle plan creation
     * 
     * @return 
     */
    $scope.submitPlanForm = function () {
    	planService.update($scope.planFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
    				  		return $state.go('secured.permission.plan-index');
    				  });
    }

}]);
plan.controller('planServiceController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', '$stateParams', 
    function ($scope, localstorageService, planService, sweetalertService, $state, $stateParams) {

        $scope.showLoading = true;

        var plan_slug = $stateParams.slug;

        planService.getPlan('plan/add-service/'+plan_slug)
        			  .then(function (response) {
                            $scope.showLoading = false;
                            $scope.plan = response.data.plan;
        			  		        $scope.services = response.data.services;

          					        $scope.service = {
          					          service_id: '',
          					          plan_id: $scope.plan.id,
          					          amount: 'add amount'
          					        };  

        			  }, function (error) {
                        
        			  });


        $scope.setService = function (service) {
          $scope.service.service_id = service.id;
        }

       $scope.addAmount = function (key, data) {
          $scope.service.amount = data;
          planService.addPrice($scope.service)
          			 .then(function (response) {
			            $scope.services.splice(key, 1);
			            toastr.success(response.data.message);
          			 }, function (response) {

          			 });
       }
}]);
plan.controller('planViewServiceController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', '$stateParams', 'httpClient',
    function ($scope, localstorageService, planService, sweetalertService, $state, $stateParams, httpClient) {

        var plan_slug = $stateParams.slug;

        planService.getPlan('plan/view-service/'+plan_slug)
                .then(function (response) {

                    console.log(response);

                    $scope.planServices = response.data.planServices;
                    $scope.plan = response.data.plan;

                    $scope.service = {
                      service_id: '',
                      plan_id: $scope.plan.id,
                      amount: ''
                    };  

                }, function (error) {
                        
                });


      /**
       * Handle the process of setting plan service
       * 
       * @param planService 
       */
      $scope.setService = function (planService) {
        $scope.service.amount = parseInt(planService.price);
        $scope.service.service_id = planService.service.id;
      }

      /**
       * Handle the process of adding adding amount
       * 
       * @param key
       * @param data
       */
      $scope.addAmount = function (key, data) {
        $scope.service.amount = data;

        planService.updatePrice($scope.service)
                   .then(function (response) {
                      $scope.planServices[key].price = data;
                      toastr.success(response.data.message);            
                   }, function () {

                   });
      }

      /**
       * Handle the process of redirecting to service page 
       * 
       * @param plan 
       * @return 
       */
      $scope.redirectToAddServicePageFor = function (plan) {
        $state.go('secured.permission.plan-add-service-for', {slug: plan.slug});
      }

      /**
       * handle the process of deleting trash 
       * 
       * @param  key         
       * @param  planService 
       * @return             
       */
      $scope.trashService = function (key, planService) {

        httpClient.getWithPermission('plan/delete-service-price/'+planService.id)
                 .then(function (response) {
                    $scope.planServices.splice(key, 1);
                    toastr.success(response.data.message);          
                 }, function () {

                 });
      }

}]);
policyset.controller('policysetIndexController', ['$scope', 'localstorageService', 'policysetService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policysetService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    $scope.displayPolicyFor = function (policyset) {
        $state.go('secured.permission.policy-index', {policysetSlug: policyset.slug});
    }
	
    $scope.addNewPolicySet = function () {
        $state.go('secured.permission.policyset-create', {companySlug: $stateParams.companySlug});
    }

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }


	policysetService.get($stateParams.companySlug)
				  .then(function (response) {
                        $scope.showLoading = false;                    
				  		$scope.policysets = response.data;
                  }, function (error) {

				  });


    $scope.deletePolicyset = function (service, key) {
        policysetService.delete(service)
        			  .then(function (response) {
			  	            $scope.policysets.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    $scope.redirectToEdit = function (policyset) {
        $state.go('secured.permission.policyset-edit', {companySlug: $stateParams.companySlug, policysetSlug: policyset.slug});
    }

}]);
policyset.controller('policysetCreateController', ['$scope', 'localstorageService', 'policysetService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policysetService, sweetalertService, $state, $stateParams) {

    $scope.allPolicySet = function () {
        $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    }

    $scope.policysetFormData = {
        company_slug: $stateParams.companySlug,
        name: '',
        start_date: '',
        end_date: '',
        is_active: '',
    }
    

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitPolicysetForm = function () {
        
    	policysetService.store($scope.policysetFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                            $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            sweetalertService.error('oops', error.response.data.end_date);
                      });
    }

}]);
policyset.controller('policysetEditController', ['$scope', 'localstorageService', 'policysetService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policysetService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    $scope.allPolicySet = function () {
        $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    }

    policysetService.getSingleFrom($stateParams.policysetSlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.policyset = response.data.policyset;

                        $scope.policysetFormData = {
                            company_slug: $stateParams.companySlug,
                            policyset_slug: $stateParams.policysetSlug,
                            name: $scope.policyset.name,
                            start_date: $scope.policyset.start_date,
                            end_date: $scope.policyset.end_date,
                            is_active: $scope.policyset.is_active,
                        }

                  }, function (error) {

                  });

    

    /**
     * Handle policyset creation
     * 
     * @return 
     */
    $scope.submitPolicysetForm = function () {
        
    	policysetService.update($scope.policysetFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                  $state.go('secured.permission.policyset-index', {companySlug: $stateParams.companySlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            sweetalertService.error('oops', error.response.data.end_date);
                      });
    }


}]);
beneficiary.controller('beneficiaryIndexController', ['$scope', 'localstorageService', 'planService', 'sweetalertService', '$state', 'beneficiaryService', 
    function ($scope, localstorageService, planService, sweetalertService, $state, beneficiaryService) {

    $scope.showLoading = true;

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	beneficiaryService.getBeneficiaryCompanies()
				  .then(function (response) {
                        console.dir(response);
                        $scope.showLoading = false;
                        $scope.beneficiaryCompanies = response.data.beneficiaryCompanies;
				  }, function (error) {

				  });

 //    $scope.deletePlan = function (plan, key) {
 //        planService.delete(plan)
 //        			  .then(function (response) {
	// 			  			// $scope.showLoading = false;

	// 		  	            $scope.plans.splice(key, 1);
	// 						toastr.success(response.data.message);
 //        			  });
 //    }

    /**
     * Handle plan creation
     * 
     * @return 
     */
    // $scope.submitPlanForm = function () {
    // 	planService.store($scope.planFormData)
    // 				  .then(function (response) {
    // 				  		sweetalertService.success(response.data.message);
    // 				  		return $state.go('secured.permission.plan-index');
    // 				  });
    // }

    // /**
    //  * Handle the process of adding service
    //  */
    // $scope.redirectToAddServicePageFor = function (plan) {
    //     $state.go('secured.permission.plan-add-service-for', {slug: plan.slug});
    // }

    // /**
    //  * Handle the process of viewing service
    //  * 
    //  * @param  plan 
    //  * @return
    //  */
    // $scope.redirectToViewServicePageFor = function (plan) {
    //     $state.go('secured.permission.plan-view-service-for', {slug: plan.slug});
    // }

}]);
beneficiary.controller('beneficiaryCreateController', ['$scope', 'localstorageService', 'beneficiaryService', 'sweetalertService', '$state', 'config', '$http', '$q',
    function ($scope, localstorageService, beneficiaryService, sweetalertService, $state, config, $http, $q) {

    $scope.showLoading = true;

    $scope.beneficiaryFormData = {
      policy_id: '',

      first_name: '',
      middle_name: '',
      last_name: '',

      date_of_birth: '',
      gender: '',
      picture: '',
      primary_phone_number: '',
      state_id: '',

      address: '',
      job_name: '',
      code: '',
    }

    //Handle the display of the modal
    $scope.currentStep = 1;
    $scope.steps = [
      {
        step: 1,
        name: "First step",
        template: "modules/beneficiary/views/step1.html"
      },
      {
        step: 2,
        name: "Second step",
        template: "modules/beneficiary/views/step2.html"
      },   
      {
        step: 3,
        name: "Third step",
        template: "modules/beneficiary/views/step3.html"
      }
    ];

    $scope.gotoStep = function(newStep) {
      $scope.currentStep = newStep;
    }  
    $scope.getStepTemplate = function(){
      for (var i = 0; i < $scope.steps.length; i++) {
        if ($scope.currentStep == $scope.steps[i].step) {
          return $scope.steps[i].template;
        }
      }
    }

    var policysets = beneficiaryService.getPolicies()
                      .then(function (response) {
                          console.dir(response);
                          $scope.policysets = response.data.policysets;
                      });

    var users = beneficiaryService.getUsers()
                      .then(function (response) {
                          console.dir(response);
                          $scope.users = response.data;
                      });

    var states = beneficiaryService.getStates()
          .then(function (response) {
              $scope.states = response.data;
          }, function (error) {
    });

    $q.all([policysets, users, states]).then(function () {
        $scope.showLoading = false;
    });

    $scope.onUserSelected = function (user, $model, $label) {
        // console.dir(user);
        $scope.beneficiaryFormData.email = user.email;
        $scope.beneficiaryFormData.first_name  = user.first_name;
        $scope.beneficiaryFormData.middle_name = user.middle_name;
        $scope.beneficiaryFormData.last_name = user.last_name;

        $scope.beneficiaryFormData.date_of_birth = user.date_of_birth;
        $scope.beneficiaryFormData.gender = user.gender;
        $scope.beneficiaryFormData.picture = user.primary_profile_picture;
        $scope.beneficiaryFormData.primary_phone_number = parseInt(user.primary_phone_number);
        $scope.beneficiaryFormData.state_id = user.state_id;

        // $scope.beneficiaryFormData.address = user.address;
        // $scope.beneficiaryFormData.job_name = user.job_name;
    }

    $scope.onPictureSelected = function (files) {
      console.dir(files);
      $scope.picture = files[0];
    }

    $scope.save = function () {
      console.dir('submiited');
     
      var data = new FormData();
      data.append("picture", $scope.picture);
      data.append("data", JSON.stringify($scope.beneficiaryFormData));

      $http({
        url: config.apiUrl+'beneficiary/store',
        method: "POST",
        data: data,
        headers: {
          "Content-Type": undefined,
          "Authorization": localstorageService.getObject('user').authorization,
          "RoleId": localstorageService.get('role_chosen', ''),
          "OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
        },        
        transformRequest: angular.identity      
      }).then(function (response) {
          sweetalertService.success('success', 'the beneficiary was created successfully');
          $state.go('secured.permission.beneficiary-index');          
      });
    }
}]);
moderator.controller('moderatorIndexController', ['$scope', 'localstorageService', 'sweetalertService', '$state', 'moderatorService', 
    function ($scope, localstorageService, sweetalertService, $state, moderatorService) {

    $scope.showLoading = true;

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	moderatorService.getmoderatorCompanies()
				  .then(function (response) {
                        console.dir(response);
                        $scope.showLoading = false;
                        $scope.organizationProfileModerators = response.data.organization_profile_moderators;
				  }, function (error) {

				  });

    $scope.statuses = [
        {value: 0, text: 'inactive'},
        {value: 1, text: 'active'},
    ];

    
    $scope.status = {
        model_name: 'OrganizationProfileModerator',
        column_name: 'is_active',
        column_id: '',
        column_value: '',
    }

    $scope.statusClick = function (organization) {
        $scope.status.column_id = organization.id;
    }

    $scope.updateStatus = function (key, data) {
        console.dir($scope.status);
        $scope.status.column_value = data;
        moderatorService.updateStatus($scope.status)
                           .then(function (response) {
                                //push an event that is going to manually update role_user
                                toastr.success(response.data.message);
                           });
    }
}]);
moderator.controller('moderatorCreateController', ['$scope', 'localstorageService', 'moderatorService', 'sweetalertService', '$state', 'config', '$http', '$q',
    function ($scope, localstorageService, moderatorService, sweetalertService, $state, config, $http, $q) {

    $scope.showLoading = true;

    $scope.moderatorFormData = {
      email: '',
      first_name: '',
      middle_name: '',
      last_name: '',

      date_of_birth: '',
      gender: '',
      picture: '',
      primary_phone_number: '',
      state_id: '',

      address: '',
      job_name: '',
      code: '',
    }

    //Handle the display of the modal
    $scope.currentStep = 1;
    $scope.steps = [
      {
        step: 1,
        name: "First step",
        template: "modules/moderator/views/step1.html"
      },
      {
        step: 2,
        name: "Second step",
        template: "modules/moderator/views/step2.html"
      },   
      {
        step: 3,
        name: "Third step",
        template: "modules/moderator/views/step3.html"
      }
    ];

    $scope.gotoStep = function(newStep) {
      $scope.currentStep = newStep;
    }  
    $scope.getStepTemplate = function(){
      for (var i = 0; i < $scope.steps.length; i++) {
        if ($scope.currentStep == $scope.steps[i].step) {
          return $scope.steps[i].template;
        }
      }
    }


    var users = moderatorService.getUsers()
                      .then(function (response) {
                          console.dir(response);
                          $scope.users = response.data;
                      });

    var states = moderatorService.getStates()
          .then(function (response) {
              $scope.states = response.data;
          }, function (error) {
    });

    $q.all([users, states]).then(function () {
        $scope.showLoading = false;
    });

    $scope.onUserSelected = function (user, $model, $label) {
        console.dir(user);
        $scope.moderatorFormData.email = user.email;
        $scope.moderatorFormData.first_name  = user.first_name;
        $scope.moderatorFormData.middle_name = user.middle_name;
        $scope.moderatorFormData.last_name = user.last_name;

        $scope.moderatorFormData.date_of_birth = user.date_of_birth;
        $scope.moderatorFormData.gender = user.gender;
        $scope.moderatorFormData.picture = user.primary_profile_picture;
        $scope.moderatorFormData.primary_phone_number = parseInt(user.primary_phone_number);
        $scope.moderatorFormData.state_id = user.state_id;
    }

    $scope.onPictureSelected = function (files) {
      console.dir(files);
      $scope.picture = files[0];
    }

    $scope.save = function () {
      console.dir('submiited');
     
      var data = new FormData();
      data.append("picture", $scope.picture);
      data.append("data", JSON.stringify($scope.moderatorFormData));

      $http({
        url: config.apiUrl+'moderator/store',
        method: "POST",
        data: data,
        headers: {
          "Content-Type": undefined,
          "Authorization": localstorageService.getObject('user').authorization,
          "RoleId": localstorageService.get('role_chosen', ''),
          "OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
        },        
        transformRequest: angular.identity      
      }).then(function (response) {
          sweetalertService.success('success', 'the moderator was created successfully');
          $state.go('secured.permission.moderator-index');          
      });
    }
}]);
dependent.controller('dependentIndexController', ['$scope', 'localstorageService', 'sweetalertService', '$state', 'dependentService', 
    function ($scope, localstorageService, sweetalertService, $state, dependentService) {

    $scope.showLoading = true;

    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }

	dependentService.getdependentCompanies()
				  .then(function (response) {
                        console.dir(response);
                        $scope.showLoading = false;
                        $scope.beneficiaryCompanyDependents = response.data.beneficiaryCompanyDependents;
				  }, function (error) {

				  });


}]);
dependent.controller('dependentCreateController', ['$scope', 'localstorageService', 'dependentService', 'sweetalertService', '$state', 'config', '$http', '$q',
    function ($scope, localstorageService, dependentService, sweetalertService, $state, config, $http, $q) {

    $scope.showLoading = true;

    $scope.dependentFormData = {
      email: '',

      first_name: '',
      middle_name: '',
      last_name: '',

      date_of_birth: '',
      gender: '',
      picture: '',
      
      primary_phone_number: '',
      state_id: '',

      address: '',
      job_name: '',
      code: '',
    }

    //Handle the display of the modal
    $scope.currentStep = 1;
    $scope.steps = [
      {
        step: 1,
        name: "First step",
        template: "modules/dependent/views/step1.html"
      },
      {
        step: 2,
        name: "Second step",
        template: "modules/dependent/views/step2.html"
      },   
      {
        step: 3,
        name: "Third step",
        template: "modules/dependent/views/step3.html"
      }
    ];

    $scope.gotoStep = function(newStep) {
      $scope.currentStep = newStep;
    }  
    $scope.getStepTemplate = function(){
      for (var i = 0; i < $scope.steps.length; i++) {
        if ($scope.currentStep == $scope.steps[i].step) {
          return $scope.steps[i].template;
        }
      }
    }


    var users = dependentService.getUsers()
                      .then(function (response) {
                          console.dir(response);
                          $scope.users = response.data;
                      });

    var states = dependentService.getStates()
          .then(function (response) {
              $scope.states = response.data;
          }, function (error) {
    });

    $q.all([users, states]).then(function () {
        $scope.showLoading = false;
    });

    $scope.onUserSelected = function (user, $model, $label) {
        console.dir(user);
        $scope.dependentFormData.email = user.email;
        $scope.dependentFormData.first_name  = user.first_name;
        $scope.dependentFormData.middle_name = user.middle_name;
        $scope.dependentFormData.last_name = user.last_name;

        $scope.dependentFormData.date_of_birth = user.date_of_birth;
        $scope.dependentFormData.gender = user.gender;
        $scope.dependentFormData.picture = user.primary_profile_picture;
        $scope.dependentFormData.primary_phone_number = parseInt(user.primary_phone_number);
        $scope.dependentFormData.state_id = user.state_id;
    }

    $scope.onPictureSelected = function (files) {
      console.dir(files);
      $scope.picture = files[0];
    }

    $scope.save = function () {
           
      var data = new FormData();
      data.append("picture", $scope.picture);
      data.append("data", JSON.stringify($scope.dependentFormData));

      $http({
        url: config.apiUrl+'dependent/store',
        method: "POST",
        data: data,
        headers: {
          "Content-Type": undefined,
          "Authorization": localstorageService.getObject('user').authorization,
          "RoleId": localstorageService.get('role_chosen', ''),
          "OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
        },        
        transformRequest: angular.identity      
      }).then(function (response) {
          sweetalertService.success('success', 'the dependent was created successfully');
          $state.go('secured.permission.dependent-index');          
      });

    }
}]);
policy.controller('policyIndexController', ['$scope', 'localstorageService', 'policyService', 'sweetalertService', '$state', '$stateParams', 'planService',
    function ($scope, localstorageService, policyService, sweetalertService, $state, $stateParams, planService) {

    $scope.showLoading = true;

    /**
     * Redirect to adding new policy
     */
    $scope.addNewPolicy = function () {
        $state.go('secured.permission.policy-create', {policysetSlug: $stateParams.policysetSlug});
    }
	
    $scope.isActive = function (status) {
        if (status == 1) {
            return true;
        } else {
            return false;
        }
    }


	policyService.get($stateParams.policysetSlug)
				  .then(function (response) {
                        $scope.showLoading = false;                    
				  		$scope.policies = response.data;
                  }, function (error) {

				  });


    $scope.deletePolicy = function (policy, key) {
        console.dir(policy);
        policyService.delete(policy)
        			  .then(function (response) {
			  	            $scope.policies.splice(key, 1);
							toastr.success(response.data.message);
        			  });
    }

    $scope.redirectToEdit = function (policy) {
        $state.go('secured.permission.policy-edit', {policysetSlug: $stateParams.policysetSlug, policySlug: policy.slug});
    }

}]);
policy.controller('policyCreateController', ['$scope', 'localstorageService', 'policyService', 'sweetalertService', '$state', '$stateParams',
    function ($scope, localstorageService, policyService, sweetalertService, $state, $stateParams) {

    $scope.showLoading = true;

    $scope.policyFormData = {
        policyset_slug: $stateParams.policysetSlug,
        plan_id: '',
        name: '',
        maximum_no_of_beneficiary_dependant: '',
        is_active: '',
    }

    /**
     * redirect to policy index
     * 
     * @return 
     */
    $scope.allPolicy = function () {
        $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    }

    /**
     * Get this organization plans
     */
    policyService.getPlan()
                 .then(function (response) {
                    $scope.showLoading = false;
                    $scope.plans = response.data;
                 });
    

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitPolicyForm = function () {
        console.dir('submited');
        // console.dir($scope.policyFormData);

    	policyService.store($scope.policyFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                            $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            // sweetalertService.error('oops', error.response.data);
                      });
    }

}]);
policy.controller('policyEditController', ['$scope', 'localstorageService', 'policyService', 'sweetalertService', '$state', '$stateParams', '$q',
    function ($scope, localstorageService, policyService, sweetalertService, $state, $stateParams, $q) {

    $scope.showLoading = true;

    /**
     * Get single policy
     * 
     * @type 
     */
    var policy = policyService.getSingleFrom($stateParams.policySlug)
                  .then(function (response) {
                        $scope.showLoading = false;
                        $scope.policy = response.data.policy;

                        $scope.policyFormData = {
                            policy_slug: $stateParams.policySlug,
                            policyset_slug: $stateParams.policysetSlug,
                            plan_id: $scope.policy.plan_id,
                            name: $scope.policy.name,
                            maximum_no_of_beneficiary_dependant: $scope.policy.maximum_no_of_beneficiary_dependant,
                            is_active: $scope.policy.is_active,
                        }

                  }, function (error) {

                  });

    /**
     * Get this organization plans
     */
    var plans = policyService.getPlan()
                 .then(function (response) {
                    $scope.showLoading = false;
                    $scope.plans = response.data;
                 });

    /**
     * Note when all promises are resolved
     * 
     * @param  
     * @return 
     */
    $q.all([policy, plans]).then(function () {
        $scope.showLoading = false;
    });

    /**
     * redirect to policy index
     * 
     * @return 
     */
    $scope.allPolicy = function () {
        $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    }
    
    $scope.allPolicySet = function () {
        $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    }

    /**
     * Handle service creation
     * 
     * @return 
     */
    $scope.submitPolicyForm = function () {
    	policyService.update($scope.policyFormData)
    				  .then(function (response) {
    				  		sweetalertService.success(response.data.message);
                            $state.go('secured.permission.policy-index', {policysetSlug: $stateParams.policysetSlug});
    				  }, function (response) {
                            var error = { response };
                            console.dir(error);
                            // sweetalertService.error('oops', error.response.data);
                      });
    }

}]);
directives.directive('myLoadingSpinner', ['$compile', function ($compile) {

	return {
		templateUrl: 'modules/shared/directives/circle-spinner.html' 
	}
	
}]);
directives.directive('onFileChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.onFileChange);

      element.bind('change', function() {
        scope.$apply(function() {
          var files = element[0].files;
          if (files) {
            onChangeHandler(files);
          }
        });
      });

    }
  };
});
directives.directive('uniqueUserPhonenumber', ['checkService', function (checkService) {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			
			var users = scope.users;

			console.dir(users);

			var array = [];	
			for (j = 0; j < users.length; j++) {
				array[parseInt(users[j].primary_phone_number)] = parseInt(users[j].primary_phone_number); 
			}

			ngModelCtrl.$validators.user_phone_invalid = function (modelValue, viewValue) {
				return ! checkService.isInArray(parseInt(viewValue), array);
			}

		}
	}
}]);
directives.directive('organizationProfileUniquenessForCode', ['checkService', function (checkService) {
	return {
		require: 'ngModel',
		link: function (scope, element, attr, ngModelCtrl) {
			
			var organizationProfiles = scope.organizationProfiles;

			var array = [];	
			for (j = 0; j < organizationProfiles.length; j++) {
				array[organizationProfiles[j].code] = organizationProfiles[j].code; 
			}

			ngModelCtrl.$validators.organization_code_invalid = function (modelValue, viewValue) {
				return ! checkService.isInArray(viewValue, array);
			}

		}
	}
}]);
var hmoApplication = angular.module('hmoApplication', [
		'authentication', 
		'secured',
		'permission',
		'dashboard',
		'mainDashboard',
		'service',
		'plan',
		'policyset',
		'organization',
		'directives',
		'policy',
		'beneficiary',
		'moderator',
		'dependent',
]);

hmoApplication.constant('config', {  
  apiUrl: 'http://hmoapi.dev/api/',
  baseUrl: '/',
  enableDebug: true,
  SUPER_ADMIN: 1,
  ADMIN: 2,
  PROVIDER: 2,
  COMPANY: 3,
});

hmoApplication.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/login')
}]);


hmoApplication.run(['$rootScope', 'authenticationService', '$state', 'localstorageService', 'checkService', '$timeout', 
	function ($rootScope, authenticationService, $state, localstorageService, checkService, $timeout) {

	$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

		console.log('about to reload page');
		// console.log($state);

		//needs authentication
		if (toState.authentication) {
			//the user is not authenticated
			if (! authenticationService.isAuthenticated() ) {
				event.preventDefault();
				authenticationService.storeStateGuestWantToVisit(toState);
				$state.go('login');
			}

			//the user is authentication
			if ( authenticationService.isAuthenticated() ) {
				//i need to do my role/permissions here
				if (! checkService.forEmptyString(localstorageService.get('role_chosen')) ) {

					$timeout(function () {
						$rootScope.$broadcast('roleWasChosen', localstorageService.get('role_chosen'));					
					}, 500);

				}
				// console.log('needs authentication and already authenticated');
			}
		}


		//does not needs authentication 
		if (!toState.authentication) {
			//user is authenticated
			if ( authenticationService.isAuthenticated() ) {
				//redirect if the user wants to visit login page when authenticated
				if (toState.name == 'login') {
					event.preventDefault();
					$state.go('secured.dashboard');
				}
			}

			//user is not authenticated
			if (! authenticationService.isAuthenticated() ) {
				console.log('route does not need authentication, and the user is not authenticated');
			}
		}


	});

}]);
var services = angular.module('services', []);

var secured = angular.module('secured', ['ui.router']);

secured.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured', {
			abstract: true,
			controller: 'securedController',
			templateUrl: 'modules/shared/secured/views/secured.html',
		})
}]);

var permission = angular.module('permission', ['ui.router', 'secured']);

permission.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission', {
			abstract: true,
			controller: 'permissionController',
			templateUrl: 'modules/shared/permission/views/permission.html',
		})
}]);
var authentication = angular.module('authentication', ['ui.router', 'services', 'secured']);

authentication.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('login', {
			url: '/login',
			templateUrl: 'modules/authentication/views/login.html',
			controller: 'authenticationController',
			authentication: false
		});
}]);
var dashboard = angular.module('dashboard', ['ui.router', 'secured']);

dashboard.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.dashboard', {
			url: '/dashboard',
			templateUrl: 'modules/dashboard/views/dashboard.html',
			controller: 'dashboardController',
			authentication: true,
		});
}]);
var mainDashboard = angular.module('mainDashboard', ['ui.router', 'permission']);

mainDashboard.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.mainDashboard', {
			url: '/main-dashboard',
			templateUrl: 'modules/main-dashboard/views/main-dashboard.html',
			controller: 'mainDashboardController',
			authentication: true,
		});
}]);
var service = angular.module('service', ['ui.router', 'permission', 'directives']);

service.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.service-index', {
			url: '/service-index',
			templateUrl: 'modules/service/views/service-index.html',
			controller: 'serviceIndexController',
			authentication: true
		})
		.state('secured.permission.service-create', {
			url: '/service-create',
			templateUrl: 'modules/service/views/service-create.html',
			controller: 'serviceCreateController',
			authentication: true
		})
		.state('secured.permission.service-edit', {
			url: '/service-edit/:serviceSlug',
			templateUrl: 'modules/service/views/service-edit.html',
			controller: 'serviceEditController',
			authentication: true
		})
}]);
var plan = angular.module('plan', ['ui.router', 'permission', 'xeditable']);

plan.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.plan-index', {
			url: '/plan-index',
			templateUrl: 'modules/plan/views/plan-index.html',
			controller: 'planController',
			authentication: true
		})
		.state('secured.permission.plan-create', {
			url: '/plan-create',
			templateUrl: 'modules/plan/views/plan-create.html',
			controller: 'planController',
			authentication: true
		})
		.state('secured.permission.plan-edit', {
			url: '/plan-edit/:planSlug',
			templateUrl: 'modules/plan/views/plan-edit.html',
			controller: 'planEditController',
			authentication: true
		})
		.state('secured.permission.plan-add-service-for', {
			url: '/plan-add-service-for/:slug',
			templateUrl: 'modules/plan/views/plan-add-service.html',
			controller: 'planServiceController',
			authentication: true
		})
		.state('secured.permission.plan-view-service-for', {
			url: '/plan-view-service-for/:slug',
			templateUrl: 'modules/plan/views/plan-view-service.html',
			controller: 'planViewServiceController',
			authentication: true
		})		
}]);

// plan.run(function(editableOptions) {
//   editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
// });
var policyset = angular.module('policyset', ['ui.router', 'permission']);

policyset.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.policyset-index', {
			url: '/policyset-index/:companySlug',
			templateUrl: 'modules/policyset/views/policyset-index.html',
			controller: 'policysetIndexController',
			authentication: true
		})
		.state('secured.permission.policyset-create', {
			url: '/policyset-create/:companySlug',
			templateUrl: 'modules/policyset/views/policyset-create.html',
			controller: 'policysetCreateController',
			authentication: true
		})
		.state('secured.permission.policyset-edit', {
			url: '/policyset-edit/:companySlug/:policysetSlug',
			templateUrl: 'modules/policyset/views/policyset-create.html',
			controller: 'policysetEditController',
			authentication: true
		})
}]);


policyset.filter('moment', function () {
    return function (input, momentFn /*, param1, param2, ...param n */) {
        var args = Array.prototype.slice.call(arguments, 2),
        momentObj = moment(input);
        return momentObj[momentFn].apply(momentObj, args);
    };
});

var policy = angular.module('policy', ['ui.router', 'permission']);

policy.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.policy-index', {
			url: '/policy-index/:policysetSlug',
			templateUrl: 'modules/policy/views/policy-index.html',
			controller: 'policyIndexController',
			authentication: true
		})
		.state('secured.permission.policy-create', {
			url: '/policy-create/:policysetSlug',
			templateUrl: 'modules/policy/views/policy-create.html',
			controller: 'policyCreateController',
			authentication: true
		})
		.state('secured.permission.policy-edit', {
			url: '/policy-edit/:policysetSlug/:policySlug',
			templateUrl: 'modules/policy/views/policy-edit.html',
			controller: 'policyEditController',
			authentication: true
		})
}]);

var organization = angular.module('organization', ['ui.router', 'permission', 'directives', 'ui.bootstrap', 'ngAnimate', 'filters', 'xeditable']);

organization.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.organization-create', {
			url: '/organization-create',
			templateUrl: 'modules/organization/views/organization-create.html',
			controller: 'organizationController',
			authentication: true
		})
		.state('secured.permission.hmo-company', {
			url: '/hmo-company',
			templateUrl: 'modules/organization/views/hmo-company.html',
			controller: 'hmoCompanyController',
			authentication: true
		})
		.state('secured.permission.hmo-provider', {
			url: '/hmo-provider',
			templateUrl: 'modules/organization/views/hmo-provider.html',
			controller: 'hmoProviderController',
			authentication: true
		})
		.state('secured.permission.all-organization', {
			url: '/all-organization',
			templateUrl: 'modules/organization/views/all-organization.html',
			controller: 'allOrganizationController',
			authentication: true
		})
		.state('secured.permission.all-hmo', {
			url: '/all-hmo',
			templateUrl: 'modules/organization/views/all-hmo.html',
			controller: 'allHmoController',
			authentication: true
		})
		.state('secured.permission.all-company', {
			url: '/all-company',
			templateUrl: 'modules/organization/views/all-company.html',
			controller: 'allCompanyController',
			authentication: true
		})
		.state('secured.permission.all-provider', {
			url: '/all-provider',
			templateUrl: 'modules/organization/views/all-provider.html',
			controller: 'allProviderController',
			authentication: true
		})
}]);

organization.run(function(editableOptions) {
  editableOptions.theme = 'bs4'; // bootstrap3 theme. Can be also 'bs2', 'default'
});
var directives = angular.module('directives', []);

var beneficiary = angular.module('beneficiary', ['ui.router', 'permission', 'ui.bootstrap', 'ngAnimate']);

beneficiary.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.beneficiary-index', {
			url: '/beneficiary-index',
			templateUrl: 'modules/beneficiary/views/beneficiary-index.html',
			controller: 'beneficiaryIndexController',
			authentication: true
		})
		.state('secured.permission.beneficiary-create', {
			url: '/beneficiary-create',
			templateUrl: 'modules/beneficiary/views/beneficiary-create.html',
			controller: 'beneficiaryCreateController',
			authentication: true
		})		
}]);
var moderator = angular.module('moderator', ['ui.router', 'permission', 'ui.bootstrap', 'ngAnimate']);

moderator.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.moderator-index', {
			url: '/moderator-index',
			templateUrl: 'modules/moderator/views/moderator-index.html',
			controller: 'moderatorIndexController',
			authentication: true
		})
		.state('secured.permission.moderator-create', {
			url: '/moderator-create',
			templateUrl: 'modules/moderator/views/moderator-create.html',
			controller: 'moderatorCreateController',
			authentication: true
		})		
}]);
var dependent = angular.module('dependent', ['ui.router', 'permission', 'ui.bootstrap', 'ngAnimate']);

dependent.config(['$stateProvider', function ($stateProvider) {
	$stateProvider
		.state('secured.permission.dependent-index', {
			url: '/dependent-index',
			templateUrl: 'modules/dependent/views/dependent-index.html',
			controller: 'dependentIndexController',
			authentication: true
		})
		.state('secured.permission.dependent-create', {
			url: '/dependent-create',
			templateUrl: 'modules/dependent/views/dependent-create.html',
			controller: 'dependentCreateController',
			authentication: true
		})		
}]);
var filters = angular.module('filters', []);

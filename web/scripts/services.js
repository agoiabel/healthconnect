authentication.factory('authenticationService', ['$state', 'httpClient', 'sweetalertService', 'localstorageService', 'checkService',
	function ($state, httpClient, sweetalertService, localstorageService, checkService) {
	
	var authenticationService = {};

	/**
	 * check if authenticated
	 * 
	 * @return {Boolean} 
	 */
	authenticationService.isAuthenticated = function () {
		return localstorageService.get('authenticated', false);
	}

	/**
	 * store the state the user wants to visit
	 * 
	 * @param  state 
	 * @return 
	 */
	authenticationService.storeStateGuestWantToVisit = function (state) {
		localstorageService.set('previous_route', state.name);
	}

	/**
	 * check the previous state
	 * 
	 * @return 
	 */
	authenticationService.checkPreviousRoute = function () {
		if ( checkService.forEmptyString(localstorageService.get('previous_route')) ) {
			return false;
		}
		return true;
	}

	/**
	 * Get previous route
	 * 
	 * @return 
	 */
	authenticationService.getPreviousRoute = function () {
		return localstorageService.get('previous_route');
	}

	/**
	 * redirect user on successful login
	 * 
	 * @return 
	 */
	authenticationService.redirectUser = function () {
		if ( authenticationService.isAuthenticated() ) {
			console.log('ready for redirection');

			sweetalertService.success('hi', 'welcome back');

			//check if there,s a storage previous_route
			if ( authenticationService.checkPreviousRoute() ) {
				var route = authenticationService.getPreviousRoute('previous_route');

				localstorageService.removeItem(route);

				return $state.go(route);
			}
			
			return $state.go('secured.dashboard');
		}
	}

	authenticationService.logout = function () {

		localstorageService.removeAll();

		sweetalertService.success('bye', 'see yha again');

		return $state.go('login');
	}


	return authenticationService;
}]);
services.factory('sweetalertService', [function () {
	var sweetalertService = {};

	sweetalertService.create = function (type, title, message) {
		return swal({   
			title: title,   
			text: message,   
			type: type,
			timer: 3000,
			showConfirmButton: false   
		});
	}

	sweetalertService.info = function (title, message) {
		return sweetalertService.create("info", title, message);
	}

	sweetalertService.success = function (title, message) {
		return sweetalertService.create("success", title, message);
	}

	sweetalertService.error = function (title, message) {
		return sweetalertService.create("error", title, message);
	}

	sweetalertService.confirm = function (title, message, confirmButtonText, callbackFunction) {
		return swal({   
			title: title,   
			text: message,   
			type: "warning",
			showConfirmButton: true,
			showConfirmButtonColor: "#2cc185",
			confirmButtonText: confirmButtonText,   
		}, callbackFunction);
	}

	return sweetalertService;
}]);
services.factory('localstorageService', ['$window', function ($window) {
	
	var localstorageService = {};

	/**
	 * set string in the local storage
	 * 
	 * @param key   
	 * @param value
	 */
	localstorageService.set = function (key, value) {
		$window.localStorage[key] = value;
	}

	/**
	 * set object in the local storage
	 * 
	 * @param key   
	 * @param value 
	 */
	localstorageService.setObject = function (key, value) {
		$window.localStorage[key] = JSON.stringify(value);
	}

	/**
	 * get a string stored in the local storage
	 * 
	 * @param  key          
	 * @param defaultValue
	 * @return
	 */
	localstorageService.get = function (key, defaultValue) {
		return $window.localStorage[key] || defaultValue;
	}

	/**
	 * get an object stored in the local storage
	 * 
	 * @param  key          
	 * @param defaultValue
	 * @return
	 */
	localstorageService.getObject = function (key) {
		return JSON.parse($window.localStorage[key] || {});
	}

	/**
	 * Remove an item from localstorage
	 * 
	 * @param key 
	 * @return
	 */
	localstorageService.removeItem = function (key) {
		$window.localStorage.removeItem(key);
	}

	/**
	 * clear all localstorage
	 * 
	 * @param key 
	 * @return
	 */
	localstorageService.removeAll = function () {
		$window.localStorage.clear();
	}


	return localstorageService;
}]);
services.factory('loadingService', [function () {
	var loadingService = {};

	return loadingService;
}]);
services.factory('httpClient', ['$http', 'config', 'localstorageService', function ($http, config, localstorageService) {
	var httpClient = {};

	/**
	 * set http settings
	 * 
	 * @return 
	 */
	function getSettingsFrom(requestData) {
		return {
			url: requestData.url,
			method: requestData.method,
			dataType: requestData.dataType || "json",
			data: requestData.data || {},
			headers: requestData.headers || {
				"accept": "application/json"
			}
		}
	}

	httpClient.postWithPermission = function (formData, url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'POST',
			data: formData,
			headers: {
				"Authorization": localstorageService.getObject('user').authorization,
				"RoleId": localstorageService.get('role_chosen', ''),
				"OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
			}
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.getWithPermission = function (url) {
		// console.log(localstorageService.get('current_organization_profile_id', ''));
		var requestData = {
			url: config.apiUrl + url,
			method: 'GET',
			headers: {
				"Authorization": localstorageService.getObject('user').authorization,
				"RoleId": localstorageService.get('role_chosen', ''),
				"OrganizationProfileId": localstorageService.get('current_organization_profile_id', ''),
			}			
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.postIsAuthorize = function (formData, url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'POST',
			data: formData,
			headers: {
				"Authorization": localstorageService.get('user').authorization
			}			
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.getIsAuthorize = function (url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'GET',
			data: formData,
			headers: {
				"Authorization": localstorageService.get('user').authorization
			}			
		};
		return $http( getSettingsFrom(requestData) );		
	}

	httpClient.post = function (formData, url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'POST',
			data: formData			
		};
		return $http( getSettingsFrom(requestData) );
	}

	httpClient.get = function (url) {
		var requestData = {
			url: config.apiUrl + url,
			method: 'GET',
			data: formData			
		};
		return $http( getSettingsFrom(requestData) );
	}

	return httpClient;
}]);
services.factory('checkService', [function () {
	
	var checkService = {};

	/**
	 * check if a string is empty
	 * 
	 * @param string 
	 * @return
	 */
	checkService.forEmptyString = function (string) {
	    if (typeof string == 'undefined' || !string || string.length === 0 || string === "" || !/[^\s]/.test(string) || /^\s*$/.test(string) || string.replace(/\s/g,"") === "")
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}

	/**
	 * check if an object is empty
	 * 
	 * @param obj
	 * @return
	 */
	checkService.forEmptyObject = function (obj) {
		for (var key in obj) {
			if (obj.hasOwnProperty(key)) {
				return false;
			}
		}
		return true;
	} 

	/**
	 * check if a string is in an array
	 * 
	 * @return 
	 */
	checkService.isInArray = function (target, array) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] === target) {
				return true;
			}
		}

		return false;
	}

	/**
	 * check if an array is in another array
	 * 
	 * @return 
	 */
	checkService.arrayContainAnotherArray = function (array1, array2) {
		for (var i = 0; i < array1.length; i++) {
			if (array2.indexOf( array1[i] === -1 )) {
				return false;
			}
		}

		return true;
	}


	return checkService;
}]);
service.factory('serviceService', ['$state', 'httpClient', 'localstorageService',
	function ($state, httpClient, localstorageService) {
	
	var serviceService = {};

	/**
	 * check if authenticated
	 * 
	 * @return {Boolean} 
	 */
	serviceService.get = function (page) {
		return httpClient.getWithPermission('service/index?page='+page);
	}

	serviceService.delete = function (service) {
		return httpClient.getWithPermission('service/delete/'+service.id)
	}
	
	serviceService.store = function (serviceFormData) {
		return httpClient.postWithPermission(serviceFormData, 'service/store');
	}

	serviceService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('service/show/'+slug);
	}

	serviceService.update = function (serviceFormData) {
		return httpClient.postWithPermission(serviceFormData, 'service/update');
	}

	return serviceService;
}]);
organization.factory('organizationService', ['$state', 'httpClient', 'config', '$http', 'localstorageService',
	function ($state, httpClient, config, $http, localstorageService) {
	
	var organizationService = {};

	/**
	 * return all organization 
	 * 
	 * @return 
	 */
	organizationService.get = function () {
		return httpClient.getWithPermission('organization/index');
	}

	/**
	 * return all states
	 * 
	 * @return 
	 */
	organizationService.getStates = function () {
		return httpClient.getWithPermission('state/index');
	}

	/**
	 * Handle the process of getting all organizations
	 * 
	 * @return 
	 */
	organizationService.getOrganizationProfiles = function () {
		return httpClient.getWithPermission('organization-profile/index');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getCompanies = function () {
		return httpClient.getWithPermission('hmo/company');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getProviders = function () {
		return httpClient.getWithPermission('hmo/provider');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getOrganizations = function () {
		return httpClient.getWithPermission('all_organization');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.getHmos = function () {
		return httpClient.getWithPermission('all_hmo');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.allCompanies = function () {
		return httpClient.getWithPermission('all_company');
	}

	/**
	 * Get the organization profile companies
	 * 
	 * @return 
	 */
	organizationService.allProviders = function () {
		return httpClient.getWithPermission('all_provider');
	}

	/**
	 * Get all users
	 * 
	 * @return 
	 */
	organizationService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	organizationService.updateStatus = function (statusFormData) {
		return httpClient.postWithPermission(statusFormData, 'database-column-update');
	}


	return organizationService;
}]);
plan.factory('planService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var planService = {};

	/**
	 * check if authenticated
	 * 
	 * @return
	 */
	planService.get = function () {
		return httpClient.getWithPermission('plan/index');
	}

	/**
	 * delete a plan
	 * 
	 * @param plan 
	 * @return 
	 */
	planService.delete = function (plan) {
		return httpClient.getWithPermission('plan/delete/'+plan.id)
	}
		
	/**
	 * store creation of plan
	 * 
	 * @param planFormData 
	 * @return 
	 */
	planService.store = function (planFormData) {
		return httpClient.postWithPermission(planFormData, 'plan/store');
	}

	/**
	 * get the plan for a particular service
	 * 
	 * @param url 
	 * @return     
	 */
	planService.getPlan = function (url) {
		return httpClient.getWithPermission(url);
	}

	/**
	 * Add price to a particular service
	 * 
	 * @param 
	 */
	planService.addPrice = function (planServiceData) {
		return httpClient.postWithPermission(planServiceData, 'plan/add-service-price');
	}

	/**
	 * update the price
	 * 
	 * @param service 
	 * @return         
	 */
	planService.updatePrice = function (service) {
		return httpClient.postWithPermission(service, 'plan/add-service-price');
	}

	/**
	 * delete a service with price
	 * 
	 * @param planService 
	 * @return 
	 */
	planService.deleteServiceWithPrice = function (planService) {
		return httpClient.getWithPermission('plan/delete-service-price/'+planService.id);
	}

	planService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('plan/show/'+slug);
	}

	planService.update = function (planFormData) {
		return httpClient.postWithPermission(planFormData, 'plan/update');
	}

	return planService;
}]);
policyset.factory('policysetService', ['$state', 'httpClient', 'localstorageService',
	function ($state, httpClient, localstorageService) {
	
	var policysetService = {};

	/**
	 * check if authenticated
	 * 
	 * @return {Boolean} 
	 */
	policysetService.get = function (companySlug) {
		return httpClient.getWithPermission('policyset/index/'+companySlug);
	}

	
	policysetService.delete = function (policyset) {
		return httpClient.getWithPermission('policyset/delete/'+policyset.id)
	}
	
	policysetService.store = function (policysetFormData) {
		return httpClient.postWithPermission(policysetFormData, 'policyset/store');
	}

	policysetService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('policyset/show/'+slug);
	}

	policysetService.update = function (policysetFormData) {
		return httpClient.postWithPermission(policysetFormData, 'policyset/update');
	}	

	return policysetService;
}]);

policy.factory('policyService', ['$state', 'httpClient', 'localstorageService',
	function ($state, httpClient, localstorageService) {
	
	var policyService = {};

	policyService.getPlan = function () {
		return httpClient.getWithPermission('plan/index');
	}

	/**
	 * check if authenticated
	 * 
	 * @return 
	 */
	policyService.get = function (policysetSlug) {
		return httpClient.getWithPermission('policy/index/'+policysetSlug);
	}

	policyService.delete = function (policy) {
		return httpClient.getWithPermission('policy/delete/'+policy.id)
	}
	
	policyService.store = function (policyFormData) {
		return httpClient.postWithPermission(policyFormData, 'policy/store');
	}

	policyService.getSingleFrom = function (slug) {
		return httpClient.getWithPermission('policy/show/'+slug);
	}

	policyService.update = function (policyFormData) {
		return httpClient.postWithPermission(policyFormData, 'policy/update');
	}	

	return policyService;
}]);

beneficiary.factory('beneficiaryService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var beneficiaryService = {};

	/**
	 * check if authenticated
	 * 
	 * @return
	 */
	beneficiaryService.getPolicies = function () {
		return httpClient.getWithPermission('company/policyset');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	beneficiaryService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	beneficiaryService.getStates = function () {
		return httpClient.getWithPermission('state/index');		
	}

	/**
	 * Get the company beneficiaries
	 * 
	 * @return 
	 */
	beneficiaryService.getBeneficiaryCompanies = function () {
		return httpClient.getWithPermission('beneficiary/index');		
	}


	return beneficiaryService;
}]);
moderator.factory('moderatorService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var moderatorService = {};

	/**
	 * check if authenticated
	 * 
	 * @return
	 */
	moderatorService.getPolicies = function () {
		return httpClient.getWithPermission('company/policyset');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	moderatorService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	moderatorService.getStates = function () {
		return httpClient.getWithPermission('state/index');		
	}

	/**
	 * Get the company beneficiaries
	 * 
	 * @return 
	 */
	moderatorService.getmoderatorCompanies = function () {
		return httpClient.getWithPermission('moderator/index');		
	}

	moderatorService.updateStatus = function (statusFormData) {
		return httpClient.postWithPermission(statusFormData, 'database-column-update');
	}

	return moderatorService;
}]);
dependent.factory('dependentService', ['$state', 'httpClient', 'localstorageService', function ($state, httpClient, localstorageService) {
	
	var dependentService = {};

	/**
	 * get all users
	 * 
	 * @return 
	 */
	dependentService.getUsers = function () {
		return httpClient.getWithPermission('user/index');
	}

	/**
	 * get all users
	 * 
	 * @return 
	 */
	dependentService.getStates = function () {
		return httpClient.getWithPermission('state/index');		
	}

	dependentService.getdependentCompanies = function () {
		return httpClient.getWithPermission('dependent/index');
	}

	return dependentService;
}]);